/*
 * Copyright (C) 2019.  Daniel Jimenez
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.apirest

import com.dinan.controlhorario.data.apientities.login.ApiConfirmChangePass
import io.reactivex.Observable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Url


interface ApiChangePass {

    /**
     * Interfaz para cambiar el password
     */
    @FormUrlEncoded
    @POST
    fun changePass(
        @Url url: String?,
        @Field("dinan") token: String?,
        @Field("cif") cif: String?,
        @Field("utoken") userToken: String?,
        @Field("donde") donde: String,
        @Field("opc")  opcCanPunchIn:  String,
        @Field("change_pass") password:  String
    ): Observable<ApiConfirmChangePass>
}