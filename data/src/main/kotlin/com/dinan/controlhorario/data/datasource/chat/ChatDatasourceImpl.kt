/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.datasource.chat

import android.content.SharedPreferences
import com.dinan.controlhorario.data.BuildConfig
import com.dinan.controlhorario.data.apientities.chat.ApiChatResponse
import com.dinan.controlhorario.data.apientities.chat.ApiSendMessageChat
import com.dinan.controlhorario.data.apirest.ApiChat
import com.dinan.controlhorario.data.apirest.OpcApis.DONDE_CHAT
import com.dinan.controlhorario.data.apirest.OpcApis.DONDE_CHAT_SEND_TEXT
import com.dinan.controlhorario.data.apirest.OpcApis.DONDE_TOKEN_FIREBASE
import com.dinan.controlhorario.data.apirest.OpcApis.OPC_CHAT
import com.dinan.controlhorario.data.apirest.OpcApis.OPC_CHAT_SEND_TEXT
import com.dinan.controlhorario.data.apirest.OpcApis.OPC_TOKEN_FIREBASE
import com.dinan.controlhorario.data.core.map.mapFromPreference
import com.dinan.controlhorario.domain.model.login.User
import io.reactivex.Observable
import io.reactivex.Single

class ChatDatasourceImpl(private val apiChat: ApiChat, preferences: SharedPreferences) :
    ChatDataSource {
    private var userSession: User? = mapFromPreference(preferences)
    override fun sendNewToken(token: String): Single<ApiSendMessageChat> {
        return apiChat.sendTokenFirebase(
            userSession?.urlSession
            , BuildConfig.tokenDinan
            , userSession?.cif
            , userSession?.userToken
            , DONDE_TOKEN_FIREBASE
            , OPC_TOKEN_FIREBASE
            , token
        )
    }


    override fun allChats(): Observable<ApiChatResponse> {
        return apiChat.getApiChat(
            userSession?.urlSession,
            BuildConfig.tokenDinan,
            userSession?.cif,
            userSession?.userToken,
            DONDE_CHAT,
            OPC_CHAT
        )
    }

    override fun sendMessage(message: String): Observable<ApiSendMessageChat> {
        return apiChat.sendMessageChat(
            userSession?.urlSession
            , BuildConfig.tokenDinan
            , userSession?.cif
            , userSession?.userToken
            , DONDE_CHAT_SEND_TEXT
            , OPC_CHAT_SEND_TEXT
            , message
        )
    }


}