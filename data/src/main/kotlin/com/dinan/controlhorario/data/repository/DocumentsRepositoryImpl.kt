/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.repository


import android.net.Uri
import com.dinan.controlhorario.data.core.map.*
import com.dinan.controlhorario.data.datasource.documents.FileDatasource
import com.dinan.controlhorario.data.datasource.documents.FolderDatasource
import com.dinan.controlhorario.data.datasource.documents.UploadFile
import com.dinan.controlhorario.domain.model.documents.FileDocument
import com.dinan.controlhorario.domain.model.documents.FolderDocument
import com.dinan.controlhorario.domain.model.documents.UploadDocument
import com.dinan.controlhorario.domain.repository.DocumentsRepository
import io.reactivex.Observable
import java.io.File

class DocumentsRepositoryImpl(
    private val folderDatasource: FolderDatasource,
    private val fileDatasource: FileDatasource,
    private val uploadDatasource: UploadFile
) : DocumentsRepository {

    override fun uploadDocument(
        filename: String,
        pathFile: String,
        fileImage: File?,
        contentURI: Uri
    ): Observable<UploadDocument> {
        return uploadDatasource.uploadFile(filename, pathFile, fileImage, contentURI)
            .map {
                it.error?.let { error -> mapErrorUploadFile(error) }
                mapUploadFromApi(it)
            }
    }

    override fun folderDocuments(): Observable<ArrayList<FolderDocument>> {
        return try { folderDatasource.foldersDocuments()
                .map {
                    it.error?.let { error -> mapError(error) }
                    mapFolderFromApi(it)
                }
        } catch (exception: Exception) {
            Observable.error(exception)
        }
    }

    override fun fildersDocuments(id: String, directorio: String): Observable<ArrayList<FileDocument>> {
        return fileDatasource.fileDocuments(id, directorio).map { mapFileFromApi(it) }
    }
}