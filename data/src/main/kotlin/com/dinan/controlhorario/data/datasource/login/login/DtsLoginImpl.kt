/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.datasource.login.login


import com.dinan.controlhorario.data.apientities.login.ApiGetHost
import com.dinan.controlhorario.data.apientities.login.ApiUser
import com.dinan.controlhorario.data.apirest.ApiLogin
import com.dinan.controlhorario.domain.BuildConfig
import com.dinan.controlhorario.domain.extension.empty
import com.dinan.controlhorario.domain.usecase.login.GetLoginUseCase
import io.reactivex.Observable


class DtsLoginImpl(private val apiLogin: ApiLogin): DtsLogin {

    override fun getLogin(paramsLogin: GetLoginUseCase.ParamsLogin, apiData: ApiGetHost): Observable<ApiUser> {
        return apiLogin.getLogin(
            changeUrl(apiData.url),
            BuildConfig.tokenDinan,
            paramsLogin.cifEmpresa,
            paramsLogin.emailUser,
            paramsLogin.passUser,
            apiData.acceso ?: String.empty()
        )
    }

    override fun getHost(paramsLogin: GetLoginUseCase.ParamsLogin): Observable<ApiGetHost> {
        return apiLogin.getData(BuildConfig.tokenDinan, paramsLogin.cifEmpresa)
    }

    private fun changeUrl(url: String?): String {
        return url?.plus("/") ?: String.empty()
    }
}