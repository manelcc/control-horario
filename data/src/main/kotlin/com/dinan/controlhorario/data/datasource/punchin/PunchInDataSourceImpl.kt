/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.datasource.punchin

import android.content.SharedPreferences
import android.location.Location
import android.util.Log
import com.dinan.controlhorario.data.BuildConfig
import com.dinan.controlhorario.data.apientities.punchin.events.ApiEventPunchIn
import com.dinan.controlhorario.data.apientities.punchin.updatePunchIn.ApiUpdatePunchIn
import com.dinan.controlhorario.data.apientities.punchin.updatefiles.ApiResponseUploadFilePunchIn
import com.dinan.controlhorario.data.apirest.ApiPunchIn
import com.dinan.controlhorario.data.apirest.OpcApis
import com.dinan.controlhorario.data.apirest.OpcApis.DONDE_CANPUNCHIN
import com.dinan.controlhorario.data.apirest.OpcApis.OPC_CAN_PUNCHIN
import com.dinan.controlhorario.data.core.map.mapFromPreference
import com.dinan.controlhorario.data.datasource.documents.UploadFileImpl
import com.dinan.controlhorario.data.extension.EVENTOS
import com.dinan.controlhorario.domain.model.login.User
import io.reactivex.Observable
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File

class PunchInDataSourceImpl(private val api: ApiPunchIn, preferences: SharedPreferences) : PunchInDatasource {
    private var userSession: User? = mapFromPreference(preferences)

    override fun eventPunchIn(): Observable<ApiEventPunchIn> {
        return api.getEventPunchIn(
            userSession?.urlSession,
            BuildConfig.tokenDinan,
            userSession?.cif,
            userSession?.userToken, OpcApis.DONDE, EVENTOS
        )
    }

    override fun updatePunchIn(statePunchIn: String, location: Location): Observable<ApiUpdatePunchIn> {
        Log.e("Location datasource", "la localizacion longitud ${location.longitude} y la latitud ${location.latitude}")
        return api.canPunchIn(
            userSession?.urlSession, BuildConfig.tokenDinan,
            userSession?.cif,
            userSession?.userToken, DONDE_CANPUNCHIN, OPC_CAN_PUNCHIN
        )
            .concatMap { canPunchIn ->
                when (canPunchIn.fichar) {
                    true -> callApiPunchIn(statePunchIn, location)
                    false -> callApiPunchIn(statePunchIn, location)//emitterErrorPunchIn(canPunchIn.estado)
                }
            }
    }

    private fun callApiPunchIn(statePunchIn: String, location: Location): Observable<ApiUpdatePunchIn> {
        return api.updatePunchIn(
            userSession?.urlSession,
            BuildConfig.tokenDinan,
            userSession?.cif,
            userSession?.userToken,
            OpcApis.DONDE,
            statePunchIn,
            location.latitude,
            location.longitude,
            location.accuracy
        )

    }

    /**
     * Datasource Upload Files
     */
    override fun updateFilePunchIn(
        cause: Int,
        text: String,
        file: File?,
        filename: String
    ): Observable<ApiResponseUploadFilePunchIn> {
        return if (file == null) {
            api.uploadFilePunchInNotFile(userSession?.urlSession, getMap(cause, text))
        } else {
            api.uploadFilePunchIn(userSession?.urlSession, getMap(cause, text), getFile(file))
        }
    }

    private fun getFile(file: File?): MultipartBody.Part {
        return MultipartBody.Part.createFormData(
            "file"
            , "provisionalFilename".plus(".").plus(file?.extension)
            , RequestBody.create(MediaType.parse("*/*"), file?.absoluteFile)
        )
    }

    private fun getMap(cause: Int, text: String): Map<String, RequestBody> {
        val map = HashMap<String, RequestBody>()
        map.put("dinan", RequestBody.create(MediaType.parse(UploadFileImpl.PLAINTEXT), BuildConfig.tokenDinan))
        map.put("cif", RequestBody.create(MediaType.parse(UploadFileImpl.PLAINTEXT), ""))
        map.put("utoken", RequestBody.create(MediaType.parse(UploadFileImpl.PLAINTEXT), userSession?.userToken))
        map.put(
            "donde",
            RequestBody.create(MediaType.parse(UploadFileImpl.PLAINTEXT), OpcApis.DONDE_PUNCHIN_UPLOAD.toString())
        )
        map.put("opc", RequestBody.create(MediaType.parse(UploadFileImpl.PLAINTEXT), OpcApis.OPC_PUNCHIN_UPLOAD))
        map.put("causa", RequestBody.create(MediaType.parse(UploadFileImpl.PLAINTEXT), cause.toString()))
        map.put("comentario", RequestBody.create(MediaType.parse(UploadFileImpl.PLAINTEXT), text))
        return map
    }

}
