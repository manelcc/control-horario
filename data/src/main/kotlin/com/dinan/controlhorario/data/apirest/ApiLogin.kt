/*
 * Copyright (C) 2019.  Manel Cabezas Caldero
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.apirest

import com.dinan.controlhorario.data.apientities.login.ApiGetHost
import com.dinan.controlhorario.data.apientities.login.ApiUser
import io.reactivex.Observable
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Url


interface ApiLogin {

    /**
     * Interfaz para obtener el host en funcion del CIF de la empresa
     * Llamada para comprobar que existe el CIF dado de alta.
     */
    @FormUrlEncoded
    @POST("43522d683c1ff931ca0a2f2368deeadb/index.php")
    fun getData(@Field("dinan") token: String, @Field("cif") cif: String): Observable<ApiGetHost>

    /**
     * Interfaz para obtener el host en funcion del CIF de la empresa
     * Llamar a la url del cliente
     */
    @FormUrlEncoded
    @POST
    fun getLogin(
        @Url url: String,
        @Field("dinan") token: String,
        @Field("cif") cif: String,
        @Field("user_email") userEmail: String,
        @Field("user_pass") userPass: String,
        @Field("acceso") acceso: String
    ): Observable<ApiUser>
}