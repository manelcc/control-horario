/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.repository

import android.annotation.SuppressLint
import android.content.SharedPreferences
import com.dinan.controlhorario.data.apientities.login.ApiGetHost
import com.dinan.controlhorario.data.apientities.login.ApiUser
import com.dinan.controlhorario.data.core.map.mapError
import com.dinan.controlhorario.data.core.map.mapErrorGetHost
import com.dinan.controlhorario.data.core.map.mapFromApi
import com.dinan.controlhorario.data.datasource.login.login.DtsLogin
import com.dinan.controlhorario.domain.model.login.User
import com.dinan.controlhorario.domain.repository.LoginRepository
import com.dinan.controlhorario.domain.usecase.login.GetLoginUseCase
import io.reactivex.Observable

class LoginRepositoryImpl(
    private val dtsLogin: DtsLogin,
    private val preference: SharedPreferences
) : LoginRepository {


    @SuppressLint("ApplySharedPref")
    override fun getLogIn(paramsLogin: GetLoginUseCase.ParamsLogin): Observable<User> {
        return dtsLogin.getHost(paramsLogin)
            .concatMap { apiData: ApiGetHost ->
                apiData.error?.let { mapErrorGetHost(apiData.error) }
                dtsLogin.getLogin(paramsLogin, apiData)
                    .map { data: ApiUser ->
                        data.error?.let { mapError(it) }
                        apiData.acceso?.let { mapFromApi(preference, apiData, data, it) }
                    }
            }
    }

}