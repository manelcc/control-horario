/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.core.map

import android.annotation.SuppressLint
import com.dinan.controlhorario.data.apientities.calendar.ApiCalendarResponse
import com.dinan.controlhorario.domain.extension.empty
import com.dinan.controlhorario.domain.model.calendario.CalendarList
import com.dinan.controlhorario.domain.model.calendario.Calendario
import java.text.SimpleDateFormat
import java.util.*

fun mapFromApi(apiCalendar: ApiCalendarResponse): Calendario {

    val lista = apiCalendar.apiCalendario?.map { CalendarList(getCalendar(it.dia), getDayCalendar(it.dia),
        getDayWeek(it.dia),it.estado,it.horas,it.canEdit,it.turno) } as ArrayList<CalendarList>

    return Calendario(apiCalendar.apiCalendario.first().dia, lista, apiCalendar.apisMensajes.last().noleidos?:String.empty())
}

fun getDayWeek(dia: String): String {
    var dayOfWeeK: String = String.empty()
    val calendar = getCalendar(dia)
    val value = calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault())
    value?.let { dayOfWeeK = it }
    return dayOfWeeK
}

fun getDayCalendar(dia: String): String {
    val calendar = getCalendar(dia)
    val value = calendar.get(Calendar.DAY_OF_MONTH)
    if (value < 10) {
        return "0".plus(value)
    } else {
        return value.toString()
    }
}


@SuppressLint("SimpleDateFormat")
fun getCalendar(dia: String?): Calendar {
    val cal: Calendar = Calendar.getInstance()
    val sdf = SimpleDateFormat("yyyy-MM-dd")
    val date: Date? = dia?.let { sdf.parse(it) }
    date?.let { it ->

        cal.time = it
    }
    return cal
}