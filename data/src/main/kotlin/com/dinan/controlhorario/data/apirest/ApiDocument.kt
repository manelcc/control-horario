/*
 * Copyright (C) 2019.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.apirest

import com.dinan.controlhorario.data.apientities.document.ApiResponUploadFile
import com.dinan.controlhorario.data.apientities.document.ApiResponseDocument
import com.dinan.controlhorario.data.apientities.document.ApiResponseFile
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface ApiDocument {

    /**
     * Interfaz para obtener las carpetas a mostrar
     */
    @FormUrlEncoded
    @POST
    fun getFolder(
        @Url url: String?,
        @Field("dinan") token: String?,
        @Field("cif") cif: String?,
        @Field("utoken") userToken: String?,
        @Field("donde") dondeFolder: Int,
        @Field("opc") opcFolder: String
    ): Observable<ApiResponseDocument>

    /**
     * Interfaz para obtener las archivos dentro de una carpeta
     */
    @FormUrlEncoded
    @POST
    fun getFile(
        @Url url: String?,
        @Field("dinan") token: String?,
        @Field("cif") cif: String?,
        @Field("utoken") userToken: String?,
        @Field("donde") dondeFile: Int,
        @Field("opc") opcFile: String,
        @Field("id") id: String,
        @Field("directorio") directorio: String
    ): Observable<ApiResponseFile>


    @Multipart
    @POST
    fun uploadFile(
        @Url url: String?,
        @PartMap  partMap : @JvmSuppressWildcards Map<String, RequestBody>,
        @Part  file : MultipartBody.Part
    ) :Observable<ApiResponUploadFile>
}