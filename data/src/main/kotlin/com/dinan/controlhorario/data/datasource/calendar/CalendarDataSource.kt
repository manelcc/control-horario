/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.datasource.calendar

import com.dinan.controlhorario.data.apientities.calendar.ApiCalendarResponse
import com.dinan.controlhorario.data.apientities.calendar.ApiChangeHourResponse
import com.dinan.controlhorario.data.apientities.calendar.ApiGetHours
import io.reactivex.Observable

interface CalendarDataSource {
    fun calendar(fecha: String): Observable<ApiCalendarResponse>
    fun getHours(fecha: String): Observable<ApiGetHours>
    fun changeHours(paramsRequest: Map<String, String>): Observable<ApiChangeHourResponse>
}