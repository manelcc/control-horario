/*
 * Copyright (C) 2020.  Manel Cabezas 
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.repository.helpermap

import com.dinan.controlhorario.data.apientities.login.ApiLopdLogoEntity
import com.dinan.controlhorario.data.core.map.mapError
import com.dinan.controlhorario.domain.model.login.User

object HelperLegalRepository {

    fun mapLogoUrl(userSession: User?,apiLopdLogo: ApiLopdLogoEntity): Pair<String,String> {
        apiLopdLogo.error?.let { mapError(it) }
        userSession?.urlLogo = apiLopdLogo.logo
        return apiLopdLogo.logo to apiLopdLogo.lopd
    }


}