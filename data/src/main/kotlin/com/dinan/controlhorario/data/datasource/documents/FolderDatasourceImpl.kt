/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.datasource.documents

import android.content.SharedPreferences
import com.dinan.controlhorario.data.BuildConfig
import com.dinan.controlhorario.data.apientities.document.ApiResponseDocument
import com.dinan.controlhorario.data.apirest.ApiDocument
import com.dinan.controlhorario.data.apirest.OpcApis.DONDE_FOLDER
import com.dinan.controlhorario.data.apirest.OpcApis.OPC_FOLDER
import com.dinan.controlhorario.data.core.map.mapFromPreference
import com.dinan.controlhorario.domain.model.login.User
import io.reactivex.Observable

class FolderDatasourceImpl(private val api: ApiDocument, preferences: SharedPreferences) : FolderDatasource {

    private var userSession: User? = mapFromPreference(preferences)
    override fun foldersDocuments(): Observable<ApiResponseDocument> {
        return api.getFolder(
            userSession?.urlSession,
            BuildConfig.tokenDinan,
            userSession?.cif,
            userSession?.userToken,
            DONDE_FOLDER,
            OPC_FOLDER
        )
    }
}