package com.dinan.controlhorario.data.apientities.calendar

import com.google.gson.annotations.SerializedName


data class ApiChangeHourResponse(

	@field:SerializedName("utoken")
	val utoken: String? = null,

	@field:SerializedName("insert")
	val insert: List<InsertItem?>? = null


)