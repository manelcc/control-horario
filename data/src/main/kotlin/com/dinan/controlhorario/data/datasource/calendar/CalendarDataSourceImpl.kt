/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.datasource.calendar

import android.content.SharedPreferences
import com.dinan.controlhorario.data.BuildConfig
import com.dinan.controlhorario.data.apientities.calendar.ApiCalendarResponse
import com.dinan.controlhorario.data.apientities.calendar.ApiChangeHourResponse
import com.dinan.controlhorario.data.apientities.calendar.ApiGetHours
import com.dinan.controlhorario.data.apirest.ApiCalendar
import com.dinan.controlhorario.data.apirest.OpcApis.DONDE_CALENDAR
import com.dinan.controlhorario.data.apirest.OpcApis.DONDE_CHANGEHOURS
import com.dinan.controlhorario.data.apirest.OpcApis.DONDE_GETHOURS
import com.dinan.controlhorario.data.apirest.OpcApis.OPC_CALENDAR
import com.dinan.controlhorario.data.apirest.OpcApis.OPC_CHANGEHOURS
import com.dinan.controlhorario.data.apirest.OpcApis.OPC_GETHOURS
import com.dinan.controlhorario.data.core.map.mapFromPreference
import com.dinan.controlhorario.domain.model.login.User
import io.reactivex.Observable

class CalendarDataSourceImpl(private val apiCalendar: ApiCalendar, preferences: SharedPreferences) : CalendarDataSource {

    private var userSession: User? = mapFromPreference(preferences)

    override fun calendar(fecha: String): Observable<ApiCalendarResponse> {
        return apiCalendar.getCalendar(
            userSession?.urlSession,
            BuildConfig.tokenDinan,
            userSession?.cif,
            userSession?.userToken,
            DONDE_CALENDAR,
            OPC_CALENDAR,
            fecha
        )
    }

    override fun getHours(fecha: String): Observable<ApiGetHours> {
        return apiCalendar.getHour(
            userSession?.urlSession,
            BuildConfig.tokenDinan,
            userSession?.cif,
            userSession?.userToken,
            DONDE_GETHOURS,
            OPC_GETHOURS,
            fecha
        )
    }

    override fun changeHours(paramsRequest: Map<String, String>): Observable<ApiChangeHourResponse> {
        return apiCalendar.changeHour(
            userSession?.urlSession,
            BuildConfig.tokenDinan,
            userSession?.cif,
            userSession?.userToken,
            DONDE_CHANGEHOURS,
            OPC_CHANGEHOURS,
            paramsRequest
        )
    }
}