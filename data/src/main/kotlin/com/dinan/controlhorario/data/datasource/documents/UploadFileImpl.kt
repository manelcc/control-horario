/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.datasource.documents

import android.content.Context
import android.content.SharedPreferences
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import com.dinan.controlhorario.data.BuildConfig
import com.dinan.controlhorario.data.apientities.document.ApiResponUploadFile
import com.dinan.controlhorario.data.apirest.ApiDocument
import com.dinan.controlhorario.data.apirest.OpcApis.DONDE_UPLOADFILE
import com.dinan.controlhorario.data.apirest.OpcApis.OPC_UPLOADFILE
import com.dinan.controlhorario.data.core.map.mapFromPreference
import com.dinan.controlhorario.domain.model.login.User
import io.reactivex.Observable
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream
import java.io.File
import kotlin.math.roundToInt


@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class UploadFileImpl(
    private val context: Context,
    private val api: ApiDocument,
    preferences: SharedPreferences
) : UploadFile {

    private var userSession: User? = mapFromPreference(preferences)
    companion object {
        const val PLAINTEXT = "text/plain"
    }

    override fun uploadFile(
        filename: String,
        pathFile: String,
        fileImage: File?,
        contentURI: Uri
    ): Observable<ApiResponUploadFile> {
        return api.uploadFile(
            userSession?.urlSession
            , getMap(filename)
            , getFile(filename, pathFile, fileImage,contentURI)
        )
    }

    private fun getMap(filename: String): Map<String, RequestBody> {
        val map = HashMap<String, RequestBody>()
        map.put("dinan", RequestBody.create(MediaType.parse(PLAINTEXT), BuildConfig.tokenDinan))
        map.put("cif", RequestBody.create(MediaType.parse(PLAINTEXT), userSession?.cif))
        map.put("utoken", RequestBody.create(MediaType.parse(PLAINTEXT), userSession?.userToken))
        map.put(
            "donde",
            RequestBody.create(MediaType.parse(PLAINTEXT), DONDE_UPLOADFILE.toString())
        )
        map.put("opc", RequestBody.create(MediaType.parse(PLAINTEXT), OPC_UPLOADFILE))
        map.put("file_name", RequestBody.create(MediaType.parse(PLAINTEXT), filename))
        return map
    }

    private fun getFile(
        filename: String,
        pathFile: String,
        fileImage: File?,
        contentURI: Uri
    ): MultipartBody.Part {
        if (fileImage == null) {
            var bitmap: Bitmap? = null
            //camera
            val file = File(pathFile)
            Log.e("manel","tamaño camera ${getSizeFile(file)}")
            resizeBitmap(file, bitmap, pathFile)
            Log.e("manel","tamaño camera aplicando reescala ${getSizeFile(file)}")
            val requestBody = RequestBody.create(MediaType.parse("image/jpg"), file)
            val body = MultipartBody.Part.createFormData("file", filename, requestBody)
            return body
        } else {
            //gallery
            return MultipartBody.Part.createFormData(
                "file"
                , filename
                , RequestBody.create(MediaType.parse("*/*"), fileImage)
            )
        }
    }


    fun getPath(uri: Uri?): String? {
        val projection = arrayOf(MediaStore.Images.Media.DATA)
        val cursor: Cursor = context.contentResolver.query(uri!!, projection, null, null, null) ?: return null
        val index: Int = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
        cursor.moveToFirst()
        val s: String = cursor.getString(index)
        cursor.close()
        return s
    }

    fun getSizeFile(file: File): String {
        val size: Long = file.length() / 1024 // Get size and convert bytes into Kb.

        return if (size >= 1024) {
            (size / 1024).toString() + " Mb"
        } else {
            "$size Kb"
        }
    }

    private fun resizeBitmap(file: File, bitmap: Bitmap?, pathFile: String) {
        var bitmap = bitmap
        val bytes = ByteArrayOutputStream()

        if (file.exists()) {
            bitmap = BitmapFactory.decodeFile(pathFile)
            try {
                bitmap?.let {
                    val newWith: Int = (it.width * 0.50).roundToInt()
                    val newHeight: Int = (it.height * 0.50).roundToInt()
                    Bitmap.createScaledBitmap(it, newWith, newHeight, true)
                    it.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
                    file.writeBytes(bytes.toByteArray())
                }
            }catch (exception: Exception){
               Log.e(javaClass.simpleName,"exception write camera ${exception.message}")
            }

        }
    }

}