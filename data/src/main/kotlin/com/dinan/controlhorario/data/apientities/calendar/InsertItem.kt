package com.dinan.controlhorario.data.apientities.calendar


import com.google.gson.annotations.SerializedName

data class InsertItem(

	@field:SerializedName("hora_old")
	val horaOld: String? = null,

	@field:SerializedName("resultado")
	val resultado: String? = null,

	@field:SerializedName("hora_new")
	val horaNew: String? = null,

	@field:SerializedName("key")
	val key: String? = null
)