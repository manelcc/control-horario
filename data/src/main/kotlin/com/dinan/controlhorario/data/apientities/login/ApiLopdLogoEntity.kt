/*
 * Copyright (C) 2019.  Daniel Jimenez
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.apientities.login


import com.google.gson.annotations.SerializedName

data class ApiLopdLogoEntity(
        @SerializedName("cif")
        val cif: String, // 32044652V
        @SerializedName("logo")
        val logo: String, // https://ch.dinaninformatica.es/images/logo_app.jpg
        @SerializedName("lopd")
        val lopd: String, // Responsable: DINAN APLICACIONES TECNOLOGICAS S.L.<br> Finalidad:<br> Gestionar la relación laboral; Legitimación: Ejecución de un contrato, cumplimiento de una obligación legal; Destinatarios: Están previstas cesiones de datos a: Administración Tributaria, Seguridad Social y Mutua, Bancos y entidades financieras, Fundación estatal para la formación en el empleo (Fundae); Se tomarán decisiones automatizadas: la empresa utiliza una aplicación de control horario para sus trabajadores, si el trabajador llega tarde un determinado número de veces al mes la empresa podrá tomar medidas disciplinarias.<br> Derechos:<br> Tiene derecho a acceder, rectificar y suprimir los datos, así como otros derechos, indicados en la información adicional, que puede ejercer dirigiéndose a <a href="mailto:alicia@dinan.es">alicia@dinan.es</a> o Avd Italia nº 3 local 2 11205 Algeciras Cadiz; Procedencia: El propio interesado; Información adicional:     <a href="www.dinan.es">www.dinan.es</a>.
        @SerializedName("utoken")
        val utoken: String, // MjU6YXJtYW5kb0Bicm9uY2EuZXM6MjU=
        @SerializedName("version")
        val version: String, // 1.0.0
        @field:SerializedName("error")
        val error: String? = null
)