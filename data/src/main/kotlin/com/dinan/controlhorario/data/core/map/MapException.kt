/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

@file:Suppress("DUPLICATE_LABEL_IN_WHEN")

package com.dinan.controlhorario.data.core.map

import com.dinan.controlhorario.data.exception.*

fun mapError(apiError: String) {
    when (apiError) {
        TypeError.TOKEN_ERROR -> throw ErrorCli001()
        TypeError.NOTACTIVE_USER -> throw ErrorCli002()
        TypeError.ERROR_LOGIN -> throw ErrorCli003()
        TypeError.NOT_LOGIN -> throw ErrorCli004()
        TypeError.NOT_TOKEN -> throw ErrorCli005()
        TypeError.NOT_TOKENDINAN -> throw ErrorCli000()
        TypeError.NOT_CAN_PUNCHIN -> throw ErrorCli000()
        else -> throw ErrorNotDefine()
    }
}

fun mapErrorGetHost(apiError: String) {
    when (apiError) {
        TypeErrorApiGetHost.NO_EXISTE_CIF -> throw ErrorCIF()
        TypeErrorApiGetHost.CIF_NORECIBIDO -> throw ErrorNotRecipeCIF()
        TypeErrorApiGetHost.SIN_TOKEN -> throw ErrorNotToken()
    }
}

fun mapErrorUploadFile(apiError: String) {
    when (apiError) {
        TypeErrorUploadfile.UP_400_00 -> throw ErrorUP400_00()
        TypeErrorUploadfile.UP_400_01 -> throw ErrorUP400_01()
        TypeErrorUploadfile.UP_400_02 -> throw ErrorUP400_02()
        TypeErrorUploadfile.UP_400_03 -> throw ErrorUP400_03()
        TypeErrorUploadfile.UP_400_04 -> throw ErrorUP400_04()
        else -> throw ErrorNotDefine()
    }
}

fun mapErrorChat(apiError: String) {
    when (apiError) {
        TypeErrorChat.CHATERROR_INSERT_300_30_28 -> throw ErrorChatInsertServer()
        TypeErrorChat.CHATERROR_EMPTY_300_30_30 -> throw ErrorChatEmpty()
    }
}

fun mapErrorCalendar(apiError: String) {
    when (apiError) {
        TypeErrorCalendar.CALENDAR_ERROR -> throw CalendarError()
        else -> throw ErrorNotDefine()
    }
}

fun mapErrorPunchInUpload(apiError: String) {
    when (apiError) {
        TypeErrorPunchInUpload.ERROR_DIRECTORIO_NO_EXISTE -> throw ErrorPunchInUploadGeneric()
        TypeErrorPunchInUpload.ERROR_INSERTAR_IMAGEN_BD -> throw ErrorPunchInUploadGeneric()
        TypeErrorPunchInUpload.ERROR_MOVER_IMAGEN -> throw ErrorPunchInUploadGeneric()
        TypeErrorPunchInUpload.ERROR_SIN_PERMISOS -> throw ErrorPunchInUploadSinPermisos()
        TypeErrorPunchInUpload.IMAGEN_DUPLICADA -> throw ErrorPunchInUploadImagen()
    }
}

fun mapErrorPunchIn(codeError: String, textError: String) {
    throw ErrorPunchIn(textError)
}