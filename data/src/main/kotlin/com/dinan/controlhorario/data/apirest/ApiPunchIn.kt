/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.apirest

import com.dinan.controlhorario.data.apientities.punchin.events.ApiEventPunchIn
import com.dinan.controlhorario.data.apientities.punchin.updatePunchIn.ApiCanPunchIn
import com.dinan.controlhorario.data.apientities.punchin.updatePunchIn.ApiUpdatePunchIn
import com.dinan.controlhorario.data.apientities.punchin.updatefiles.ApiResponseUploadFilePunchIn
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface ApiPunchIn {


    /**
     * Interfaz para obtener los apiEventos
     */
    @FormUrlEncoded
    @POST
    fun getEventPunchIn(
        @Url url: String?,
        @Field("dinan") token: String?,
        @Field("cif") cif: String?,
        @Field("utoken") userToken: String?,
        @Field("donde") donde: Int,
        @Field("opc") opc:  String
    ): Observable<ApiEventPunchIn>


    /**
     * Interfaz para preguntar si se puede fichar
     */
    @FormUrlEncoded
    @POST
    fun canPunchIn(
        @Url url: String?,
        @Field("dinan") token: String?,
        @Field("cif") cif: String?,
        @Field("utoken") userToken: String?,
        @Field("donde") donde: Int,
        @Field("opc")  opcCanPunchIn:  String
    ): Observable<ApiCanPunchIn>


    /**
     * Interfaz para Fichar {INICIAR,PAUSAR,REANUDAR,FINALIZAR}
     */
    @FormUrlEncoded
    @POST
    fun updatePunchIn(
        @Url url: String?,
        @Field("dinan") token: String?,
        @Field("cif") cif: String?,
        @Field("utoken") userToken: String?,
        @Field("donde") donde: Int,
        @Field("opc") opc:  String,
        @Field("latitud") latitud:  Double?,
        @Field("longitud") longitud:  Double?,
        @Field("accuracy") accuracy:  Float?
    ): Observable<ApiUpdatePunchIn>

    @Multipart
    @POST
    fun uploadFilePunchIn(
        @Url url: String?,
        @PartMap partMap : @JvmSuppressWildcards Map<String, RequestBody>,
        @Part file : MultipartBody.Part
    ) :Observable<ApiResponseUploadFilePunchIn>

    @Multipart
    @POST
    fun uploadFilePunchInNotFile(
        @Url url: String?,
        @PartMap partMap : @JvmSuppressWildcards Map<String, RequestBody>
    ) :Observable<ApiResponseUploadFilePunchIn>

}