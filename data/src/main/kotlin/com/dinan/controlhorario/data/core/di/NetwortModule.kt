/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.core.di


import com.dinan.controlhorario.data.BuildConfig
import com.dinan.controlhorario.data.apirest.*
import com.dinan.controlhorario.data.core.di.Properties.BASEURL
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object Properties {
    const val BASEURL = "https://controlhorario-dinan.es/"
}


private val loggingInterceptor = HttpLoggingInterceptor().apply {
    if(BuildConfig.DEBUG) {
        level = HttpLoggingInterceptor.Level.BODY
    }
}

private val client = OkHttpClient.Builder()
    .connectTimeout(60, TimeUnit.SECONDS)
    .readTimeout(45, TimeUnit.SECONDS)
    .writeTimeout(25, TimeUnit.SECONDS)
    .addInterceptor(loggingInterceptor)
    .build()

private val gson = GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES).create()

inline fun <reified T> createWebService(okHttpClient: OkHttpClient, url: String): T {
    val retrofit = Retrofit.Builder()
        .baseUrl(url)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()
    return retrofit.create(T::class.java)
}


val moduleNetwork = module {
    factory {
        createWebService<ApiLogin>(
            client,
            BASEURL
        )
    }
    factory {
        createWebService<ApiPunchIn>(
            client,
            BASEURL
        )
    }
    factory {
        createWebService<ApiLopdLogo>(
            client,
            BASEURL
        )
    }
    factory {
        createWebService<ApiChangePass>(
            client,
            BASEURL
        )
    }
    factory {
        createWebService<ApiCalendar>(
            client,
            BASEURL
        )
    }

    factory {
        createWebService<ApiDocument>(
            client,
            BASEURL
        )
    }

    factory {
        createWebService<ApiChat>(
            client,
            BASEURL
        )
    }




}




