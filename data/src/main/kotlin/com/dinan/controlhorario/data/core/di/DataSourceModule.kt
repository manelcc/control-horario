/*
 * Copyright (C) 2019.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.core.di

import com.dinan.controlhorario.data.datasource.calendar.CalendarDataSource
import com.dinan.controlhorario.data.datasource.calendar.CalendarDataSourceImpl
import com.dinan.controlhorario.data.datasource.chat.ChatDataSource
import com.dinan.controlhorario.data.datasource.chat.ChatDatasourceImpl
import com.dinan.controlhorario.data.datasource.documents.*
import com.dinan.controlhorario.data.datasource.login.legal.DataSourceLoadLogo
import com.dinan.controlhorario.data.datasource.login.legal.DataSourceLoadLogoImpl
import com.dinan.controlhorario.data.datasource.login.login.DtsLogin
import com.dinan.controlhorario.data.datasource.login.login.DtsLoginImpl
import com.dinan.controlhorario.data.datasource.punchin.PunchInDataSourceImpl
import com.dinan.controlhorario.data.datasource.punchin.PunchInDatasource
import org.koin.dsl.module

val datasourceModule = module {

    factory<DtsLogin> { DtsLoginImpl(get()) }
    factory<DataSourceLoadLogo> { DataSourceLoadLogoImpl(get(), get()) }
    factory<PunchInDatasource> { PunchInDataSourceImpl(get(), get()) }
    factory<CalendarDataSource> { CalendarDataSourceImpl(get(), get()) }
    factory<FolderDatasource> { FolderDatasourceImpl(get(), get()) }
    factory<FileDatasource> { FileDatasourceImpl(get(), get()) }
    factory<UploadFile> { UploadFileImpl(get(),get(),get()) }
    factory<ChatDataSource> { ChatDatasourceImpl(get(),get()) }
}