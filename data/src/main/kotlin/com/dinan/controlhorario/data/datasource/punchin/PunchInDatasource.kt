/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.datasource.punchin

import android.location.Location
import com.dinan.controlhorario.data.apientities.punchin.events.ApiEventPunchIn
import com.dinan.controlhorario.data.apientities.punchin.updatePunchIn.ApiUpdatePunchIn
import com.dinan.controlhorario.data.apientities.punchin.updatefiles.ApiResponseUploadFilePunchIn
import io.reactivex.Observable
import java.io.File

interface PunchInDatasource {
    fun updatePunchIn(statePunchIn: String, location: Location): Observable<ApiUpdatePunchIn>
    fun eventPunchIn(): Observable<ApiEventPunchIn>
    fun updateFilePunchIn(cause:Int,text:String,file: File?,filename: String): Observable<ApiResponseUploadFilePunchIn>
}