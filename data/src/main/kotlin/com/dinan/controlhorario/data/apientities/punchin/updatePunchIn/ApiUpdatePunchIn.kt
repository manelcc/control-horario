/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.apientities.punchin.updatePunchIn


import com.dinan.controlhorario.data.apientities.chat.ApisMensajes
import com.dinan.controlhorario.data.extension.empty
import com.google.gson.annotations.SerializedName

data class ApiUpdatePunchIn(
    @SerializedName("btn")
    val btn: String,
    @SerializedName("cif")
    val cif: String,
    @SerializedName("donde")
    val donde: Int,
    @SerializedName("hora")
    val hora: String,
    @SerializedName("opc")
    val opc: String,
    @SerializedName("utoken")
    val utoken: String,
    @SerializedName("version")
    val version: String,
    @SerializedName("mensajes")
    val apisMensajes: List<ApisMensajes>,
    @SerializedName("totalhoras")
    val totalhoras: String = "0",
    @SerializedName("error")
    val error:String = String.empty(),
    @SerializedName("errortexto")
    val textError:String?

)