/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.apirest

import com.dinan.controlhorario.data.apientities.calendar.ApiCalendarResponse
import com.dinan.controlhorario.data.apientities.calendar.ApiChangeHourResponse
import com.dinan.controlhorario.data.apientities.calendar.ApiGetHours
import io.reactivex.Observable
import retrofit2.http.*


interface ApiCalendar {

    /**
     * Interfaz para obtener el host en funcion del CIF de la empresa
     * Llamar a la url del cliente
     */
    @FormUrlEncoded
    @POST
    fun getCalendar(
        @Url url: String?,
        @Field("dinan") token: String?,
        @Field("cif") cif: String?,
        @Field("utoken") userToken: String?,
        @Field("donde") donde: Int,
        @Field("opc") opcCanPunchIn: String,
        @Field("fecha") fecha: String //formato yyyy-MM-dd
    ): Observable<ApiCalendarResponse>

    @FormUrlEncoded
    @POST
    fun getHour(
        @Url url: String?,
        @Field("dinan") token: String?,
        @Field("cif") cif: String?,
        @Field("utoken") userToken: String?,
        @Field("donde") dondeHours: Int,
        @Field("opc") opcGetHours: String,
        @Field("fecha") fecha: String //formato yyyy-MM-dd
    ): Observable<ApiGetHours>

    @FormUrlEncoded
    @POST
    fun changeHour(
        @Url url: String?,
        @Field("dinan") token: String?,
        @Field("cif") cif: String?,
        @Field("utoken") userToken: String?,
        @Field("donde") dondeHours: Int,
        @Field("opc") opcGetHours: String,
        @FieldMap mapField: Map<String,String>
    ): Observable<ApiChangeHourResponse>
}