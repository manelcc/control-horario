/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.apientities.calendar

import com.dinan.controlhorario.data.apientities.chat.ApisMensajes
import com.google.gson.annotations.SerializedName


data class ApiGetHours(

    @field:SerializedName("horas")
    val horas: List<HorasItem?>?,

    @field:SerializedName("cif")
    val cif: String?,

    @field:SerializedName("donde")
    val donde: String?,

    @field:SerializedName("acceso")
    val acceso: String?,

    @field:SerializedName("mensajes")
    val apisMensajes: List<ApisMensajes>,

    @field:SerializedName("utoken")
    val utoken: String?,

    @field:SerializedName("version")
    val version: String?,

    @field:SerializedName("error")
    val error: String?


)