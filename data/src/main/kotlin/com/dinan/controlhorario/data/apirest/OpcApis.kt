/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.apirest

object OpcApis {
    const val OPC_LOPD_LOGO = "logo-lopd"
    const val OPC_CONFIRM_LOPD = "lopd-ok"
    const val OPC_CHANGE_PASS = "change"
    const val OPC_CAN_PUNCHIN = "fichar"
    const val OPC_CALENDAR = "calendario"
    const val OPC_FOLDER = "default"
    const val OPC_FILE = "carpetas"
    const val OPC_UPLOADFILE = "upload"
    const val OPC_CHAT = "todo"
    const val OPC_CHAT_SEND_TEXT = "insertar"
    const val OPC_TOKEN_FIREBASE = "registrar"
    const val OPC_GETHOURS = "gethours"
    const val OPC_CHANGEHOURS = "changehours"
    const val OPC_PUNCHIN_UPLOAD = "nota"
    const val DONDE : Int= 100
    const val DONDE_TOKEN_FIREBASE: Int = 300
    const val DONDE_CANPUNCHIN : Int= 100
    const val DONDE_CALENDAR : Int= 200
    const val DONDE_FOLDER : Int= 400
    const val DONDE_FILE: Int = 400
    const val DONDE_UPLOADFILE = 400
    const val DONDE_CHAT = 300
    const val DONDE_CHAT_SEND_TEXT = 300
    const val DONDE_LOPD_LOGO  = "000"
    const val DONDE_CHANGE_PASS  = "000"
    const val DONDE_GETHOURS = 200
    const val DONDE_CHANGEHOURS = 200
    const val DONDE_PUNCHIN_UPLOAD = 100





}