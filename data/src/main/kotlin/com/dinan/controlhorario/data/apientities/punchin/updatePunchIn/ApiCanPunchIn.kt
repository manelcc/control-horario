/*
 * Copyright (C) 2019.  Daniel Jimenez
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.apientities.punchin.updatePunchIn


import com.google.gson.annotations.SerializedName

data class ApiCanPunchIn(
    @SerializedName("cif")
    val cif: String, // 32044652V
    @SerializedName("estado")
    val estado: String, // Laboral
    @SerializedName("fichar")
    val fichar: Boolean, // true
    @SerializedName("utoken")
    val utoken: String, // MjU6YXJtYW5kb0Bicm9uY2EuZXM6MjU=
    @SerializedName("version")
    val version: String // 1.0.0
)