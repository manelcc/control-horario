/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.extension

const val PERMISSION_GPS_FINE = android.Manifest.permission.ACCESS_FINE_LOCATION
const val PERMISSION_GPS_COURSE = android.Manifest.permission.ACCESS_COARSE_LOCATION
const val INICIAR = "iniciar"
const val PAUSAR = "pausar"
const val REANUDAR = "reanudar"
const val FINALIZAR = "finalizar"
const val EVENTOS = "eventos"
const val ENDPUNCHIN = ""
const val NONE = "none"
