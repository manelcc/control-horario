/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.repository


import android.location.Location
import com.dinan.controlhorario.data.core.map.mapErrorPunchIn
import com.dinan.controlhorario.data.core.map.mapErrorPunchInUpload
import com.dinan.controlhorario.data.datasource.punchin.PunchInDatasource
import com.dinan.controlhorario.data.repository.helpermap.HelperPunchIn.listEvents
import com.dinan.controlhorario.domain.model.punchin.PunchInModel
import com.dinan.controlhorario.domain.repository.PunchInRepository
import io.reactivex.Observable
import java.io.File

class PunchInRepositoryImpl(private val punchInDatasource: PunchInDatasource) : PunchInRepository {

    override fun updatePunchIn(statePunchIn: String, location: Location): Observable<PunchInModel> {
        return punchInDatasource.updatePunchIn(statePunchIn, location)
                .concatMap { response ->
                    response.textError?.let{mapErrorPunchIn(response.error,it)}
                    loadEvents() }

    }


    override fun loadEvents(): Observable<PunchInModel> {
        return punchInDatasource.eventPunchIn().map { apiEventPunchInt ->
            PunchInModel(
                apiEventPunchInt.botonToShow,
                listEvents(apiEventPunchInt),
                apiEventPunchInt.totalHours ?: "00:00:00"
            )
        }
    }

    override fun uploadCausePunchIn(cause:Int,text:String, file: File?,filename:String): Observable<Boolean> {
        return punchInDatasource.updateFilePunchIn(cause,text,file,filename)
            .map { response ->
                response.error?.let { mapErrorPunchInUpload(it) }
                return@map response.causa
            }
    }

}