/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.exception

object TypeErrorApiGetHost{
    const val NO_EXISTE_CIF= "di-001"
    const val CIF_NORECIBIDO = "di-002"
    const val SIN_TOKEN = "di-000"
}

object TypeError {
    const val NOT_CAN_PUNCHIN = "BussinesError"
    const val TOKEN_ERROR = "cli-001"
    const val NOTACTIVE_USER = "cli-002"
    const val ERROR_LOGIN = "cli-003"
    const val NOT_LOGIN = "cli-004"
    const val NOT_TOKEN = "cli-005"
    const val NOT_TOKENDINAN = "di-000"
}

object TypeErrorUploadfile{
    const val UP_400_00 = "400-UP-00"
    const val UP_400_01 = "400-UP-01"
    const val UP_400_02 = "400-UP-02"
    const val UP_400_03 = "400-UP-03"
    const val UP_400_04 = "400-UP-04"
}

object TypeErrorChat{
    const val CHATERROR_INSERT_300_30_28 = "300-30-28"
    const val CHATERROR_EMPTY_300_30_30 = "300-30-30"
}

object TypeErrorCalendar{
    const val CALENDAR_ERROR = "cal-001"
}

object TypeErrorPunchInUpload{
    const val IMAGEN_DUPLICADA = "100-47-28"
    const val ERROR_INSERTAR_IMAGEN_BD = "100-47-41"
    const val ERROR_MOVER_IMAGEN = "100-47-44"
    const val ERROR_DIRECTORIO_NO_EXISTE = "100-47-48"
    const val ERROR_SIN_PERMISOS = "100-47-56"
}

//ApiGetHost
class ErrorCIF(message: String = "CIF es incorrecto"):Exception(message)
class ErrorNotRecipeCIF(message: String = "No hemos recibido el CIF"):Exception(message)
class ErrorNotToken(message: String = "No existe token"):Exception(message)
//Login
class ErrorCli000(message: String = "No hay token de Dinan" ) : Exception(message)
class ErrorCli001(message: String = "Error al crear el token del usuario" ) : Exception(message)
class ErrorCli002(message: String = "Usuario o contraseña incorrecto, revise los datos" ) : Exception(message)
class ErrorCli003(message: String = "Usuario o contraseña incorrecto, revise los datos" ) : Exception(message)
class ErrorCli004(message: String = "Usuario no logeado pedir login" ) : Exception(message)
class ErrorCli005(message: String = "Sin token para hacer Login" ) : Exception(message)
//PunchIn
class ErrorCanPuchIn(message: String ) : Exception(message)
class ErrorPunchIn(message: String):Exception(message)
//Update Documents
class ErrorUP400_00(message: String = "No se ha recibido ningún documento" ) : Exception(message)
class ErrorUP400_01(message: String = "El documento no es jpg" ) : Exception(message)
class ErrorUP400_02(message: String = "Documento duplicado no se a subido" ) : Exception(message)
class ErrorUP400_03(message: String = "El documento no se ha subido" ) : Exception(message)
class ErrorUP400_04(message: String = "El Documento se ha subido pero no insertado e la Base de Datos" ) : Exception(message)
class ErrorNotDefine(message: String = "Ha surgido un error inesperado, pruebe en unos momentos" ) : Exception(message)
//Chat
class ErrorChatInsertServer(message:String ="Error al insertar el mensaje"):Exception(message)
class ErrorChatEmpty(message: String = "Error texto del mensaje vacío"):Exception(message)
class CalendarError(message: String = "No existen horas para el dia solicitado"):Exception(message)
class ErrorPunchInUploadImagen(message: String = "La imagen esta duplicada"):Exception(message)
class ErrorPunchInUploadGeneric(message: String = "Se a producido un error con la imagen"):Exception(message)
class ErrorPunchInUploadSinPermisos(message: String="Usted no tiene permisos para subir esta petición"):Exception(message)