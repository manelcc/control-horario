/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.repository

import android.content.SharedPreferences
import com.dinan.controlhorario.data.apientities.login.ApiConfirmLopd
import com.dinan.controlhorario.data.datasource.login.legal.DataSourceLoadLogo
import com.dinan.controlhorario.data.repository.helpermap.HelperLegalRepository.mapLogoUrl
import com.dinan.controlhorario.domain.model.login.User
import com.dinan.controlhorario.domain.repository.LegalRepository
import io.reactivex.Observable

class LegalRepositoryImpl(
    private val dataSourceLoadLogo: DataSourceLoadLogo,
    private val userSession: User?,
    private val preference: SharedPreferences
) : LegalRepository {

    override fun loadLopdLogo(): Observable<Pair<String,String>> {
        return dataSourceLoadLogo.loadLopdAndLogo()
                .map { apiLopdLogo -> mapLogoUrl(userSession,apiLopdLogo) }
    }



    override fun acceptLopd(): Observable<Boolean> {
        return dataSourceLoadLogo.confirmLOPD().map { t: ApiConfirmLopd -> t.lopdOk }
    }
}