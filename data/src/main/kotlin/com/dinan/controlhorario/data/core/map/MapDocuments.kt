/*
 * Copyright (C) 2019.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.core.map

import com.dinan.controlhorario.data.apientities.document.ApiResponUploadFile
import com.dinan.controlhorario.data.apientities.document.ApiResponseDocument
import com.dinan.controlhorario.data.apientities.document.ApiResponseFile
import com.dinan.controlhorario.data.extension.formatDefaultPattern
import com.dinan.controlhorario.data.extension.formatOutputYear
import com.dinan.controlhorario.data.extension.formatOutput_dd_MM
import com.dinan.controlhorario.data.extension.getStringDate
import com.dinan.controlhorario.domain.extension.empty
import com.dinan.controlhorario.domain.model.documents.FileDocument
import com.dinan.controlhorario.domain.model.documents.FolderDocument
import com.dinan.controlhorario.domain.model.documents.UploadDocument

fun mapFolderFromApi(responseFolder: ApiResponseDocument): ArrayList<FolderDocument> {
    val list: ArrayList<FolderDocument> = ArrayList()
    responseFolder.carpeta?.forEach {
        it.apply { list.add(FolderDocument(nombre, id, directorio, responseFolder.apisMensajes.last().noleidos?:String.empty())) }
    }
    return list
}

fun mapUploadFromApi(responseFolder: ApiResponUploadFile): UploadDocument {
    return UploadDocument(
        responseFolder.ficheros?.isNotEmpty() ?: false,
        responseFolder.apisMensajes.last().noleidos?:String.empty()
    )
}


fun mapFileFromApi(responseFile: ApiResponseFile): ArrayList<FileDocument> {
    val list: ArrayList<FileDocument> = ArrayList()
    responseFile.ficheros?.forEach {
        it.apply {
            list.add(
                FileDocument(
                    nombre,
                    getStringDate(formatDefaultPattern, formatOutput_dd_MM, fecha),
                    getStringDate(formatDefaultPattern, formatOutputYear, fecha),
                    documento,
                    responseFile.apisMensajes.last().noleidos?:String.empty()
                )
            )
        }
    }
    return list
}




