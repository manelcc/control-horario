/*
 * Copyright (C) 2020.  Manel Cabezas 
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.repository.helpermap

import com.dinan.controlhorario.data.apientities.punchin.events.ApiEventPunchIn
import com.dinan.controlhorario.data.core.map.mapApiListToListEventPunchIn
import com.dinan.controlhorario.domain.model.punchin.EventPunchIn

object HelperPunchIn {

    fun listEvents(apiEventPunchInt: ApiEventPunchIn): ArrayList<EventPunchIn> {
        val list: ArrayList<EventPunchIn> = mapApiListToListEventPunchIn(apiEventPunchInt)
        return if(list.isNotEmpty()) {
            list.reverse()
            list
        }else{
            ArrayList()
        }
    }
}