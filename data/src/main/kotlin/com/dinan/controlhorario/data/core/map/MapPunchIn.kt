/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.core.map

import android.annotation.SuppressLint
import com.dinan.controlhorario.data.apientities.calendar.ApiGetHours
import com.dinan.controlhorario.data.apientities.punchin.events.ApiEventPunchIn
import com.dinan.controlhorario.data.extension.NONE
import com.dinan.controlhorario.domain.extension.empty
import com.dinan.controlhorario.domain.model.punchin.EventPunchIn
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


fun mapFromApi(apiGetHours: ApiGetHours, horas: String): List<EventPunchIn> {
    return apiGetHours.horas?.map {
        EventPunchIn(
            it?.accion ?: ""
            , getDateTime(it?.datetime ?: "")
            , getHourTime(it?.datetime), apiGetHours.apisMensajes.last().noleidos ?: String.empty()
            , getTotalHour(horas)
            , it?.id ?: ""
        )
    }.orEmpty()
}

fun mapApiListToListEventPunchIn(apiValue: ApiEventPunchIn?): ArrayList<EventPunchIn> {
    val list = ArrayList<EventPunchIn>()
    apiValue?.eventos?.let { listEvents ->
        listEvents.filter { it.accion != NONE }
            .map {
                list.add(
                    EventPunchIn(
                        it.accion,
                        getDateTime(it.datetime),
                        getHourTime(it.datetime),
                        apiValue.apisMensajes.last().noleidos ?: String.empty(),
                        getTotalHour(apiValue.totalHours)
                    )
                )
            }
    }


    /*listEvents.forEach {
        if (NONE != it.accion) {
            list.add(
                EventPunchIn(
                    it.accion,
                    getDateTime(it.datetime),
                    getHourTime(it.datetime),
                    apiValue.apisMensajes.last().noleidos ?: String.empty(),
                    getTotalHour(apiValue.totalHours)
                )
            )
        }
    }
}*/
    return list
}

private fun getTotalHour(totalhoras: String?): String {
    totalhoras?.let { return it }
    return "0:00"
}

private fun getDateTime(datetime: String): String {
    val pattern = "yyyy-MM-dd"
    return when (datetime) {
        "none" -> String.empty()
        else -> getParsedTimer(datetime, pattern)
    }
}

private fun getHourTime(datetime: String?): String {
    datetime?.let {
        val pattern = "HH:mm:ss"
        return when (datetime) {
            "none" -> "0:00"
            else -> return getParsedTimer(datetime, pattern)
        }
    }
    return "0:00"
}

@SuppressLint("SimpleDateFormat")
private fun getParsedTimer(aTime: String, pattern: String): String {
    val formatOutput = SimpleDateFormat(pattern)
    val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
    return try {
        formatOutput.format(sdf.parse(aTime)!!)
    } catch (e: ParseException) {
        e.printStackTrace()
        formatOutput.format(Date())
    }
}

