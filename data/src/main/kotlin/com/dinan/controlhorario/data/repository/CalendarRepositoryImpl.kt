/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.repository

import android.util.Log
import com.dinan.controlhorario.data.core.map.mapErrorCalendar
import com.dinan.controlhorario.data.core.map.mapFromApi
import com.dinan.controlhorario.data.datasource.calendar.CalendarDataSource
import com.dinan.controlhorario.data.extension.customDate
import com.dinan.controlhorario.domain.model.calendario.Calendario
import com.dinan.controlhorario.domain.model.punchin.EventPunchIn
import com.dinan.controlhorario.domain.repository.CalendarRepository
import io.reactivex.Observable

class CalendarRepositoryImpl(private val datasoruce: CalendarDataSource) : CalendarRepository {

    val sufixx = "_new"
    val spaceRequest = "^"

    override fun calendar(fecha: String): Observable<Calendario> {
        return datasoruce.calendar(fecha)
            .map { calendarResponse -> mapFromApi(calendarResponse) }
    }

    override fun getHours(fecha: String, horas: String): Observable<List<EventPunchIn>> {
        return datasoruce.getHours(fecha)
            .map { getHoursResponse ->
                getHoursResponse.error?.let { mapErrorCalendar(it) }
                mapFromApi(getHoursResponse, horas)
            }
    }

    override fun changeHours(params: ArrayList<EventPunchIn>, observation: String): Observable<Boolean> {
        return datasoruce.changeHours(getParmasMap(params, observation))
            .map { response ->
                response.insert?.first()?.resultado == "ok"
            }
    }

    private fun getParmasMap(params: java.util.ArrayList<EventPunchIn>, observation: String): Map<String, String> {
        val paramsRequest: MutableMap<String, String> = mutableMapOf()
        params.filter { it.hourSelected.isNotEmpty() }
            .map { item ->
                with(paramsRequest) {
                    put(
                            getPrefix(item.action).plus(item.id).plus(sufixx) ,
                                String.customDate("HH:mm", item.hourTime)
                                    .plus(spaceRequest)
                                    .plus(String.customDate("HH:mm", item.hourSelected))
                                    .plus(spaceRequest)
                                    .plus(observation)
                        )
                }
                Log.e("manel", "size del map en paramRequest ${paramsRequest.size}")

        }
        return paramsRequest
    }

    private fun getPrefix(action: String): String {
        return when (action) {
            "iniciar" -> Prefix.iniciar.value
            "pausar" -> Prefix.pausar.value
            "reanudar" -> Prefix.reaunudar.value
            "finalizar" -> Prefix.finalizar.value
            else -> ""
        }
    }

    enum class Prefix(val value: String) {
        iniciar("a_"),
        pausar("b_"),
        reaunudar("c_"),
        finalizar("d_")
    }
}