/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.core.map

import android.content.SharedPreferences
import com.dinan.controlhorario.data.apientities.login.ApiGetHost
import com.dinan.controlhorario.data.apientities.login.ApiUser
import com.dinan.controlhorario.data.core.base.DataConstants.KEY_USER_PREF
import com.dinan.controlhorario.domain.extension.empty
import com.dinan.controlhorario.domain.model.login.User
import com.google.gson.Gson

fun mapFromPreference(preferences: SharedPreferences): User? {
    return try {
        val stringUserPreference: String? = preferences.getString(KEY_USER_PREF, String.empty())
        val userSession = Gson().fromJson(stringUserPreference, User::class.java)
        userSession
    } catch (exc : Exception){
        null
    }
}

fun mapFromApi(preference: SharedPreferences,apiData: ApiGetHost, data: ApiUser, accessUser: String): User {
    val user=User(
            apiData.url ?: String.empty(),
            data.cif ?: String.empty(),
            data.utoken ?: String.empty(),
            accessUser,
        "0" == data.lopd,
        "0" == data.change
    )

    val serializedUser: String = Gson().toJson(user)
    preference.edit().putString(KEY_USER_PREF, serializedUser).commit()
    return user

}
