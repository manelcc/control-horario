/*
 * Copyright (C) 2019.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.repository

import com.dinan.controlhorario.data.core.map.mapFromApi
import com.dinan.controlhorario.data.datasource.chat.ChatDataSource
import com.dinan.controlhorario.data.exception.ErrorChatEmpty
import com.dinan.controlhorario.data.exception.ErrorChatInsertServer
import com.dinan.controlhorario.data.exception.TypeErrorChat.CHATERROR_EMPTY_300_30_30
import com.dinan.controlhorario.data.exception.TypeErrorChat.CHATERROR_INSERT_300_30_28
import com.dinan.controlhorario.domain.model.chat.MessageChatList
import com.dinan.controlhorario.domain.repository.ChatRepository
import io.reactivex.Observable

@Suppress("DUPLICATE_LABEL_IN_WHEN")
class ChatRespositoryImpl(private val chatDataSource: ChatDataSource) : ChatRepository {

    override fun getAllChats(): Observable<ArrayList<MessageChatList>> {
        return chatDataSource.allChats().map { apiResponse -> mapFromApi(apiResponse) }
    }

    override fun sendMessage(message: String): Observable<ArrayList<MessageChatList>> {
        return chatDataSource.sendMessage(message).concatMap {
            if (!it.ok.equals("1")) {
                it.error?.let { errorString ->
                    when (errorString) {
                        CHATERROR_INSERT_300_30_28 -> throw ErrorChatInsertServer()
                        CHATERROR_EMPTY_300_30_30 -> throw ErrorChatEmpty()
                        else -> throw ErrorChatInsertServer()
                    }
                }
            } else {
                getAllChats()
            }
        }
    }
}


