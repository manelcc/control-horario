/*
 * Copyright (C) 2019.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.apirest

import com.dinan.controlhorario.data.apientities.chat.ApiChatResponse
import com.dinan.controlhorario.data.apientities.chat.ApiSendMessageChat
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import retrofit2.http.Url

interface ApiChat {

    /**
     * Interfaz para obtener los mensajes del chat
     */
    @FormUrlEncoded
    @POST
    fun getApiChat(
        @Url url: String?,
        @Field("dinan") token: String?,
        @Field("cif") cif: String?,
        @Field("utoken") userToken: String?,
        @Field("donde") donde: Int,
        @Field("opc") opc:  String
    ): Observable<ApiChatResponse>

    /**
     * Interfaz para enviar mensajes por el chat
     */
    @FormUrlEncoded
    @POST
    fun sendMessageChat(
        @Url url: String?,
        @Field("dinan") token: String?,
        @Field("cif") cif: String?,
        @Field("utoken") userToken: String?,
        @Field("donde") donde: Int,
        @Field("opc") opc:  String,
        @Field("texto_send") message : String
    ): Observable<ApiSendMessageChat>

    /**
     * Interfaz para enviar registrar token de Firebase
     */
    @FormUrlEncoded
    @POST
    fun sendTokenFirebase(
        @Url url: String?,
        @Field("dinan") token: String?,
        @Field("cif") cif: String?,
        @Field("utoken") userToken: String?,
        @Field("donde") donde: Int,
        @Field("opc") opc:  String,
        @Field("tFirebase") tokenFirebase : String
    ): Single<ApiSendMessageChat>
}