/*
 * Copyright (C) 2019.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.repository

import com.dinan.controlhorario.data.BuildConfig
import com.dinan.controlhorario.data.apirest.ApiChangePass
import com.dinan.controlhorario.data.apirest.OpcApis.DONDE_CHANGE_PASS
import com.dinan.controlhorario.data.apirest.OpcApis.OPC_CHANGE_PASS
import com.dinan.controlhorario.domain.model.login.User
import com.dinan.controlhorario.domain.repository.ChangePassRepository
import io.reactivex.Observable

class ChangePassRepositoryImpl(
    private val apiChangePass: ApiChangePass,
    private val userSession: User?
) : ChangePassRepository {


    override fun changePassword(newPass: String, repeatPass: String): Observable<Boolean> {
        return apiChangePass.changePass(
            userSession?.urlSession,
            BuildConfig.tokenDinan,
            userSession?.cif,
            userSession?.userToken,
            DONDE_CHANGE_PASS,
            OPC_CHANGE_PASS,
            newPass
        ).map { it.changePass }
    }


}
