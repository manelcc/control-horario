/*
 * Copyright (C) 2019.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.repository

import android.content.SharedPreferences
import com.dinan.controlhorario.data.apientities.login.ApiGetHost
import com.dinan.controlhorario.data.datasource.login.login.DtsLogin
import com.dinan.controlhorario.domain.repository.LoginRepository
import com.dinan.controlhorario.domain.usecase.login.GetLoginUseCase
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.util.concurrent.atomic.AtomicInteger

class LoginRepositoryImplTest{

    @Mock
    private lateinit var dtsLogin: DtsLogin
    @Mock
    private lateinit var preferences: SharedPreferences
    //testClass
    private lateinit var repository: LoginRepository


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        repository = LoginRepositoryImpl(dtsLogin, preferences)

    }

    @Test
    fun `check emissions count`() {
        val emissionsCount = AtomicInteger()//(1)
        Observable.range(1, 10)
                .subscribeOn(Schedulers.computation())
                .blockingSubscribe {//(2)
                    _ ->
                    emissionsCount.incrementAndGet()
                }

        assertEquals(10, emissionsCount.get())//(3)
    }

    @Test
    fun getApiHostValues(){
        val apiData: ApiGetHost = getApiHost()
        val params = GetLoginUseCase.ParamsLogin("a", "b", "c")

        whenever(dtsLogin.getHost(params)).thenReturn(Observable.just(getApiHost()))

        val apiGetHostTest = dtsLogin.getHost(params)
        val testObserver = TestObserver<ApiGetHost>()
        apiGetHostTest.subscribe(testObserver)//(1) suscribiendo el observable

        var values = testObserver.values().get(0)

        assertNotNull(values)

    }


    private fun getApiHost(): ApiGetHost {
        return ApiGetHost("acceso", "version", "url")
    }

}