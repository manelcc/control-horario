/*
 * Copyright (C) 2019.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data

import com.google.gson.Gson
import java.io.BufferedReader


fun <T> getApi(resource: String, typeApi: Class<T>): T {
    val valueResource = convertStreamToString(resource)
    return Gson().fromJson<T>(valueResource, typeApi)
}


fun convertStreamToString(filename: String): String {
    val input = ClassLoader.getSystemClassLoader().getResource(filename).openStream();
    val reader = BufferedReader(input.reader())
    val content = StringBuilder()
    try {
        var line = reader.readLine()
        while (line != null) {
            content.append(line)
            line = reader.readLine()
        }
    } finally {
        reader.close()
    }
    return content.toString()

}

