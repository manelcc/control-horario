/*
 * Copyright (C) 2019.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.datasource.login.login

import com.dinan.controlhorario.data.apientities.login.ApiGetHost
import com.dinan.controlhorario.data.apientities.login.ApiUser
import com.dinan.controlhorario.data.apirest.ApiLogin
import com.dinan.controlhorario.domain.usecase.login.GetLoginUseCase
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class DtsLoginImplTest {


    @Mock
    private lateinit var apiLogin: ApiLogin

    @Mock
    private lateinit var apiGetHost: ApiGetHost

    //test datasource
    private lateinit var datasourceLogin: DtsLogin

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        datasourceLogin = DtsLoginImpl(apiLogin)
    }


    @Test
    fun `return api call login`() {
        //Given
        val params = GetLoginUseCase.ParamsLogin("a", "b", "c")
        //When
        Mockito.`when`(datasourceLogin.getLogin(params,apiGetHost)).thenReturn(Observable.just(getApiUser()))
        //Then
        val observable: Observable<ApiUser> = datasourceLogin.getLogin(params,apiGetHost)

        val testObserver = TestObserver<ApiUser>()

        observable.subscribe(testObserver)//(1) suscribiendo el observable
        testObserver.assertSubscribed()//(2) preguntamos si la suscribcion es exitosa
        testObserver.awaitTerminalEvent()//(3) estamos bloqueando el hilo hasta que Observable / Flowable complete su ejecución con el
        testObserver.assertNoErrors()//(4)
        testObserver.assertComplete()//(5)
        testObserver.assertValueCount(1)//(6)
        testObserver.assertOf { it is TestObserver<ApiUser> }


    }

    @Test
    fun return_apiHost() {
    //Given
    val params = GetLoginUseCase.ParamsLogin("a", "b", "c")
    //When
    Mockito.`when`(datasourceLogin.getHost(params)).thenReturn(Observable.just(getApiHost()))
    //Then
    val observable: Observable<ApiGetHost> = datasourceLogin.getHost(params)

    val testObserver = TestObserver<ApiGetHost>()

    observable.subscribe(testObserver)//(1) suscribiendo el observable
    testObserver.assertSubscribed()//(2) preguntamos si la suscribcion es exitosa
    testObserver.awaitTerminalEvent()//(3) estamos bloqueando el hilo hasta que Observable / Flowable complete su ejecución con el
    testObserver.assertNoErrors()//(4)
    testObserver.assertComplete()//(5)
    testObserver.assertValueCount(1)//(6)
    }

    private fun getApiUser(): ApiUser {
        return ApiUser("cif", "utoken", null, "version", "0", "0")
    }

    private fun getApiHost(): ApiGetHost {
        return ApiGetHost()
    }
}