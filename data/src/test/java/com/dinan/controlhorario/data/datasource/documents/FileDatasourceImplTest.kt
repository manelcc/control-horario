/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.datasource.documents

import android.content.SharedPreferences
import com.dinan.controlhorario.data.apientities.document.ApiResponseFile
import com.dinan.controlhorario.data.apirest.ApiDocument
import com.dinan.controlhorario.data.getApi
import com.dinan.controlhorario.domain.model.login.User
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class FileDatasourceImplTest {

    @Mock
    private lateinit var api: ApiDocument
    @Mock
    private lateinit var userSession: User
    @Mock
    private lateinit var preference: SharedPreferences

    private lateinit var datasource: FileDatasourceImpl


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        datasource = FileDatasourceImpl(api, preference)
    }


    @Test
    fun funFileDocuments() {
        //Given
        val id: String = "id"
        val directorio: String = "directorio"
        //When
        Mockito.`when`(datasource.fileDocuments(id, directorio))
            .thenReturn(Observable.just(getApi("file_succes.json", ApiResponseFile::class.java)))
        //Then
        val observable: Observable<ApiResponseFile> = datasource.fileDocuments(id, directorio)
        val testObserver = TestObserver<ApiResponseFile>()

        observable.subscribe(testObserver)//(1) suscribiendo el observable
        testObserver.assertSubscribed()//(2) preguntamos si la suscribcion es exitosa
        testObserver.awaitTerminalEvent()//(3) estamos bloqueando el hilo hasta que Observable / Flowable complete su ejecución con el
        testObserver.assertNoErrors()//(4)
        testObserver.assertComplete()//(5)
        testObserver.assertValueCount(1)//(6)
        testObserver.assertOf { it is TestObserver<ApiResponseFile> }
        //testObserver.assertValue { it.ficheros.size == 2 }
        //testObserver.assertValue{it.ficheros[0].nombre == "Cierre Sábados"}
    }
}