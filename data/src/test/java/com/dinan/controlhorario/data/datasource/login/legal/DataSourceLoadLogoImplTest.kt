/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.data.datasource.login.legal

import android.content.SharedPreferences
import com.dinan.controlhorario.data.apientities.login.ApiConfirmLopd
import com.dinan.controlhorario.data.apientities.login.ApiLopdLogoEntity
import com.dinan.controlhorario.data.apirest.ApiLopdLogo
import com.dinan.controlhorario.domain.model.login.User
import com.dinan.controlhorario.domain.usecase.login.GetLoginUseCase
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class DataSourceLoadLogoImplTest{


    @Mock
    private lateinit var apiLopdLogo: ApiLopdLogo
    @Mock
    private lateinit var userSession: User

    @Mock
    private lateinit var preference: SharedPreferences
    //test
    private lateinit var datasourceLopdLogo: DataSourceLoadLogo

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        datasourceLopdLogo =
                DataSourceLoadLogoImpl(apiLopdLogo, preference)
    }

    @Test
    fun loadLogoUrl() {
        //Given
        val params = GetLoginUseCase.ParamsLogin("a", "b", "c")
        //When
        whenever(datasourceLopdLogo.loadLopdAndLogo()).thenReturn(Observable.just(getApiLopdLogo()))
        //Then
        val observable: Observable<ApiLopdLogoEntity> = datasourceLopdLogo.loadLopdAndLogo()

        val testObserver = TestObserver<ApiLopdLogoEntity>()

        observable.subscribe(testObserver)
        testObserver.assertSubscribed()
        testObserver.awaitTerminalEvent()
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValueCount(1)
    }

    @Test
    fun confirmLOPD() {
        //Given
        val params = GetLoginUseCase.ParamsLogin("a", "b", "c")
        //When
        whenever(datasourceLopdLogo.confirmLOPD()).thenReturn(Observable.just(getApiConfirmLopd()))
        //Then
        val observable: Observable<ApiConfirmLopd> = datasourceLopdLogo.confirmLOPD()

        val testObserver = TestObserver<ApiConfirmLopd>()

        observable.subscribe(testObserver)
        testObserver.assertSubscribed()
        testObserver.awaitTerminalEvent()
        testObserver.assertNoErrors()
        testObserver.assertComplete()
        testObserver.assertValueCount(1)
    }

    fun getApiLopdLogo(): ApiLopdLogoEntity {
        return ApiLopdLogoEntity(
                "cif",
                "urlologo",
                "lopd",
                "utoken",
                "verion"
        )
    }

    fun getApiConfirmLopd(): ApiConfirmLopd {
        return ApiConfirmLopd("cif", true, "lopd", "utoken")
    }
}