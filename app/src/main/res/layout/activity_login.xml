<?xml version="1.0" encoding="utf-8"?><!--
  ~ Copyright (C) 2020.  Manel Cabezas
  ~
  ~  Licensed under the Apache License, Version 2.0 (the "License");
  ~  you may not use this file except in compliance with the License.
  ~  You may obtain a copy of the License at
  ~
  ~       http://www.apache.org/licenses/LICENSE-2.0
  ~
  ~  Unless required by applicable law or agreed to in writing, software
  ~  distributed under the License is distributed on an "AS IS" BASIS,
  ~  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  ~  See the License for the specific language governing permissions and
  ~  limitations under the License.
  ~
  -->

<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
        xmlns:app="http://schemas.android.com/apk/res-auto"
        xmlns:tools="http://schemas.android.com/tools"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        tools:context=".features.login.view.LoginActivity">

    <TextView
            android:id="@+id/login_txt_header"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:layout_marginTop="50dp"
            android:layout_marginEnd="8dp"
            android:fontFamily="@font/helveticabold"
            android:text="Horario"
            android:textAlignment="center"
            android:textColor="@color/textLightBlue"
            android:textSize="58sp"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintHorizontal_bias="0.0"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="@+id/login_txt_subheader" />

    <TextView
            android:id="@+id/login_txt_subheader"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginStart="8dp"
            android:layout_marginTop="16dp"
            android:layout_marginEnd="8dp"
            android:layout_marginBottom="16dp"
            android:fontFamily="@font/helveticabold"
            android:text="Control"
            android:textAlignment="center"
            android:textColor="@color/textGray"
            android:textSize="58sp"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toTopOf="parent"
            app:layout_constraintVertical_bias="0.20" />

    <com.google.android.material.textfield.TextInputLayout
            android:id="@+id/login_input_layout_cif"
            style="@style/Widget.MaterialComponents.TextInputLayout.OutlinedBox"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:focusableInTouchMode="true"
            android:hint="@string/login_hint_cif"
            app:layout_constraintBottom_toTopOf="@+id/login_input_layout_user_email"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/login_txt_header"
            app:layout_constraintVertical_bias="0.20"
            app:layout_constraintVertical_chainStyle="packed"
            app:layout_constraintWidth_percent="0.75">

        <com.google.android.material.textfield.TextInputEditText
                android:id="@+id/login_et_cif_empresa"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:ems="10"
                android:focusableInTouchMode="true"
                android:fontFamily="@font/helvetica"
                android:inputType="textPhonetic"
                android:maxLength="10" />
    </com.google.android.material.textfield.TextInputLayout>

    <com.google.android.material.textfield.TextInputLayout
            android:id="@+id/login_input_layout_user_email"
            style="@style/Widget.MaterialComponents.TextInputLayout.OutlinedBox"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginTop="16dp"
            android:focusableInTouchMode="true"
            android:hint="@string/login_hint_user"
            app:layout_constraintBottom_toTopOf="@+id/login_input_layout_password"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/login_input_layout_cif"
            app:layout_constraintWidth_percent="0.75">

        <com.google.android.material.textfield.TextInputEditText
                android:id="@+id/login_et_user_email"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:ems="10"
                android:focusableInTouchMode="true"
                android:fontFamily="@font/helvetica"
                android:inputType="text" />
    </com.google.android.material.textfield.TextInputLayout>

    <com.google.android.material.textfield.TextInputLayout
            android:id="@+id/login_input_layout_password"
            style="@style/Widget.MaterialComponents.TextInputLayout.OutlinedBox"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginTop="16dp"
            android:focusableInTouchMode="true"
            android:hint="@string/login_hint_password"
            app:endIconMode="clear_text"
            app:layout_constraintBottom_toTopOf="@+id/login_btn_entrar"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/login_input_layout_user_email"
            app:layout_constraintWidth_percent="0.75">

        <com.google.android.material.textfield.TextInputEditText
                android:id="@+id/login_et_password"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:ems="10"
                android:focusableInTouchMode="true"
                android:fontFamily="@font/helvetica"
                android:inputType="textPassword"
                android:maxLength="20" />
    </com.google.android.material.textfield.TextInputLayout>

    <com.google.android.material.button.MaterialButton
            android:id="@+id/login_btn_entrar"
            style="@style/Widget.MaterialComponents.Button.UnelevatedButton"
            android:layout_width="0dp"
            android:layout_height="wrap_content"
            android:layout_marginTop="24dp"
            android:fontFamily="@font/helvetica"
            android:text="@string/global_txt_entrar"
            app:layout_constraintBottom_toBottomOf="parent"
            app:layout_constraintEnd_toEndOf="parent"
            app:layout_constraintStart_toStartOf="parent"
            app:layout_constraintTop_toBottomOf="@+id/login_input_layout_password"
            app:layout_constraintWidth_percent="0.75" />

    <include android:id="@+id/progress_login"
            layout="@layout/progress" />

</androidx.constraintlayout.widget.ConstraintLayout>