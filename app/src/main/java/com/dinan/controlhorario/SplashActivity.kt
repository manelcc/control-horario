/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario

import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.dinan.controlhorario.data.core.map.mapFromPreference
import com.dinan.controlhorario.data.extension.PERMISSION_GPS_COURSE
import com.dinan.controlhorario.data.extension.PERMISSION_GPS_FINE
import com.dinan.controlhorario.extension.launchActivity
import com.dinan.controlhorario.features.factoryDialogs.CustomDialog
import com.dinan.controlhorario.features.factoryDialogs.FactoryDialogs
import com.dinan.controlhorario.features.home.view.MainActivity
import com.dinan.controlhorario.features.login.view.LoginActivity
import com.tbruyelle.rxpermissions2.RxPermissions
import org.koin.android.ext.android.inject


class SplashActivity : AppCompatActivity() {

    private lateinit var params: RxPermissions
    //lazy injection with lifecycleObserve
    private val preference: SharedPreferences by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        validatePermmissionsApp()
    }

    private fun validateToken() {
        //TODO:CAMBIAR POR UN LOGIN INTERNO?
        when (userLoggedIn()) {
            true -> goHome()
            false -> initLogin()
        }
    }

    private fun userLoggedIn(): Boolean{
        //si el usuario existe el userToken no esta vacio y ademas el cambio pass y lopd estan a true=vistos entonces envia true
        //para ir a la home directo -> el usuario esta logueado, en caso contrario ir a login.
        val user  = mapFromPreference(preference)
        return user!=null && user.userToken.isNotEmpty() && user.changePass && user.lopd
    }

    private fun validatePermmissionsApp() {
        params = RxPermissions(this)
        when (params.isGranted(PERMISSION_GPS_FINE) || params.isGranted(
            PERMISSION_GPS_COURSE)) {
            true -> validateToken()
            false -> requestPermission()
        }
    }

    @SuppressLint("CheckResult")
    private fun requestPermission() {
        params.request(PERMISSION_GPS_FINE, PERMISSION_GPS_COURSE)
            .subscribe({ granted ->
                when (granted) {
                    true -> validateToken()
                    false -> exitAppRevokePermissions()
                }
            },{ exitAppRevokePermissions() })

    }

    private fun exitAppRevokePermissions() {
        val dialogs: CustomDialog = FactoryDialogs.createDialog(this, FactoryDialogs.TypeDialogs.PERMISSION)
        dialogs.getNegativeButton().setOnClickListener { finish() }
        dialogs.getPositiveBoton().setOnClickListener { dialogs.dismiss();validatePermmissionsApp() }
        dialogs.showDialog()
    }

    private fun goHome() {
        launchActivity<MainActivity>(this)
        finishAfterTransition()
    }

    private fun initLogin() {
        launchActivity<LoginActivity>(this)
        finishAfterTransition()
    }


}