/*
 * Copyright (C) 2019.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.core.base

object globalConstants {
    const val KEY_URL_DOCUMENT = "url_document"
    const val KEY_NAME_FOLDER = "name_folder"
    const val KEY_ID_DOCUMENTS = "id_document"
    const val KEY_DIRECTORIO = "directorio document"
    const val TAG = "ControlHorario"
    const val KEY_STRING_TOKEN = "token_firebase"
    const val PUSH_GO_CHAT = "go chat"
    const val PUSH_GO_PUNCHIN = "go punchin"
    const val KEY_PUSH_SCREEN = "screen"
}