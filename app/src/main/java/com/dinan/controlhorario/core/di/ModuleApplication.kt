/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.core.di

import android.content.Context
import android.content.SharedPreferences
import com.dinan.controlhorario.data.core.base.DataConstants.KEY_USER_PREF
import com.dinan.controlhorario.domain.extension.empty
import com.dinan.controlhorario.domain.model.login.User
import com.dinan.controlhorario.extension.networkInfo
import com.google.gson.Gson
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import java.util.*


private val PREFERENCES_FILE_KEY = "user"


fun userPreference(preferences: SharedPreferences): User? {
    val stringUserPreference: String? = preferences.getString(KEY_USER_PREF, String.empty())
    stringUserPreference.let { return Gson().fromJson(stringUserPreference, User::class.java) }
}


private fun provideSettingsPreferences(app: Context): SharedPreferences =
        app.getSharedPreferences(PREFERENCES_FILE_KEY, Context.MODE_PRIVATE)



private fun provideCalendar(): Calendar {
    return Calendar.getInstance()
}


// just declare it
val moduleApplication = module {

    single { this@module }
    single { NetworkUtils(androidContext()) }
    single { provideSettingsPreferences(androidContext()) }
    single { provideCalendar() }
    single { userPreference(provideSettingsPreferences(androidContext())) }


}


