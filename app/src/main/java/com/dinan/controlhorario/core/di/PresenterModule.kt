/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.core.di

import com.dinan.controlhorario.features.calendar.calendar.presenter.CalendarPresenter
import com.dinan.controlhorario.features.calendar.calendar.presenter.CalendarPresenterImpl
import com.dinan.controlhorario.features.calendar.changehours.presenter.ChangeHourPresenterImpl
import com.dinan.controlhorario.features.calendar.changehours.presenter.ChangeHoursPresenter
import com.dinan.controlhorario.features.documents.presenter.DocumentsPresenter
import com.dinan.controlhorario.features.documents.presenter.DocumentsPresenterImpl
import com.dinan.controlhorario.features.home.presenter.HomePresenter
import com.dinan.controlhorario.features.home.presenter.HomePresenterImpl
import com.dinan.controlhorario.features.login.presenter.changepass.ChangePassPresenter
import com.dinan.controlhorario.features.login.presenter.changepass.ChangePassPresenterImpl
import com.dinan.controlhorario.features.login.presenter.legal.LegalPresenter
import com.dinan.controlhorario.features.login.presenter.legal.LegalPresenterImpl
import com.dinan.controlhorario.features.login.presenter.login.LoginPresenter
import com.dinan.controlhorario.features.login.presenter.login.LoginPresenterImpl
import com.dinan.controlhorario.features.messages.presenter.ChatPresenter
import com.dinan.controlhorario.features.messages.presenter.ChatPresenterImpl
import com.dinan.controlhorario.features.punchin.presenter.PunchInPresenter
import com.dinan.controlhorario.features.punchin.presenter.PunchInPresenterImpl
import com.dinan.controlhorario.features.punchin.presenter.UpdateFilesPunchInPresenter
import com.dinan.controlhorario.features.punchin.presenter.UpdateFilesPunchInPresenterImpl
import org.koin.dsl.module

val presenterModule = module {

    factory<LoginPresenter.Presenter> { LoginPresenterImpl(get(), get(),get()) }
    factory<LegalPresenter.Presenter> { LegalPresenterImpl(get(), get()) }
    factory<ChangePassPresenter.Presenter> { ChangePassPresenterImpl(get(), get()) }
    factory<HomePresenter.Presenter> { HomePresenterImpl(get()) }
    factory<PunchInPresenter.Presenter> { PunchInPresenterImpl(get(),get()) }
    factory<CalendarPresenter.Presenter> { CalendarPresenterImpl(get(),get()) }
    factory<DocumentsPresenter.Presenter> { DocumentsPresenterImpl(get(),get(),get()) }
    factory<ChatPresenter.Presenter> { ChatPresenterImpl(get(),get()) }
    factory<ChangeHoursPresenter.Presenter> { ChangeHourPresenterImpl(get()) }
    factory<UpdateFilesPunchInPresenter.Presenter> { UpdateFilesPunchInPresenterImpl(get()) }

}

