/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.punchin.presenter

import android.location.Location
import androidx.lifecycle.LifecycleObserver
import com.dinan.controlhorario.domain.model.punchin.EventPunchIn
import com.example.android.architecture.blueprints.todoapp.BasePresenter
import com.example.android.architecture.blueprints.todoapp.BaseView

interface PunchInPresenter {
    interface Presenter : BasePresenter<View>, LifecycleObserver {

        fun launchPunchIn(stateButtom: String, location: Location?)
        fun loadEvents(location: Location)
    }

    interface View : BaseView<Presenter> {

        fun initEvents(botonToShow:String, listEvents: ArrayList<EventPunchIn>)
        fun eventsHandle(botonToShow: String, listEvents: ArrayList<EventPunchIn>)
        fun eventsSucces(listEvents: ArrayList<EventPunchIn>)
        fun eventsError(message: String?)
        fun showTimer(valueHour: String)
        fun emptyList()
        fun updateChat(pendingMessageReader: String)
        fun notCanPunchIn(message: String?)
        fun endPunchIn(botonToShow: String, list: java.util.ArrayList<EventPunchIn>)
        fun errorNetwork()
        fun showProgress()
        fun hideProgress()
        fun errorPunchInRetry(message: String?)

    }
}