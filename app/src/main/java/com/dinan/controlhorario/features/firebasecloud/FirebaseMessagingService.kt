/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.firebasecloud

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.work.Data
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.dinan.controlhorario.R
import com.dinan.controlhorario.core.base.globalConstants.KEY_PUSH_SCREEN
import com.dinan.controlhorario.core.base.globalConstants.KEY_STRING_TOKEN
import com.dinan.controlhorario.core.base.globalConstants.PUSH_GO_CHAT
import com.dinan.controlhorario.core.base.globalConstants.PUSH_GO_PUNCHIN
import com.dinan.controlhorario.core.base.globalConstants.TAG
import com.dinan.controlhorario.features.home.view.MainActivity
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class FirebaseMessagingService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        // Check if message contains a data payload.
        remoteMessage.data.isNotEmpty().let {
            when (it) {
                true -> scheduleJob()
                false -> handleNow()
            }
            it
        }

        // Check if message contains a notification payload.
        if (remoteMessage.notification != null) {
            val destinity = getDestinationScreen(remoteMessage.data.get("screen"))
            sendNotification(remoteMessage, destinity)
        }

    }

    private fun getDestinationScreen(screen: String?): String {
        if(screen.isNullOrEmpty()){
             return PUSH_GO_CHAT//mientras no se defina si se recibe una notificación vamos al chat
        }else{
            when(screen){
                "chat" -> return PUSH_GO_CHAT
                "fichar"-> return PUSH_GO_PUNCHIN
                else -> return PUSH_GO_CHAT//mientras no se defina si se recibe una notificación vamos al chat
            }
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private fun sendNotification(remoteMessage: RemoteMessage, screen: String) {

        val notifyIntent = Intent(this, MainActivity::class.java).apply {
            putExtra(KEY_PUSH_SCREEN,screen)
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP
        }
        val pendingIntent = PendingIntent.getActivity(
            this, 0 /* Request code */, notifyIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        val channelId = getString(R.string.default_notification_channel_id)
        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        //Setting up Notification channels for android O and above
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_HIGH
            )
            channel.description ="descriptionChanel"
            channel.enableVibration(true)
            channel.enableLights(true)
            notificationManager.createNotificationChannel(channel)
        }
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.ic_icon_push)
            .setContentTitle(remoteMessage.notification?.title)
            .setContentText(remoteMessage.notification?.body)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent)

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelId,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationManager.createNotificationChannel(channel)
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build())
    }

    private fun handleNow() {
        Log.e(TAG,"entramos en hacer ahora")
        //empty method now

    }

    private fun scheduleJob() {
        Log.e(TAG,"entramos en hacer en background")
        //empty method now
    }

    override fun onNewToken(token: String) {
        Log.d("firebaseManel", "nuevo token ${token} ")
        sendNewTokenToServer(token)
    }

    private fun sendNewTokenToServer(token: String) {
        //without KTX
        val inputData = Data.Builder().putString(KEY_STRING_TOKEN, token).build()
        val work = OneTimeWorkRequest.Builder(FirebaseWork::class.java).setInputData(inputData).build()
        WorkManager.getInstance(applicationContext).beginWith(work).enqueue()
    }
}
