/*
 * Copyright (C) 2020.  Manel Cabezas
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.calendar.calendar.presenter

import android.util.Log
import com.dinan.controlhorario.domain.core.base.DefaultObserver
import com.dinan.controlhorario.domain.model.calendario.Calendario

class CallBackCalendar(private val view: CalendarPresenter.View, private val fechaSolicitada: String) : DefaultObserver<Calendario>() {

    override fun onComplete() {
        super.onComplete()
        view.hideProgress()
    }

    override fun onNext(resultSuccess: Calendario) {
        super.onNext(resultSuccess)
        view.calendar(resultSuccess,fechaSolicitada)
        view.messagePending(resultSuccess.pendingMessageReader)
    }

    override fun onError(exception: Throwable) {
        super.onError(exception)
        view.hideProgress()
        Log.e("CallBackCalendar","se ha producido un error")
        view.errorCalendar(fechaSolicitada)

    }
}
