/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.login.view

import android.app.Activity
import android.os.Build
import android.os.Bundle
import android.text.Html
import androidx.appcompat.app.AppCompatActivity
import com.dinan.controlhorario.R
import com.dinan.controlhorario.extension.showSnackbar
import com.dinan.controlhorario.features.factoryDialogs.FactoryDialogs
import com.dinan.controlhorario.features.login.presenter.legal.LegalPresenter
import kotlinx.android.synthetic.main.activity_legal.*
import org.koin.android.ext.android.inject

class LegalActivity : AppCompatActivity(), LegalPresenter.View {

    override val presenter: LegalPresenter.Presenter by inject()

    //***************************************************
    // LIFECYCLE
    //***************************************************
    override fun onStart() {
        super.onStart()
        lifecycle.addObserver(presenter)
    }
    override fun onStop() {
        super.onStop()
        lifecycle.removeObserver(presenter)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectKoin()
        setContentView(R.layout.activity_legal)
        setListeners()

    }

    private fun injectKoin() {
        presenter.view = this
    }

    private fun setListeners() {
        legalBtnAceptarLopd.setOnClickListener {
            presenter.acceptLopd()
        }
    }


    //***************************************************
    // GESTION EVENTOS
    //***************************************************

    @Suppress("DEPRECATION")
    override fun showLegal(t: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            legalTxtContentLopd.text = Html.fromHtml(t, Html.FROM_HTML_MODE_COMPACT)
        } else {
            legalTxtContentLopd.text = Html.fromHtml(t)
        }
    }

    override fun aceptLopdSucces(t: Boolean) {
        when(t){
            true->{ setResult(Activity.RESULT_OK);finish()}
            false-> legalBtnAceptarLopd.showSnackbar("Se ha producido un error, vuelva a Aceptar")
        }
    }


    //***************************************************
    // GESTION ERRORES
    //***************************************************
    override fun errorLegal(message: String?) {
        FactoryDialogs.createDialog(this,FactoryDialogs.TypeDialogs.ALERTLEGAL).showDialog()
    }

    override fun aceptLopdError() {
        errorLegal("error btn Accept")
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }
}
