/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.calendar.changehours.presenter

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.dinan.controlhorario.domain.model.punchin.EventPunchIn
import com.dinan.controlhorario.domain.usecase.GetRequestChangeHour

class ChangeHourPresenterImpl(private val useCase: GetRequestChangeHour) :
    ChangeHoursPresenter.Presenter, LifecycleObserver {

    override lateinit var view: ChangeHoursPresenter.View

    override fun requestChangeHours(eventList: ArrayList<EventPunchIn>, observation: String) {
        view.showProgress()
        useCase.execute(
            CallBackChangeHours(view), GetRequestChangeHour.Params(
                eventList,
                observation
            )
        )
    }

    override fun changeHourItem(finalHour: String, eventPunchIn: EventPunchIn) {
        Log.e("manel", "guarda item $finalHour")
        eventPunchIn.hourSelected = finalHour
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    override fun onResumeView() {

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    override fun onDestroyView() {
        useCase.dispose()
    }
}