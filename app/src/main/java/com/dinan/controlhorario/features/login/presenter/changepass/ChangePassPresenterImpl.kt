/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.login.presenter.changepass

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

import com.dinan.controlhorario.domain.model.login.User
import com.dinan.controlhorario.domain.usecase.login.GetChangePassUseCaseImpl
import com.dinan.controlhorario.extension.isValidPass
import com.dinan.controlhorario.features.login.presenter.callback.CallBackChangePass


class ChangePassPresenterImpl(private val getChangePassUseCase: GetChangePassUseCaseImpl, private val user: User?) : ChangePassPresenter.Presenter,
    LifecycleObserver {

    override fun loadUrl(): String? {
       return user?.urlLogo
    }

    override lateinit var view: ChangePassPresenter.View

    override fun changePassword(newPass: String, repeatPass: String) {
        when {
            newPass.isValidPass() && newPass == repeatPass -> getChangePassUseCase.execute(CallBackChangePass(view),GetChangePassUseCaseImpl.ParamsChangePass(repeatPass,newPass))
            else -> view.changePassInputError()
        }
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    override fun onResumeView() {

    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    override fun onDestroyView() {
    getChangePassUseCase.dispose()
    }

}