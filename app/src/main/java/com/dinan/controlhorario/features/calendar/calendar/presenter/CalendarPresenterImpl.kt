/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.calendar.calendar.presenter

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.dinan.controlhorario.domain.model.calendario.CalendarList
import com.dinan.controlhorario.domain.usecase.GetCalendarioUseCase
import com.dinan.controlhorario.domain.usecase.GetHoursUseCase
import com.dinan.controlhorario.extension.empty
import com.dinan.controlhorario.extension.stringServerFormat

class CalendarPresenterImpl(
    private val useCase: GetCalendarioUseCase,
    private val usecaseHours: GetHoursUseCase
) : CalendarPresenter.Presenter, LifecycleObserver {

    override lateinit var view: CalendarPresenter.View


    override fun loadCalendar(fecha: String) {
        view.showProgress()
        useCase.execute(CallBackCalendar(view, fecha), GetCalendarioUseCase.ParamsCalendar(fecha))
    }

    override fun getHours(fecha: CalendarList, isResumeJob: Boolean) {
        if ("00:00:00" != fecha.horas) {
            usecaseHours.execute(
                CallBackGetHours(
                    view,
                    fecha.fecha.stringServerFormat(),
                    isResumeJob
                ), GetHoursUseCase.Params(fecha.fecha.stringServerFormat(),fecha.horas)
            )
        } else {
            view.errorGetHours(String.empty())
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun pausedFragment() {
        view.hideProgress()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    override fun onDestroyView() {
        useCase.dispose()
        usecaseHours.dispose()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    override fun onResumeView() {

    }
}