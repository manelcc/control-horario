/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.home.presenter


import android.widget.ImageView
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.dinan.controlhorario.domain.usecase.login.GetLegalUseCaseImpl

class HomePresenterImpl(private val getLogoUrlUseCase: GetLegalUseCaseImpl) : HomePresenter.Presenter, LifecycleObserver {
    override lateinit var view: HomePresenter.View

    override fun loadLogo(homeImagTop: ImageView) {
        getLogoUrlUseCase.execute(CallBackLoadLogo(view, homeImagTop),Unit)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    override fun onResumeView() {
        //EMPTY METHOD
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    override fun onDestroyView() {
        getLogoUrlUseCase.dispose()
    }

}