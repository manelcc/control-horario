/*
 * Copyright (C) 2019.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.messages.view


import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.dinan.controlhorario.R
import com.dinan.controlhorario.core.base.BaseFragment
import com.dinan.controlhorario.core.base.globalConstants.TAG
import com.dinan.controlhorario.domain.model.chat.MessageChatList
import com.dinan.controlhorario.extension.showSnackbar
import com.dinan.controlhorario.features.messages.presenter.ChatPresenter
import com.dinan.controlhorario.features.messages.view.adapter.ChatAdapter
import kotlinx.android.synthetic.main.fragment_messages.view.*
import org.koin.android.ext.android.inject
import java.util.*


/**
 * A simple [Fragment] subclass.
 *
 */
class MessagesFragment : BaseFragment<ChatPresenter.Presenter, ChatPresenter.View>(),
    ChatPresenter.View {

    private lateinit var text_message_et: AppCompatEditText
    private lateinit var send_message_btn: AppCompatImageView
    private lateinit var recycler: RecyclerView
    private lateinit var adapter: ChatAdapter
    override val view: ChatPresenter.View
        get() = this


    override val presenter: ChatPresenter.Presenter by inject()

    override fun setUpToolbarFromFragment() {
        listener?.setUpTitleToolbar(getString(R.string.home_text_mensajes))
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        if (isVisibleToUser) {
            presenter.view = view
            setUpToolbarFromFragment()
            Log.e(TAG, "pedimos todos los charts")
            presenter.getAllChats()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val layout = inflater.inflate(R.layout.fragment_messages, container, false)
        bindViews(layout)
        settingListeners()

        return layout
    }

    private fun settingListeners() {
        send_message_btn.setOnClickListener { presenter.sendMessage(text_message_et.text.toString()) }
        text_message_et.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                //empty method
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //empty method
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                when {
                    count > 0 -> send_message_btn.isClickable = true
                    else -> send_message_btn.isClickable = false
                }
            }
        })
    }

    private fun bindViews(layout: View) {
        recycler = layout.chat_recycler
        send_message_btn = layout.chat_btn_send_message
        text_message_et = layout.chat_et_send_message
    }


    override fun successChat(resultSuccess: ArrayList<MessageChatList>) {
        adapter = ChatAdapter(resultSuccess)
        recycler.setHasFixedSize(true)
        recycler.adapter = adapter
        recycler.smoothScrollToPosition(adapter.itemCount)

    }

    override fun errorMessageEmpty() {
        recycler.showSnackbar(getString(R.string.emptySendMessage))
    }

    override fun succesSendChat() {
        text_message_et.text?.clear()

    }

    override fun errorSendMessage(message: String?) {
        recycler.showSnackbar("Error inesperado: ${message}")
    }

    override fun messagePending(pendingMessageReader: String) {
        listener?.showPendingMessages(pendingMessageReader)
    }

}
