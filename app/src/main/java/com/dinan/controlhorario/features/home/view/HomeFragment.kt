/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.home.view


import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.dinan.controlhorario.BuildConfig.VERSION_NAME

import com.dinan.controlhorario.R
import com.dinan.controlhorario.core.base.BaseFragment

import com.dinan.controlhorario.domain.model.login.User
import com.dinan.controlhorario.features.factoryDialogs.CustomDialog
import com.dinan.controlhorario.features.factoryDialogs.FactoryDialogs
import com.dinan.controlhorario.features.home.presenter.HomePresenter
import com.dinan.controlhorario.features.home.view.MainActivity.Companion.ACCESSPLUSCHAT
import com.dinan.controlhorario.features.home.view.MainActivity.Companion.ACCESSPLUSDOC
import com.dinan.controlhorario.features.home.view.MainActivity.Companion.ACCESSPUNCHINCALENDAR
import com.dinan.controlhorario.features.login.view.LoginActivity
import kotlinx.android.synthetic.main.fragment_home.view.*
import org.koin.android.ext.android.inject


class HomeFragment : BaseFragment<HomePresenter.Presenter,HomePresenter.View>(), HomePresenter.View {

    override val presenter: HomePresenter.Presenter by inject()
    private val preference: SharedPreferences by inject()
    private val userSession: User by inject()

    override fun setUpToolbarFromFragment() {
        listener?.setUpTitleToolbar("")
    }
    override val view: HomePresenter.View
        get() = this


    companion object {
        @JvmStatic
        fun newInstance() = HomeFragment()
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?): View? {

        val layout = inflater.inflate(R.layout.fragment_home, container, false)
        presenter.view = this
        bindUrlLogo(layout)
        setListeners(layout)
        bindEnablesAccess(layout)
        return layout
    }

    private fun bindEnablesAccess(layout: View) {
        fun disableChat() {
            layout.home_cardview_mensajes_img.setColorFilter(ContextCompat.getColor(context!!, R.color.textGray), android.graphics.PorterDuff.Mode.SRC_IN)
            layout.home_cardview_mensajes.isEnabled = false
            layout.home_cardview_mensajes.setOnClickListener { null }
            layout.home_cardview_message_txt.setTextColor(ContextCompat.getColor(context!!,R.color.textGray))

        }

        fun disableDocument() {
            layout.home_cardview_doc_img.setColorFilter(ContextCompat.getColor(context!!, R.color.textGray), android.graphics.PorterDuff.Mode.SRC_IN)
            layout.home_cardview_documentos.isEnabled = false
            layout.home_cardview_documentos.setOnClickListener { null }
            layout.home_cardview_doc_txt.setTextColor(ContextCompat.getColor(context!!,R.color.textGray))
        }
        fun disableDocAndMessage() {
            disableChat()
            disableDocument()
        }
        when (userSession.accessUser) {
            ACCESSPUNCHINCALENDAR -> disableDocAndMessage()
            ACCESSPLUSDOC -> disableChat()
            ACCESSPLUSCHAT -> disableDocument()
        }
    }

    private fun bindUrlLogo(layout: View) {
        if (userSession.urlLogo.isNotEmpty()) {
            logo(userSession.urlLogo, layout.home_imag_top)
        } else {
            presenter.loadLogo(layout.home_imag_top)
        }
    }

    override fun logo(urlLogo: String, homeImagTop: ImageView) {
        Glide.with(this).load(urlLogo).into(homeImagTop)
    }

    private fun setListeners(layout: View) {
        layout.home_cardview_fichar.setOnClickListener { listener?.cardClickFichar() }
        layout.home_cardview_calendario.setOnClickListener { listener?.cardClickCalendario() }
        layout.home_cardview_documentos.setOnClickListener { listener?.cardClickDocumentos() }
        layout.home_cardview_mensajes.setOnClickListener { listener?.cardClickMensajes() }
        layout.home_cerrar_session.setOnClickListener { listenerFinishSession() }
        layout.home_version.text = "D: ".plus(VERSION_NAME)
    }


    private fun listenerFinishSession() {
        val dialog: CustomDialog = FactoryDialogs.createDialog(activity, FactoryDialogs.TypeDialogs.FINISH_SESSION)
        dialog.getNegativeButton().setOnClickListener { dialog.dismiss() }
        dialog.getPositiveBoton().setOnClickListener {
            dialog.dismiss()
            userSession.urlLogo = ""
            preference.edit().clear().commit()
            startActivity(Intent(activity, LoginActivity::class.java)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK))
            activity?.finish()
        }
        dialog.showDialog()
    }
}


