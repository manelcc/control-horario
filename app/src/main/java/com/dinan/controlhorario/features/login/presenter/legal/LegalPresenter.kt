/*
 * Copyright (C) 2019.  Daniel Jimenez
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.login.presenter.legal

import androidx.lifecycle.LifecycleObserver
import com.example.android.architecture.blueprints.todoapp.BasePresenter
import com.example.android.architecture.blueprints.todoapp.BaseView

interface LegalPresenter {

    interface Presenter : BasePresenter<View>, LifecycleObserver {
        fun acceptLopd()


    }

    interface View : BaseView<Presenter> {
        fun showLegal(t: String)
        fun errorLegal(message: String?)
        fun aceptLopdSucces(t: Boolean)
        fun aceptLopdError()


    }

}