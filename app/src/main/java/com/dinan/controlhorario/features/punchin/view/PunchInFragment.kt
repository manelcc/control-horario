/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.punchin.view


import android.annotation.SuppressLint
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.annotation.NonNull
import androidx.fragment.app.FragmentActivity
import com.airbnb.lottie.LottieAnimationView
import com.dinan.controlhorario.R
import com.dinan.controlhorario.core.base.BaseFragment
import com.dinan.controlhorario.data.extension.*
import com.dinan.controlhorario.domain.extension.empty
import com.dinan.controlhorario.domain.model.punchin.EventPunchIn
import com.dinan.controlhorario.extension.Properties.DATEFORMAT_STRING_MONTH
import com.dinan.controlhorario.extension.customDateNow
import com.dinan.controlhorario.extension.gone
import com.dinan.controlhorario.extension.replaceFragmentAnim
import com.dinan.controlhorario.extension.show
import com.dinan.controlhorario.extension.showSnackbar
import com.dinan.controlhorario.features.factoryDialogs.CustomDialog
import com.dinan.controlhorario.features.factoryDialogs.FactoryDialogs
import com.dinan.controlhorario.features.factoryDialogs.dialogs.BeginDayDialog
import com.dinan.controlhorario.features.factoryDialogs.dialogs.GenericDialog
import com.dinan.controlhorario.features.punchin.presenter.PunchInPresenter
import com.dinan.controlhorario.features.punchin.view.adapter.PunchInAdapterEvent
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.Task
import kotlinx.android.synthetic.main.fragment_punch_in.*
import kotlinx.android.synthetic.main.fragment_punch_in.view.*
import org.koin.android.ext.android.inject

class PunchInFragment : BaseFragment<PunchInPresenter.Presenter, PunchInPresenter.View>(),
    PunchInPresenter.View {


    private var stateButtom: String = String.empty()
    private var layout: View? = null
    private lateinit var adapter: PunchInAdapterEvent
    override val presenter: PunchInPresenter.Presenter by inject()
    override val view: PunchInPresenter.View
        get() = this

    var isVisibleToUser: Boolean = false
    override fun setUpToolbarFromFragment() {
        listener?.setUpTitleToolbar(getString(R.string.home_text_fichar))
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        this.isVisibleToUser = isVisibleToUser
        if (isVisibleToUser) {
            presenter.view = view
            setUpToolbarFromFragment()
            presenter.loadEvents(defectLocation())
            punchInToogleButton.isEnabled = true
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        layout = inflater.inflate(R.layout.fragment_punch_in, container, false)
        bindViews()
        return layout
    }

    private fun bindViews() {
        layout?.apply {
            punchInFloating.setOnClickListener {
                activity?.replaceFragmentAnim(
                    UpdateFilesFragment.newInstance(),
                    android.R.id.content,
                    "changeHours"
                )
            }
            adapter = PunchInAdapterEvent(true)
            punchInDate.text = String.customDateNow(DATEFORMAT_STRING_MONTH)
            punchInRecycler.setHasFixedSize(true)
            punchInRecycler.adapter = adapter
            punchInToogleButton.setOnClickListener {
                when (stateButtom) {
                    INICIAR -> iniciar()
                    PAUSAR -> dialogEventPausedEnd()
                    REANUDAR -> reanudar()
                    FINALIZAR -> finalizar()
                    ENDPUNCHIN -> endPunchInState()

                }
            }
        }
    }


    /***********************
     * ACTIONS
     ***********************/
    private fun punchIn(@NonNull stateButtom: String) {
        if (isVisibleToUser) {
            try {
                activity?.let { fragmentActivity ->
                    provideLocation(fragmentActivity)?.let { task: Task<Location> ->
                        task.addOnSuccessListener { presenter.launchPunchIn(stateButtom, it) }
                        task.addOnFailureListener {
                            //todo: mostrar cuadro dialogo para enviarlo a activar localizacion
                            presenter.launchPunchIn(stateButtom, defectLocation())
                        }
                    }
                }
            } catch (exception: Exception) {
                //todo: mostrar cuadro dialogo para enviarlo a activar localizacion
                presenter.launchPunchIn(stateButtom, defectLocation())
            }
        }
    }

    @SuppressLint("MissingPermission")
    private fun provideLocation(app: FragmentActivity): Task<Location>? {
        return LocationServices.getFusedLocationProviderClient(app).lastLocation
    }

    private fun defectLocation(): Location {
        val defectLocation: Location = Location("defecto")
        defectLocation.altitude = 1.00
        defectLocation.longitude = 1.00
        return defectLocation
    }

    private fun endPunchInState() {
        Log.e("manel", "no generamos eventos ")
    }

    private fun dialogEventPausedEnd() {
        val dialogs = FactoryDialogs.createDialog(activity, FactoryDialogs.TypeDialogs.PAUSE_END_DAY)
        dialogs.getNegativeButton().setOnClickListener { dialogs.dismiss(); }
        dialogs.getNeutralButton().setOnClickListener { dialogs.dismiss();punchIn(PAUSAR) }
        dialogs.getPositiveBoton().setOnClickListener { dialogs.dismiss();punchIn(FINALIZAR) }
        dialogs.showDialog()
    }

    private fun iniciar() {
        val dialog: CustomDialog = FactoryDialogs.createDialog(activity, FactoryDialogs.TypeDialogs.BEGIN_DAY)
        dialog.getPositiveBoton().setOnClickListener { dialog.dismiss();punchIn(INICIAR) }
        dialog.getNegativeButton().setOnClickListener { dialog.dismiss() }
        dialog.showDialog()
    }

    private fun reanudar() {
        val dialog: CustomDialog = FactoryDialogs.createDialog(activity, FactoryDialogs.TypeDialogs.BEGIN_DAY)
        (dialog as BeginDayDialog).settingCustomText(
            "Reanudar Jornada",
            "¿Seguro que deseas Reanudar Jornada?"
        )
        dialog.getPositiveBoton().setOnClickListener { dialog.dismiss();punchIn(REANUDAR) }
        dialog.getNegativeButton().setOnClickListener { dialog.dismiss() }
        dialog.showDialog()
    }

    private fun finalizar() {
        val dialog: CustomDialog = FactoryDialogs.createDialog(activity, FactoryDialogs.TypeDialogs.BEGIN_DAY)
        (dialog as BeginDayDialog).settingCustomText(
            "Finalizar Jornada",
            "¿Seguro que deseas Finalizar Jornada?"
        )
        dialog.getPositiveBoton().setOnClickListener { dialog.dismiss();punchIn(FINALIZAR) }
        dialog.getNegativeButton().setOnClickListener { dialog.dismiss() }
        dialog.showDialog()
    }

    override fun emptyList() {
        showBottomState(INICIAR)
    }

    override fun showTimer(valueHour: String) {
        layout?.punchInHour?.text = valueHour
    }


    /*****************************************************************
     *  GESTION EVENTOS
     *****************************************************************/

    override fun showProgress() {
        progress_punchIn.visibility = VISIBLE
        if(!(progress_punchIn as LottieAnimationView).isAnimating) {
            (progress_punchIn as LottieAnimationView).playAnimation()
        }
    }

    override fun hideProgress() {
        if ((progress_punchIn as LottieAnimationView).isAnimating) {
            (progress_punchIn as LottieAnimationView).pauseAnimation()
        }
        progress_punchIn.visibility = View.GONE
    }

    override fun errorPunchInRetry(message: String?) {
        message?.let {
            val dialogs = FactoryDialogs.createDialog(activity, FactoryDialogs.TypeDialogs.GENERICDIALOG)
            (dialogs as GenericDialog).setCustomTitleMessageHtmlMessage(
                getString(R.string.login_error_title_punchin),
                it,
                GenericDialog.EnumDialogGenericButton.ONEBUTTON
            )
            dialogs.btnPositiveOneButton.setOnClickListener{
                dialogs.dismiss()
                presenter.loadEvents(defectLocation())
            }
            dialogs.showDialog()
        }
    }

    override fun updateChat(pendingMessageReader: String) {
        listener?.showPendingMessages(pendingMessageReader)
    }

    fun updateToggleButton(value: Boolean) {
        when (value) {
            true -> updateView()
            false -> layout?.punchInToogleButton?.gone()
        }
    }

    fun updateView() {
        listener?.enableEvents()
        layout?.punchInToogleButton?.show()
    }

    override fun eventsSucces(listEvents: ArrayList<EventPunchIn>) {
        adapter.updateValues(listEvents)
    }

    override fun eventsHandle(botonToShow: String, listEvents: ArrayList<EventPunchIn>) {
        this.stateButtom = botonToShow
        showBottomState(botonToShow)
        eventsSucces(listEvents)
        showResume(listEvents)
    }

    fun showResume(listEvents: ArrayList<EventPunchIn>){
       if(listEvents.size > 1) {
           punchInBtnResume.visibility = VISIBLE
           punchInBtnResume.setOnClickListener { makeResume(listEvents) }
       }
    }

    override fun initEvents(botonToShow: String, listEvents: ArrayList<EventPunchIn>) {
        punchInToogleButton.isEnabled = true
        this.stateButtom = botonToShow
        showBottomState(botonToShow)
        eventsSucces(listEvents)
        showResume(listEvents)
    }

    override fun endPunchIn(botonToShow: String, list: ArrayList<EventPunchIn>) {
        eventsSucces(list)
        showBottomState(botonToShow)
        punchInToogleButton.isEnabled = false
        punchInBtnResume.visibility = VISIBLE
        punchInBtnResume.setOnClickListener { makeResume(list) }
    }

    override fun errorNetwork() {
        punchInDate.showSnackbar("Necesitas internet para poder fichar")
    }



    @SuppressLint("CheckResult")
    private fun makeResume(listEvents: ArrayList<EventPunchIn>) {
        layout?.let {
            listener?.disableEvents()
            updateToggleButton(false)
            activity?.replaceFragmentAnim(
                ResumePunchIn.newInstance(listEvents, String.customDateNow("yyyy-MM-dd")),
                android.R.id.content,
                "resumeFragment"
            )
        }
    }

    override fun notCanPunchIn(message: String?) {
        if (isVisibleToUser) {
            message?.let { layout?.showSnackbar("No puede fichar por $it") }
        }
    }


    override fun eventsError(message: String?) {
        if (isVisibleToUser) {
            message?.let { layout?.showSnackbar(it) }
        }
        showBottomState(this.stateButtom)
    }

    private fun showBottomState(stateButtom: String = String.empty()) {
        when (stateButtom) {
            INICIAR -> btnInicar_Reanudar(getString(R.string.punchInIniciar))
            REANUDAR -> btnInicar_Reanudar(getString(R.string.punchInReanudar))
            PAUSAR -> btnPausar()
            FINALIZAR -> btnFinalizar()
            else -> endPunchIn()
        }
    }

    private fun endPunchIn() {
        punchInToogleButton.background = activity?.getDrawable(R.drawable.state_play)
        punchInBtnImg.setImageResource(R.drawable.ic_play)
        punchInBtnText.text = ""
    }

    private fun btnInicar_Reanudar(text: String) {
        punchInToogleButton.background = activity?.getDrawable(R.drawable.state_play)
        punchInBtnImg.setImageResource(R.drawable.ic_play)
        punchInBtnText.text = text
    }

    private fun btnPausar() {
        punchInToogleButton.background = activity?.getDrawable(R.drawable.state_stop)
        punchInBtnImg.setImageResource(R.drawable.ic_pause_white_24dp)
        punchInBtnText.text = activity?.getString(R.string.punchInPausarFinalizar)
    }

    private fun btnFinalizar() {
        punchInToogleButton.background = activity?.getDrawable(R.drawable.state_stop)
        punchInBtnImg.setImageResource(R.drawable.ic_stop)
        punchInBtnText.text = activity?.getString(R.string.punchInFinalizar)
    }

}
