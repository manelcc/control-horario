/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.documents.presenter

import android.net.Uri
import androidx.lifecycle.LifecycleObserver
import com.dinan.controlhorario.domain.model.documents.FileDocument
import com.dinan.controlhorario.domain.model.documents.FolderDocument
import com.example.android.architecture.blueprints.todoapp.BasePresenter
import com.example.android.architecture.blueprints.todoapp.BaseView
import java.io.File

interface DocumentsPresenter {

    interface Presenter : BasePresenter<View>, LifecycleObserver {
        fun loadFolders()
        fun loadFiles(id: String, directorio: String)
        fun updateDocument(nameFile: String, pathImage: String)
        fun updateDocumentGallery(nameFile: String, imageByteArray: File, contentURI: Uri?)
    }

    interface View : BaseView<Presenter> {
        fun folderError(message: String){}//only for folder
        fun folderSuccess(resultSuccess: ArrayList<FolderDocument>){}//only for folder
        fun successFilesDocuments(resultSuccess: java.util.ArrayList<FileDocument>){}//only for files
        fun errorFilesDocuments(message: String){}//only for files
        fun updateChat(pendingMessageReader: String){}//only for upload
        fun onErrorUpdateDocument(message: String){}//only for upload}
        fun successUpdateDocument(){}//only for upload
        fun showProgress(){}
        fun hideProgress(){}


    }
}