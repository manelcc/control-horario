/*
 * Copyright (C) 2019.  Daniel Jimenez
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.factoryDialogs


import android.app.Activity
import android.content.Context
import com.dinan.controlhorario.features.factoryDialogs.dialogs.*

abstract class FactoryDialogs {
    enum class TypeDialogs() {
        BEGIN_DAY, PAUSE_END_DAY, FINISH_SESSION, PERMISSION, ALERTLEGAL, GENERICDIALOG,NAMEDOC
    }
    companion object {
        fun createDialog(context: Context?, dialogType: TypeDialogs): CustomDialog {
            return when (dialogType) {
                TypeDialogs.BEGIN_DAY -> BeginDayDialog(context as Activity)
                TypeDialogs.FINISH_SESSION -> FinishSession(context as Activity)
                TypeDialogs.PERMISSION -> PermissionDialog(context as Activity)
                TypeDialogs.PAUSE_END_DAY -> PausedEndDayDialog(context as Activity)
                TypeDialogs.ALERTLEGAL -> AlertLegal(context as Activity)
                TypeDialogs.GENERICDIALOG ->GenericDialog(context as Activity)
                TypeDialogs.NAMEDOC ->NameDocUpdate(context as Activity)
            }
        }
    }
}