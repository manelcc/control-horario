/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.calendar.calendar.view.adapter.viewholder

import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.dinan.controlhorario.R
import com.dinan.controlhorario.domain.model.calendario.CalendarList
import com.dinan.controlhorario.extension.formatInputHourCalendar
import com.dinan.controlhorario.extension.formatOutputHourCalendar
import com.dinan.controlhorario.extension.showOrGone
import com.dinan.controlhorario.features.calendar.calendar.view.adapter.ClickItem
import kotlinx.android.synthetic.main.item_layout_calendar.view.*
import java.util.*

@Suppress("DEPRECATION")
class ViewHolderCalendar(
    itemView: View,
    val clickItem: ClickItem
) : RecyclerView.ViewHolder(itemView) {

    private lateinit var state: AppCompatTextView
    private lateinit var dayWeek: AppCompatTextView
    private lateinit var day: AppCompatTextView
    private lateinit var canEdit: AppCompatImageView

    fun bindView(calendar: CalendarList) {
        binds()
        listeners(calendar)
        day.text = calendar.diaMes
        dayWeek.text = calendar.diaSemana.capitalize()
        canEdit.showOrGone(calendar.canEdit)
        when (calendar.estado) {
            "Vacaciones" -> descanso(calendar)
            "Descanso" -> descanso(calendar)
            "Festivo" -> descanso(calendar)
            "Laboral" -> laboral(calendar)
            else -> otros(calendar)
        }
    }

    private fun binds() {
        canEdit = itemView.item_layout_calendar_can_edit
        day = itemView.item_layout_calendar_day
        dayWeek = itemView.item_layout_calendar_day_week
        state = itemView.item_layout_calendar_estado
    }

    private fun listeners(calendar: CalendarList) {
        canEdit.setOnClickListener{clickItem(calendar,true)}
        day.setOnClickListener{clickItem(calendar,false)}
        dayWeek.setOnClickListener{clickItem(calendar,false)}
        state.setOnClickListener{clickItem(calendar,false)}
    }

    private fun laboral(calendar: CalendarList) {
        when {
            calendar.fecha.before(Calendar.getInstance()) -> trabajadoItem(calendar)
            else -> horarioItem(calendar)
        }

    }

    private fun horarioItem(calendar: CalendarList) {
        state.setTextColor(itemView.context.resources.getColor(R.color.colorCalendarNormal))
        state.text = itemView.context.getString(R.string.turno,calendar.turno)
    }


    private fun trabajadoItem(calendar: CalendarList) {
        if (!calendar.horas.contains("-")) {
            state.run {
                setTextColor(itemView.context.resources.getColor(R.color.colorCalendarTrabajado))
                text = itemView.context.getString(R.string.formatHours,
                    formatOutputHourCalendar.format(formatInputHourCalendar.parse(calendar.horas).time))
            }
        } else {
            horarioItem(calendar)
        }
    }

    private fun descanso(calendar: CalendarList) {
        state.setTextColor(itemView.context.resources.getColor(R.color.colorCalendarDescando))
        state.text = calendar.estado

    }

    private fun otros(calendar: CalendarList) {
        state.setTextColor(itemView.context.resources.getColor(R.color.colorCalendarOtros))
        state.text = calendar.estado
    }
}