/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.documents.presenter.callbacks

import com.dinan.controlhorario.domain.core.base.DefaultObserver
import com.dinan.controlhorario.domain.model.documents.FolderDocument
import com.dinan.controlhorario.features.documents.presenter.DocumentsPresenter

class FolderCallBack(private val view: DocumentsPresenter.View) :
    DefaultObserver<ArrayList<FolderDocument>>() {

    override fun onNext(resultSuccess: ArrayList<FolderDocument>) {
        super.onNext(resultSuccess)
        if(resultSuccess.isNotEmpty()){
            view.folderSuccess(resultSuccess)
            if (resultSuccess.last().pendingMessageReader.isNotEmpty()) {
                view.updateChat(resultSuccess.last().pendingMessageReader)
            }
        }
    }

    override fun onError(exception: Throwable) {
        super.onError(exception)
        exception.message?.let { view.folderError(it) }
    }
}