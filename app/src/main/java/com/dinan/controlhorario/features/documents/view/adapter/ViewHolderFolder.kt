/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.documents.view.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.dinan.controlhorario.core.base.ItemCheck
import com.dinan.controlhorario.domain.model.documents.FolderDocument
import kotlinx.android.synthetic.main.item_layout_folder_document.view.*

class ViewHolderFolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val itemViewCheck: ItemCheck = itemView.context as ItemCheck

    fun bindView(folderDocument: FolderDocument) {
        itemView.txt_document_folder_name.text = folderDocument.nameFolder
        itemView.setOnClickListener { itemViewCheck.run { jumpFilesActivity(folderDocument.id,folderDocument.directory,folderDocument.nameFolder) } }
    }
}