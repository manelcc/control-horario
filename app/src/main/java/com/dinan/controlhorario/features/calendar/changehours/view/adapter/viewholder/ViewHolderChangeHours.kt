/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.calendar.changehours.view.adapter.viewholder

import android.app.TimePickerDialog
import android.view.View
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.dinan.controlhorario.R
import com.dinan.controlhorario.domain.model.punchin.EventPunchIn
import com.dinan.controlhorario.extension.empty
import com.dinan.controlhorario.features.calendar.changehours.presenter.ChangeHoursPresenter
import kotlinx.android.synthetic.main.item_layout_change_hours.view.*
import org.koin.core.KoinComponent
import org.koin.core.inject

class ViewHolderChangeHours(val listener: () -> Unit, itemView: View) : RecyclerView.ViewHolder(itemView),
    KoinComponent {


    private lateinit var clearHour: AppCompatImageView
    private lateinit var newUserHour: AppCompatTextView
    private lateinit var serverHour: AppCompatTextView
    private lateinit var actionTitle: AppCompatTextView

    val presenter: ChangeHoursPresenter.Presenter by inject()

    fun bindView(eventPunchIn: EventPunchIn) {
        bindItems()
        title(eventPunchIn.action)
        serverHour.text = eventPunchIn.hourTime
        newUserHour.setOnClickListener { getHourFromPicker(eventPunchIn) }
        clearHour.setOnClickListener { newUserHour.text = itemView.context.getString(R.string.it_change_hours_default_hour)
            presenter.changeHourItem(String.empty(),eventPunchIn)
            listener.invoke()
        }

    }

    private fun getHourFromPicker(eventPunchIn: EventPunchIn) {
        TimePickerDialog(itemView.context,
            TimePickerDialog.OnTimeSetListener { _, hourOfDay, minute ->
                val hourSelected:String = getHours(hourOfDay)
                val minuteSelected:String = getMinutes(minute)
                val finalHour = hourSelected.plus(":").plus(minuteSelected)
                newUserHour.text = finalHour
                presenter.changeHourItem(finalHour, eventPunchIn)
                listener.invoke()
            }, 0, 0, true).show()
    }

    private fun getMinutes(minute: Int): String {
        return if (minute < 10) {
            "0".plus(minute)
        } else {
            minute.toString()
        }
    }

    private fun getHours(hourOfDay: Int): String {
        return if (hourOfDay < 10) {
             "0".plus(hourOfDay)
         } else {
             hourOfDay.toString()
         }
    }


    private fun title(action: String) {
        when (action) {
            "iniciar" -> actionTitle.text = ActionChangeHours.INICIAR.action
            "pausar" -> actionTitle.text = ActionChangeHours.PAUSAR.action
            "finalizar" -> actionTitle.text = ActionChangeHours.FINALIZAR.action
            "reanudar" -> actionTitle.text = ActionChangeHours.REANUDAR.action
        }
    }

    private fun bindItems() {
        actionTitle = itemView.it_change_hours_title
        serverHour = itemView.it_change_hours_server_hour
        newUserHour = itemView.it_change_hours_et_hour
        clearHour = itemView.it_change_hours_clear_input
    }


    enum class ActionChangeHours(val action: String) {
        INICIAR("Hora de Inicio:"),
        FINALIZAR("Hora de Fin:"),
        PAUSAR("Hora de Pausa:"),
        REANUDAR("Hora de Reanudar:")
    }

}