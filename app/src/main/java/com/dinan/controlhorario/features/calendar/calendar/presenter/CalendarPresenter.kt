/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.calendar.calendar.presenter

import androidx.lifecycle.LifecycleObserver
import com.dinan.controlhorario.domain.model.calendario.CalendarList
import com.dinan.controlhorario.domain.model.calendario.Calendario
import com.dinan.controlhorario.domain.model.punchin.EventPunchIn
import com.example.android.architecture.blueprints.todoapp.BasePresenter
import com.example.android.architecture.blueprints.todoapp.BaseView

interface CalendarPresenter {

    interface Presenter : BasePresenter<View>, LifecycleObserver {
        fun loadCalendar(fecha : String)
        fun getHours(fecha: CalendarList, isResumeJob: Boolean)

    }

    interface View : BaseView<Presenter> {
        fun calendar(calendar: Calendario, fechaSolicitada: String)
        fun messagePending(pendingMessageReader: String)
        fun errorCalendar(fechaSolicitada: String)
        fun errorGetHours(message: String)
        fun showResumeJob(resultSuccess: List<EventPunchIn>, fecha: String)
        fun changeHours(resultSuccess: List<EventPunchIn>, fecha: String)
        fun showProgress()
        fun hideProgress()

    }
}