/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.factoryDialogs.dialogs

import android.app.Activity
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.text.HtmlCompat
import com.dinan.controlhorario.R
import com.dinan.controlhorario.features.factoryDialogs.CustomDialog
import kotlinx.android.synthetic.main.dialog_beginday.view.dialog_text_message
import kotlinx.android.synthetic.main.dialog_beginday.view.dialog_text_title
import kotlinx.android.synthetic.main.dialog_custom_title_message.view.*

class GenericDialog(context: Activity?) : BaseAlertDialogDinan(context), CustomDialog {

    enum class EnumDialogGenericButton { ONEBUTTON, TWOBUTTON }

    val dialogView: View by lazy {
        val viewGroup = context?.findViewById<ViewGroup>(android.R.id.content)
        LayoutInflater.from(context).inflate(R.layout.dialog_custom_title_message, viewGroup, false)
    }
    val alertDialog: AlertDialog by lazy { dialogMaterial(dialogView) }
    val btnPositiveOneButton: View by lazy { dialogView.dialog_custom_one_positiveButon }
    val btnPositiveTwoButton: View by lazy { dialogView.dialog_custom_two_positiveButon }
    val btnNegativeTwoButton: View by lazy { dialogView.dialog_custom_two_negative }


    fun setCustomTitleMessage(title: String, message: String, howButton: EnumDialogGenericButton = EnumDialogGenericButton.ONEBUTTON) {
        dialogView.dialog_text_title.text = title
        dialogView.dialog_text_message.text = message
            when (howButton) {
                EnumDialogGenericButton.ONEBUTTON -> makeOneButton()
                EnumDialogGenericButton.TWOBUTTON -> makeTwoButton()
            }
    }

    fun setCustomTitleMessageHtmlMessage(title: String, message: String, howButton: EnumDialogGenericButton = EnumDialogGenericButton.ONEBUTTON) {
        dialogView.dialog_text_title.text = title
        setMessageHtml(message)
        when (howButton) {
            EnumDialogGenericButton.ONEBUTTON -> makeOneButton()
            EnumDialogGenericButton.TWOBUTTON -> makeTwoButton()
        }
    }

    private fun setMessageHtml(message: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            dialogView.dialog_text_message.text = HtmlCompat.fromHtml(message, HtmlCompat.FROM_HTML_MODE_LEGACY)
        } else {
            dialogView.dialog_text_message.text= Html.fromHtml(message)
        }
    }


    private fun makeTwoButton() {
        dialogView.dialog_custom_one_positiveButon.visibility = GONE
        dialogView.dialog_custom_two_negative.visibility = VISIBLE
        dialogView.dialog_custom_two_positiveButon.visibility = VISIBLE
    }

    private fun makeOneButton() {
        dialogView.dialog_custom_one_positiveButon.visibility = VISIBLE
        dialogView.dialog_custom_two_negative.visibility = GONE
        dialogView.dialog_custom_two_positiveButon.visibility = GONE
    }

    override fun showDialog() {
        alertDialog.show()
    }



    override fun getPositiveBoton(): View {
        return btnPositiveOneButton
    }

    override fun getNegativeButton(): View {
        return btnPositiveOneButton
    }

    override fun dismiss() {
        alertDialog.dismiss()
    }

    override fun getNeutralButton(): View {
        return btnPositiveOneButton
    }

}




