/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.messages.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dinan.controlhorario.R
import com.dinan.controlhorario.core.exception.ExceptionTypeClient
import com.dinan.controlhorario.domain.model.chat.Interlocutor
import com.dinan.controlhorario.domain.model.chat.MessageChatList
import com.dinan.controlhorario.features.messages.view.adapter.viewholder.ViewHolderAdmin
import com.dinan.controlhorario.features.messages.view.adapter.viewholder.ViewHolderClient

class ChatAdapter(resultSuccess: java.util.ArrayList<MessageChatList>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val CLIENTE: Int = 2
    private val ADMINISTRADOR: Int = 1
    private val items: List<MessageChatList> = resultSuccess


    override fun getItemViewType(position: Int): Int {
        when (items.get(position).type) {
             Interlocutor.ADMIN -> return ADMINISTRADOR
            Interlocutor.CLIENT -> return CLIENTE
            else -> return ADMINISTRADOR
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            ADMINISTRADOR -> return ViewHolderAdmin(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_layout_chat_admin,
                    parent,
                    false
                )
            )
            CLIENTE -> return ViewHolderClient(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_layout_chat_user,
                    parent,
                    false
                )
            )
            else -> throw ExceptionTypeClient()
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is ViewHolderAdmin -> holder.bindView(items[position])
            is ViewHolderClient -> holder.bindView(items[position])
        }
    }

}