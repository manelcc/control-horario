/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.login.presenter.login

import android.content.Context
import android.content.SharedPreferences
import android.text.TextUtils
import androidx.annotation.VisibleForTesting
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.dinan.controlhorario.data.core.map.mapFromPreference
import com.dinan.controlhorario.domain.usecase.login.GetLoginUseCase
import com.dinan.controlhorario.extension.networkInfo
import com.dinan.controlhorario.features.login.presenter.callback.CallBackLogin

class LoginPresenterImpl(
    private val getLoginUseCase: GetLoginUseCase,
    val preferences: SharedPreferences,
    val context: Context
) :
    LoginPresenter.Presenter, LifecycleObserver {

    override lateinit var view: LoginPresenter.View

    override fun getUserSessionCif(): String {
        val userSession = mapFromPreference(preferences)
        return userSession?.cif ?: "32044652V"
    }


    override fun getUserSession(): Boolean {
        return mapFromPreference(preferences) != null
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    override fun onResumeView() {
        //empty method
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    override fun onDestroyView() {
        getLoginUseCase.dispose()
    }


    override fun logIn(cifEmpresa: String, emailUser: String, passUser: String) {
        context.networkInfo?.isAvailable?.let {
            view.showProgress()
            if (validateInputText(cifEmpresa, emailUser, passUser)) {
                getLoginUseCase.execute(
                    CallBackLogin(view),
                    GetLoginUseCase.ParamsLogin(cifEmpresa, emailUser, passUser)
                )
            }
        }
    }

    @VisibleForTesting
    fun validateInputText(cifEmpresa: String, emailUser: String, passUser: String): Boolean {
        var valid = true
        /*if (TextUtils.isEmpty(cifEmpresa) || cifEmpresa.length != 9) {
            view.errorInputCif()
            valid = false
        }*/

        if (TextUtils.isEmpty(emailUser) || emailUser.length < 5) {
            view.errorInputUser()
            valid = false
        }

        if (TextUtils.isEmpty(passUser) || passUser.length < 4) {
            view.errorInputPassword()
            valid = false
        }

        if (!valid) {
            view.hideProgress()
            view.enableInputs()
        }

        return valid
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun pausedFragment() {
        view.hideProgress()
    }


}