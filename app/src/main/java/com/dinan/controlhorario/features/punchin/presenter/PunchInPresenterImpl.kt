/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.punchin.presenter

import android.annotation.SuppressLint

import android.location.Location
import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.dinan.controlhorario.core.di.NetworkUtils
import com.dinan.controlhorario.domain.extension.asynCompose
import com.dinan.controlhorario.domain.usecase.GetPunchInUseCase

import com.dinan.controlhorario.features.punchin.presenter.callbacks.CallBackEvents

import io.reactivex.Observable.interval
import io.reactivex.disposables.Disposable
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class PunchInPresenterImpl(private val useCase: GetPunchInUseCase,private val networkUtils: NetworkUtils) : PunchInPresenter.Presenter, LifecycleObserver {

    override lateinit var view: PunchInPresenter.View
    lateinit var timerDispose: Disposable

    override fun launchPunchIn(stateButtom: String, location: Location?) {
        if(networkUtils.hasNetworkConnection()) {
            view.showProgress()
            val loc: Location = location ?: defectLocation()
            useCase.execute(
                CallBackEvents(view),
                GetPunchInUseCase.ParamsPunchIn(
                    GetPunchInUseCase.EventPunchIn.UPDATEEVENTS,
                    loc,
                    stateButtom
                )
            )
        }else{
            view.errorNetwork()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    override fun onResumeView() {
        timerDispose = interval(0, 1, TimeUnit.SECONDS)
            .timeInterval()
            .asynCompose()
            .subscribe(
                { successTimer() },
                { Log.e("manel", "tiempo ${it.message}") })

    }

    @SuppressLint("SimpleDateFormat")
    private fun successTimer() {
        val dateFormat = SimpleDateFormat("HH:mm:ss")
        val valueHour: String = dateFormat.format(Calendar.getInstance().time)
        view.showTimer(valueHour)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun onPausedView() {
        timerDispose.dispose()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    override fun onDestroyView() {
        useCase.dispose()
    }

    override fun loadEvents(location: Location) {
        if(networkUtils.hasNetworkConnection()) {
            view.showProgress()
            useCase.execute(
                CallBackEvents(view),
                GetPunchInUseCase.ParamsPunchIn(GetPunchInUseCase.EventPunchIn.LOADEVENTS, location)
            )
        }else{
            view.errorNetwork()
        }
    }

    private fun defectLocation(): Location {
        val defectLocation = Location("defecto")
        defectLocation.altitude = 1.00
        defectLocation.longitude = 1.00
        return defectLocation
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun pausedFragment() {
        view.hideProgress()
    }

}