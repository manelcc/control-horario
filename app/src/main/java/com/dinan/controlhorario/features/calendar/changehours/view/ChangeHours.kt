/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.calendar.changehours.view


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.dinan.controlhorario.R
import com.dinan.controlhorario.core.base.BaseFragment
import com.dinan.controlhorario.domain.model.punchin.EventPunchIn
import com.dinan.controlhorario.extension.customDate
import com.dinan.controlhorario.features.calendar.changehours.presenter.ChangeHoursPresenter
import com.dinan.controlhorario.features.calendar.changehours.view.adapter.AdapterChangeHours
import com.dinan.controlhorario.features.factoryDialogs.FactoryDialogs
import com.dinan.controlhorario.features.factoryDialogs.dialogs.GenericDialog
import com.google.android.material.button.MaterialButton
import com.google.android.material.textview.MaterialTextView
import kotlinx.android.synthetic.main.fragment_change_hours.*
import kotlinx.android.synthetic.main.fragment_change_hours.view.*
import org.koin.android.ext.android.inject

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class ChangeHours : BaseFragment<ChangeHoursPresenter.Presenter, ChangeHoursPresenter.View>(),
    ChangeHoursPresenter.View {


    override val view: ChangeHoursPresenter.View
        get() = this


    override val presenter: ChangeHoursPresenter.Presenter by inject()

    private lateinit var observation: AppCompatEditText
    private lateinit var closeFragment: AppCompatImageView
    private lateinit var btnRequest: MaterialButton
    private lateinit var btnCancel: MaterialButton
    private lateinit var adapter: AdapterChangeHours
    private lateinit var recycler: RecyclerView
    private lateinit var date: MaterialTextView
    private var listEvents: ArrayList<EventPunchIn>? = null
    private var fecha: String? = null


    override fun setUpToolbarFromFragment() {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activity?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN)

        arguments?.let {
            listEvents = it.getParcelableArrayList(ARG_PARAM1)
            fecha = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val fragmentLayout: View =
            inflater.inflate(R.layout.fragment_change_hours, container, false)
        bindViews(fragmentLayout)
        listenerButtons()
        date.text = String.customDate("EEEE dd MMMM yyyy", fecha ?: "")
        initRecycler()
        return fragmentLayout
    }

    private fun listenerButtons() {
        btnCancel.setOnClickListener { activity?.onBackPressed() }
        btnRequest.setOnClickListener {
            presenter.requestChangeHours(
                listEvents ?: ArrayList(),
                observation.text.toString()
            )
        }
        closeFragment.setOnClickListener { activity?.onBackPressed() }
    }

    private fun initRecycler() {
        adapter = AdapterChangeHours(listEvents ?: ArrayList()) {
            btnRequest.isEnabled = listEvents?.all { it.hourSelected.isEmpty() } == false
        }
        recycler.adapter = adapter
    }

    private fun bindViews(fragmentLayout: View) {
        date = fragmentLayout.fragment_change_hour_date
        recycler = fragmentLayout.fragment_change_hour_recycler
        btnCancel = fragmentLayout.fragment_change_hour_btn_cancel
        btnRequest = fragmentLayout.fragment_change_hour_btn_request
        closeFragment = fragmentLayout.fragment_change_hour_close
        observation = fragmentLayout.fragment_change_hour_obs
    }

    override fun successChangeHour() {
        val dialogs =
            FactoryDialogs.createDialog(activity, FactoryDialogs.TypeDialogs.GENERICDIALOG)
        (dialogs as GenericDialog).setCustomTitleMessage(
            getString(R.string.fragment_changehour_sucesstitle),
            getString(R.string.fragment_changehour_succesMesage),
            GenericDialog.EnumDialogGenericButton.ONEBUTTON
        )
        dialogs.btnPositiveOneButton.setOnClickListener {
            dialogs.dismiss()
            activity?.onBackPressed()
        }
        dialogs.showDialog()
    }

    private fun showMessageError(title: String, message: String) {
        val dialogs =
            FactoryDialogs.createDialog(activity, FactoryDialogs.TypeDialogs.GENERICDIALOG)
        (dialogs as GenericDialog).setCustomTitleMessage(
            title,
            message,
            GenericDialog.EnumDialogGenericButton.ONEBUTTON
        )
        dialogs.btnPositiveOneButton.setOnClickListener {
            dialogs.dismiss()
        }
        dialogs.showDialog()
    }

    override fun errorChangeHour() {
        showMessageError(getString(R.string.fragment_changehour_kotitle), getString(R.string.fragment_changehour_komessage))
    }

    override fun emptyListEvents() {
        showMessageError(
            getString(R.string.fragment_changehour_kotitle),
            getString(R.string.fragment_changehour_error_no_values_message)
        )
    }

    override fun showProgress() {
        progressChangeHour.visibility = View.VISIBLE
        if(!(progressChangeHour as LottieAnimationView).isAnimating) {
            (progressChangeHour as LottieAnimationView).playAnimation()
        }
    }

    override fun hideProgress() {
        if ((progressChangeHour as LottieAnimationView).isAnimating) {
            (progressChangeHour as LottieAnimationView).pauseAnimation()
        }
        progressChangeHour.visibility = View.GONE
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param eventList Parameter 1.
         * @param fecha Parameter 2.
         * @return A new instance of fragment ChangeHours.
         */
        @JvmStatic
        fun newInstance(eventList: ArrayList<EventPunchIn>, fecha: String) =
            ChangeHours().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList(ARG_PARAM1, eventList)
                    putString(ARG_PARAM2, fecha)
                }
            }
    }

}
