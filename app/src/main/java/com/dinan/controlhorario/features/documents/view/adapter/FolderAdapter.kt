/*
 * Copyright (C) 2019.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.documents.view.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.dinan.controlhorario.R
import com.dinan.controlhorario.domain.model.documents.FolderDocument

class FolderAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val items: ArrayList<FolderDocument> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
        ViewHolderFolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_layout_folder_document,
                parent,
                false
            )
        )

    override fun getItemCount(): Int = items.size


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolderFolder).bindView(items[position])
    }

    fun updateFolderItems(resultSuccess: java.util.ArrayList<FolderDocument>) {
        items.clear()
        items.addAll(resultSuccess)
        notifyDataSetChanged()
    }
}