/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.documents.view


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.dinan.controlhorario.R
import com.dinan.controlhorario.core.base.BaseFragment
import com.dinan.controlhorario.domain.model.documents.FolderDocument
import com.dinan.controlhorario.extension.showSnackbar
import com.dinan.controlhorario.features.commons.ItemClickHolder
import com.dinan.controlhorario.features.documents.presenter.DocumentsPresenter
import com.dinan.controlhorario.features.documents.view.adapter.FolderAdapter
import kotlinx.android.synthetic.main.fragment_documents.*
import kotlinx.android.synthetic.main.fragment_documents.view.*
import org.koin.android.ext.android.inject
import java.util.*


/**
 * A simple [Fragment] subclass.
 *
 */
class DocumentsFolderFragment : BaseFragment<DocumentsPresenter.Presenter,DocumentsPresenter.View>(), DocumentsPresenter.View, ItemClickHolder {

    override val view: DocumentsPresenter.View
        get() = this

    private lateinit var adapter: FolderAdapter
    private lateinit var recyclerDocument: RecyclerView
    override val presenter: DocumentsPresenter.Presenter by inject()

    override fun setUpToolbarFromFragment() {
        listener?.setUpTitleToolbar(getString(R.string.home_text_documentos))
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val layout: View = inflater.inflate(R.layout.fragment_documents, container, false)
        bindView(layout)
        presenter.loadFolders()
        return layout
    }

    private fun bindView(layout: View) {
        adapter = FolderAdapter()
        recyclerDocument = layout.document_fragment_folder_recycler
    }

    override fun folderError(message: String) {
        recyclerDocument.showSnackbar(message)
    }

    override fun folderSuccess(resultSuccess: ArrayList<FolderDocument>) {
        recyclerDocument.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )
        recyclerDocument.adapter = adapter
        adapter.updateFolderItems(resultSuccess)
    }

    override fun eventActionFolder(folderDocument: FolderDocument) {
        Log.e("manel", "si que hemos entrado")
    }

    override fun updateChat(pendingMessageReader: String) {
        listener?.showPendingMessages(pendingMessageReader)
    }

    override fun showProgress() {
        progressDocumentFolder.let {
            progressDocumentFolder.visibility = View.VISIBLE
            if (!(progressDocumentFolder as LottieAnimationView).isAnimating) {
                (progressDocumentFolder as LottieAnimationView).playAnimation()
            }
        }
    }

    override fun hideProgress() {
        progressDocumentFolder.let {
            if ((progressDocumentFolder as LottieAnimationView).isAnimating) {
                (progressDocumentFolder as LottieAnimationView).pauseAnimation()
            }
            progressDocumentFolder.visibility = View.GONE
        }
    }

}

