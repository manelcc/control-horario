/*
 * Copyright (C) 2020.  Manel Cabezas
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.punchin.presenter.callbacks

import com.dinan.controlhorario.data.exception.ErrorCanPuchIn
import com.dinan.controlhorario.data.exception.ErrorPunchIn
import com.dinan.controlhorario.data.extension.FINALIZAR
import com.dinan.controlhorario.data.extension.INICIAR
import com.dinan.controlhorario.data.extension.PAUSAR
import com.dinan.controlhorario.data.extension.REANUDAR
import com.dinan.controlhorario.domain.core.base.DefaultObserver
import com.dinan.controlhorario.domain.model.punchin.PunchInModel
import com.dinan.controlhorario.features.punchin.presenter.PunchInPresenter


class CallBackEvents(private val view: PunchInPresenter.View) :
    DefaultObserver<PunchInModel>() {

    override fun onComplete() {
       view.hideProgress()
    }

    override fun onNext(resultSuccess: PunchInModel) {
        if (resultSuccess.list.isNotEmpty()) {
            view.updateChat(resultSuccess.list.last().pendingMessageReader)
        }
        when (resultSuccess.botonToShow) {
            INICIAR -> view.initEvents(resultSuccess.botonToShow, resultSuccess.list)
            REANUDAR -> view.eventsHandle(resultSuccess.botonToShow, resultSuccess.list)
            PAUSAR -> view.eventsHandle(resultSuccess.botonToShow, resultSuccess.list)
            FINALIZAR -> view.eventsHandle(resultSuccess.botonToShow, resultSuccess.list)
            else -> view.endPunchIn(resultSuccess.botonToShow, resultSuccess.list)
        }
    }

    override fun onError(exception: Throwable) {
        when(exception){
            is ErrorCanPuchIn-> view.notCanPunchIn(exception.message)
            is ErrorPunchIn -> view.errorPunchInRetry(exception.message)
            else -> view.eventsError(exception.message)
        }
    }

}
