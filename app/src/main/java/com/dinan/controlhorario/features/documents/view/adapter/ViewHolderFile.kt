/*
 * Copyright (C) 2019.  Manel Cabezas 
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.documents.view.adapter

import android.content.Intent
import android.net.Uri
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.dinan.controlhorario.domain.model.documents.FileDocument
import kotlinx.android.synthetic.main.item_layout_files_document.view.*

class ViewHolderFile(val inflate: View) : RecyclerView.ViewHolder(inflate) {


    fun bindView(fileDocument: FileDocument) {
        with(inflate){
            item_layout_files_document_day_month.text = fileDocument.monthDay.plus("\n").plus(fileDocument.year)
            //item_layout_files_document_year.text = fileDocument.year
            item_layout_files_document_name_file.text = fileDocument.nameFile
            inflate.setOnClickListener {
                val intent: Intent = Intent()
                intent.setAction(Intent.ACTION_VIEW)
                intent.setData(Uri.parse(fileDocument.urlDocument))
                inflate.context.startActivity(intent)
            } }
        }
    }


