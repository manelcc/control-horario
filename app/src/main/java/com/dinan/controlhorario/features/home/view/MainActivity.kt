/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.home.view


import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.ViewPager
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.dinan.controlhorario.R
import com.dinan.controlhorario.core.base.ItemCheck
import com.dinan.controlhorario.core.base.globalConstants.KEY_PUSH_SCREEN
import com.dinan.controlhorario.core.base.globalConstants.PUSH_GO_CHAT
import com.dinan.controlhorario.core.base.globalConstants.PUSH_GO_PUNCHIN
import com.dinan.controlhorario.domain.model.login.User
import com.dinan.controlhorario.extension.gone
import com.dinan.controlhorario.extension.hideSoftKeyboard
import com.dinan.controlhorario.extension.show
import com.dinan.controlhorario.features.calendar.calendar.view.CalendarFragment
import com.dinan.controlhorario.features.commons.OnCardViewClickListener
import com.dinan.controlhorario.features.documents.view.DocumentsFolderFragment
import com.dinan.controlhorario.features.firebasecloud.FirebaseWork
import com.dinan.controlhorario.features.home.view.PropertiesHome.CALENDARIO
import com.dinan.controlhorario.features.home.view.PropertiesHome.DOCUMENTOS
import com.dinan.controlhorario.features.home.view.PropertiesHome.FICHAR
import com.dinan.controlhorario.features.home.view.PropertiesHome.HOME
import com.dinan.controlhorario.features.home.view.PropertiesHome.MENSAJES
import com.dinan.controlhorario.features.home.view.adapters.PagerAdapter
import com.dinan.controlhorario.features.messages.view.MessagesFragment
import com.dinan.controlhorario.features.punchin.view.PunchInFragment
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.ext.android.inject


class MainActivity : AppCompatActivity(),
    OnCardViewClickListener, ItemCheck {

    override val itemHolderCheckContext = this
    private lateinit var adapter: PagerAdapter
    private var prevBottomSelected: MenuItem? = null
    private val user: User by inject()

    companion object {
        const val ACCESSPUNCHINCALENDAR = "1100"
        const val ACCESSPLUSDOC = "1110"
        const val ACCESSPLUSCHAT = "1101"
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setUpViewPager(getPagerAdapter())
        setUpBottomNavigationBar()
        enableEventsAccess()
        sendTokenMessageToServer()
        gotoChart(intent)
    }

    private fun enableEventsAccess() {
        fun disableDocAndMessage() {
            val menu: Menu = bottomNavigation.menu
            menu.findItem(R.id.bottom_nav_documents).isEnabled = false
            menu.findItem(R.id.bottom_nav_messages).isEnabled = false
        }

        fun disableChat() {
            val menu: Menu = bottomNavigation.menu
            menu.findItem(R.id.bottom_nav_messages).isEnabled = false
        }

        fun disableDocument() {
            val menu: Menu = bottomNavigation.menu
            menu.findItem(R.id.bottom_nav_documents).isEnabled = false
        }

        when (user.accessUser) {
            ACCESSPUNCHINCALENDAR -> disableDocAndMessage()
            ACCESSPLUSDOC -> disableChat()
            ACCESSPLUSCHAT -> disableDocument()
        }
    }


    private fun sendTokenMessageToServer() {
        val work = OneTimeWorkRequest.Builder(FirebaseWork.WorkInit::class.java).build()
        WorkManager.getInstance(applicationContext).enqueue(work)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        gotoChart(intent)
    }

    private fun gotoChart(intent: Intent) {
        val message = intent.extras?.getString(KEY_PUSH_SCREEN, "")
        message?.let {
            when (it) {
                PUSH_GO_CHAT -> navigateTo(MENSAJES)
                PUSH_GO_PUNCHIN -> navigateTo(FICHAR)
            }
        }
    }

    private fun getPagerAdapter(): PagerAdapter {
        adapter = PagerAdapter(supportFragmentManager)
        adapter.addFragment(HomeFragment.newInstance())
        adapter.addFragment(PunchInFragment())
        adapter.addFragment(CalendarFragment())
        adapter.addFragment(DocumentsFolderFragment())
        adapter.addFragment(MessagesFragment())
        return adapter
    }

    private fun setUpViewPager(adapter: PagerAdapter) {
        viewPager.adapter = adapter
        viewPager.offscreenPageLimit = 5
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {}

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {}

            override fun onPageSelected(position: Int) {
                if (prevBottomSelected == null) {
                    bottomNavigation.menu.getItem(0).isChecked = false
                } else {
                    prevBottomSelected!!.isChecked = false
                }
                bottomNavigation.menu.getItem(position).isChecked = true
                prevBottomSelected = bottomNavigation.menu.getItem(position)
            }

        })

    }

    private fun setUpBottomNavigationBar() {
        bottomNavigation.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.bottom_nav_home -> {
                    hideSoftKeyboard()
                    navigateTo(HOME); true
                }
                R.id.bottom_nav_punch_in -> {
                    hideSoftKeyboard()
                    navigateTo(FICHAR); true
                }
                R.id.bottom_nav_calendar -> {
                    hideSoftKeyboard()
                    navigateTo(CALENDARIO); true
                }
                R.id.bottom_nav_documents -> {
                    hideSoftKeyboard()
                    navigateTo(DOCUMENTOS); true
                }
                R.id.bottom_nav_messages -> {
                    navigateTo(MENSAJES); true
                }
                else -> false
            }
        }
    }

    override fun cardClickFichar() {
        navigateTo(FICHAR)
    }

    override fun cardClickDocumentos() {
        navigateTo(DOCUMENTOS)
    }

    override fun cardClickCalendario() {
        navigateTo(CALENDARIO)
    }

    override fun cardClickMensajes() {
        navigateTo(MENSAJES)
    }

    fun navigateTo(valueItem: Int) {
        viewPager.currentItem = valueItem
    }

    override fun disableEvents() {
        bottomNavigation.gone()
    }

    override fun enableEvents() {
        bottomNavigation.show()
    }

    override fun goHome() {
        navigateTo(HOME)
    }

    override fun setUpTitleToolbar(titleToolbar: String?) {
        when {
            titleToolbar.isNullOrEmpty() -> supportActionBar?.hide()
            else -> {
                appBar.show();setSupportActionBar(toolbar);supportActionBar?.show();supportActionBar?.title =
                    titleToolbar
            }
        }
    }

    override fun showPendingMessages(pendingMessageReader: String) {
        val messageNotReader = pendingMessageReader.toInt()
        if (messageNotReader > 0) {
            bottomNavigation.getOrCreateBadge(R.id.bottom_nav_messages).number = messageNotReader
        } else {
            bottomNavigation.removeBadge(R.id.bottom_nav_messages)
        }
    }
}
