/*
 * Copyright (C) 2020.  Manel Cabezas
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.login.presenter.callback

import com.dinan.controlhorario.domain.core.base.DefaultObserver
import com.dinan.controlhorario.domain.model.login.User
import com.dinan.controlhorario.features.login.presenter.login.LoginPresenter

class CallBackLogin(val view: LoginPresenter.View) : DefaultObserver<User>() {


    override fun onNext(resultSuccess: User) {
        super.onNext(resultSuccess)
        when {
            resultSuccess.lopd && resultSuccess.changePass -> view.goHome()
            !resultSuccess.lopd && !resultSuccess.changePass -> view.lopdAndChangePass()
            resultSuccess.lopd && !resultSuccess.changePass -> view.onlyChangePass()
            !resultSuccess.lopd && resultSuccess.changePass -> view.onlyLopd()
        }
    }

    override fun onComplete() {
        super.onComplete()
        view.hideProgress()
    }

    override fun onError(exception: Throwable) {
        super.onError(exception)
        view.hideProgress()
        view.loginError(exception.message ?: "viene vacio")
    }
}
