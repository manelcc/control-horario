/*
 * Copyright (C) 2019.  Manel Cabezas Calderó
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.punchin.view.adapter.viewholder

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.dinan.controlhorario.domain.model.punchin.EventPunchIn
import com.dinan.controlhorario.extension.Properties.DATEFORMAT_STRING_NORMALCALENDAR
import com.dinan.controlhorario.extension.customDate
import com.dinan.controlhorario.extension.gone
import com.dinan.controlhorario.extension.show
import kotlinx.android.synthetic.main.item_layout_punchin_timeline.view.*

class ViewHolderPunchInEvents(itemView: View) : RecyclerView.ViewHolder(itemView) {


    fun bindView(
        position: Int,
        itemValue: EventPunchIn,
        fromPunchIn: Boolean
    ) {
        if (position == 0 && !fromPunchIn) {
            itemView.item_layout_punchin_img_line.gone()
        } else {
            itemView.item_layout_punchin_img_line.show()
        }

        itemView.punchInTimeLineDate.text = String.customDate(DATEFORMAT_STRING_NORMALCALENDAR, itemValue.dataTime)
        itemView.punchInTimeLineHour.text = itemValue.hourTime
    }

}