/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.calendar.calendar.view


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.airbnb.lottie.LottieAnimationView
import com.dinan.controlhorario.R
import com.dinan.controlhorario.core.base.BaseFragment
import com.dinan.controlhorario.domain.model.calendario.CalendarList
import com.dinan.controlhorario.domain.model.calendario.Calendario
import com.dinan.controlhorario.domain.model.punchin.EventPunchIn
import com.dinan.controlhorario.extension.*
import com.dinan.controlhorario.features.calendar.calendar.presenter.CalendarPresenter
import com.dinan.controlhorario.features.calendar.calendar.view.adapter.AdapterCalendar
import com.dinan.controlhorario.features.calendar.changehours.view.ChangeHours
import com.dinan.controlhorario.features.punchin.view.ResumePunchIn
import kotlinx.android.synthetic.main.fragment_calendar.*
import kotlinx.android.synthetic.main.fragment_calendar.view.*
import org.koin.android.ext.android.inject
import java.util.*


class CalendarFragment : BaseFragment<CalendarPresenter.Presenter, CalendarPresenter.View>(),
    CalendarPresenter.View {

    override val view: CalendarPresenter.View
        get() = this

    private lateinit var adapter: AdapterCalendar
    private var selectedCalendar: Calendar = Calendar.getInstance()
    private lateinit var calendarRecycler: RecyclerView
    private lateinit var fowardMonth: AppCompatImageView
    private lateinit var backMonth: AppCompatImageView
    private lateinit var calendarMonth: TextView
    private lateinit var calendarYear: TextView
    override val presenter: CalendarPresenter.Presenter by inject()
    private val currentMonth: String = String.empty()

    override fun setUpToolbarFromFragment() {
        listener?.setUpTitleToolbar(getString(R.string.home_text_calendar))
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        if (isVisibleToUser) {
            setUpToolbarFromFragment()
            presenter.loadCalendar(currentMonth)
        }

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val layout: View = inflater.inflate(R.layout.fragment_calendar, container, false)
        bindViews(layout)
        return layout
    }

    private fun bindViews(layout: View) {
        calendarYear = layout.calendar_fragment_txt_year as TextView
        calendarMonth = layout.calendar_fragment_txt_month as TextView
        backMonth = layout.calendar_fragment_back_month as AppCompatImageView
        fowardMonth = layout.calendar_fragment_foward_month as AppCompatImageView
        calendarRecycler = layout.calendar_fragment_recycler as RecyclerView
    }

    override fun calendar(calendar: Calendario, fechaSolicitada: String) {
        listenersArrows()
        adapter = AdapterCalendar { calendarList: CalendarList, isArrow: Boolean ->
            when (isArrow) {
                true -> presenter.getHours(calendarList, false)
                false -> presenter.getHours(calendarList, true)
            }
        }
        calendarRecycler.setHasFixedSize(true)
        calendarRecycler.adapter = adapter
        calendarRecycler.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))

        if (calendar.calendar.isNotEmpty()) {
            calendarNotEmpty(calendar)
        } else {
            calendarEmpty(fechaSolicitada, calendar)
        }

    }

    private fun calendarEmpty(fechaSolicitada: String, calendar: Calendario) {
        try {
            formatDefaultPattern.parse(fechaSolicitada)?.let {
                this.selectedCalendar.time = it
            }
        } catch (exc: Exception) {
            Log.e("manel", "error pendiente")
        }
        updateViews()
        adapter.updateItems(calendar.calendarList)
    }

    private fun calendarNotEmpty(calendar: Calendario) {
        formatDefaultPattern.parse(calendar.calendar)?.let {
            this.selectedCalendar.time = it
        }
        updateViews()
        adapter.updateItems(calendar.calendarList)
    }

    override fun errorCalendar(fechaSolicitada: String) {
        listenersArrows()
        try {
            formatDefaultPattern.parse(fechaSolicitada)?.let {

                this.selectedCalendar.time = it
            }
        } catch (exc: Exception) {
            Log.e("manel", "error pendiente")
        }

        updateViews()
        //adapter?.emptyList()
    }

    override fun errorGetHours(message: String) {
        calendarMonth.showSnackbar(getString(R.string.error_calendar_getHours))
    }

    override fun showResumeJob(resultSuccess: List<EventPunchIn>, fecha: String) {
        activity?.replaceFragmentAnim(
            ResumePunchIn.newInstance(resultSuccess as ArrayList<EventPunchIn>, fecha),
            android.R.id.content,
            "resumeFragment"
        )
    }

    override fun changeHours(
        resultSuccess: List<EventPunchIn>,
        fecha: String
    ) {
        activity?.replaceFragmentAnim(
            ChangeHours.newInstance(
                resultSuccess as ArrayList<EventPunchIn>,
                fecha
            ),
            android.R.id.content,
            "changeHours"
        )
    }

    private fun listenersArrows() {
        fowardMonth.setOnClickListener {
            selectedCalendar.addMonth();presenter.loadCalendar(
            formatDefaultPattern.format(selectedCalendar.time)
        )
        }
        backMonth.setOnClickListener {
            selectedCalendar.lessMonth();presenter.loadCalendar(
            formatDefaultPattern.format(selectedCalendar.time)
        )
        }
    }

    private fun updateViews() {
        calendarYear.text = selectedCalendar.get(Calendar.YEAR).toString()
        calendarMonth.text = formatFullMonth.format(selectedCalendar.time).capitalize()
    }

    override fun messagePending(pendingMessageReader: String) {
        listener?.showPendingMessages(pendingMessageReader)
    }

    override fun showProgress() {
        progressCalendar.visibility = View.VISIBLE
        if(!(progressCalendar as LottieAnimationView).isAnimating) {
            (progressCalendar as LottieAnimationView).playAnimation()
        }
    }

    override fun hideProgress() {
        if ((progressCalendar as LottieAnimationView).isAnimating) {
            (progressCalendar as LottieAnimationView).pauseAnimation()
        }
        progressCalendar.visibility = View.GONE
    }


}
