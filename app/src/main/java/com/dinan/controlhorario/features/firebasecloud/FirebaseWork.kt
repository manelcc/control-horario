/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.firebasecloud

import android.content.Context
import android.util.Log
import androidx.work.*
import com.crashlytics.android.Crashlytics
import com.dinan.controlhorario.core.base.globalConstants.KEY_STRING_TOKEN
import com.dinan.controlhorario.core.base.globalConstants.TAG
import com.dinan.controlhorario.data.datasource.chat.ChatDataSource
import com.google.firebase.iid.FirebaseInstanceId
import io.reactivex.Single
import org.koin.core.KoinComponent
import org.koin.core.inject

class FirebaseWork(appContext: Context, workerParams: WorkerParameters) : RxWorker(appContext, workerParams), KoinComponent {
    override fun createWork(): Single<Result> {

        val networkRepo: ChatDataSource by inject()
        var newToken: String? = inputData.getString(KEY_STRING_TOKEN)
        if (newToken == null) {
            newToken = ""
        }
        return networkRepo.sendNewToken(newToken).map {
            when (it.ok) {
                "1" -> Result.success()
                else -> failSendToken()
            }
        }.onErrorReturn { failSendToken() }
    }

    private fun failSendToken(): Result {
        Crashlytics.log(Log.ERROR, TAG, "NO SE HA ENVIADO EL TOKEN")
        return Result.failure()
    }

    class WorkInit(appContext: Context, workerParams: WorkerParameters) : Worker(appContext, workerParams) {
        override fun doWork(): Result {
            Log.e(WorkInit::class.java.simpleName,"iniciamos envio token firebase")
            FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener { task ->
                val inputData = Data.Builder().putString(KEY_STRING_TOKEN, task.result?.token).build()
                val work =
                    OneTimeWorkRequest.Builder(FirebaseWork::class.java).setInputData(inputData).build()
                WorkManager.getInstance(applicationContext).beginWith(work).enqueue()
            }
            return Result.success()
        }
    }
}