/*
 * Copyright (C) 2019.  Daniel Jimenez
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.login.presenter.legal

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.dinan.controlhorario.domain.usecase.login.GetAcceptLopdImpl
import com.dinan.controlhorario.domain.usecase.login.GetLegalUseCaseImpl
import com.dinan.controlhorario.features.login.presenter.callback.CallBackAceptLopd
import com.dinan.controlhorario.features.login.presenter.callback.CallBackLopd

class LegalPresenterImpl(private val useCase: GetLegalUseCaseImpl, private val acceptUseCase: GetAcceptLopdImpl) :
    LegalPresenter.Presenter, LifecycleObserver {

    override lateinit var view: LegalPresenter.View


    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    override fun onResumeView() {
        useCase.execute(CallBackLopd(view), Unit)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    override fun onDestroyView() {
        useCase.dispose()

    }

    override fun acceptLopd() {
        acceptUseCase.execute(CallBackAceptLopd(view), Unit)
    }

}