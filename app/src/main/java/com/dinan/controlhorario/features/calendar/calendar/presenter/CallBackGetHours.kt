/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.calendar.calendar.presenter

import android.util.Log
import com.dinan.controlhorario.domain.core.base.DefaultObserver
import com.dinan.controlhorario.domain.model.punchin.EventPunchIn
import com.dinan.controlhorario.extension.empty

class CallBackGetHours(
    val view: CalendarPresenter.View,
    val fecha: String,
    val isResumeJob: Boolean
) : DefaultObserver<List<EventPunchIn>>() {

    override fun onNext(resultSuccess: List<EventPunchIn>) {
        when(isResumeJob){
            true -> view.showResumeJob(resultSuccess,fecha)
            false -> view.changeHours(resultSuccess,fecha)
        }
    }

    override fun onComplete() {
        view.hideProgress()
       Log.e(javaClass.simpleName,"complete Hours")
    }

    override fun onError(exception: Throwable) {
        super.onError(exception)
        view.hideProgress()
        view.errorGetHours(exception.message?:String.empty())
    }
}