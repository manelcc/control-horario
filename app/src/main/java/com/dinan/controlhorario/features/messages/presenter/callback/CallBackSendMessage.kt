/*
 * Copyright (C) 2019.  Manel Cabezas 
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.messages.presenter.callback

import com.dinan.controlhorario.domain.core.base.DefaultObserver
import com.dinan.controlhorario.features.messages.presenter.ChatPresenter

class CallBackSendMessage(val view: ChatPresenter.View) : DefaultObserver<Boolean>() {
    override fun onNext(resultSuccess: Boolean) {
        super.onNext(resultSuccess)
        view.succesSendChat()
    }

    override fun onError(exception: Throwable) {
        super.onError(exception)
        view.errorSendMessage(exception.message)
    }
}
