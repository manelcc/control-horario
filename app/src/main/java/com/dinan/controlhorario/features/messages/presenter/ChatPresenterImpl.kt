/*
 * Copyright (C) 2019.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.messages.presenter

import android.text.TextUtils
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.dinan.controlhorario.domain.usecase.GetMessagesList
import com.dinan.controlhorario.domain.usecase.SendMessageUseCase
import com.dinan.controlhorario.features.messages.presenter.callback.CallBackMessage

class ChatPresenterImpl(
    private val getAllMessagesList: GetMessagesList,
    private val sendMessageUseCase: SendMessageUseCase
) : ChatPresenter.Presenter,
    LifecycleObserver {

    override lateinit var view: ChatPresenter.View

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    override fun onResumeView() {
        //empty method
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    override fun onDestroyView() {
        getAllMessagesList.dispose()
    }

    override fun getAllChats(){
        getAllMessagesList.execute(
            CallBackMessage(
                view
            ), Unit
        )
    }

    override fun sendMessage(message: String) {
        if (TextUtils.isEmpty(message)) {
            view.errorMessageEmpty()
        } else {
            sendMessageUseCase.execute(
                CallBackMessage(view),
                SendMessageUseCase.Params(message.trimEnd())
            )
        }
    }
}