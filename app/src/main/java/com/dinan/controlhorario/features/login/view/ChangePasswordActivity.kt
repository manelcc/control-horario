/*
 * Copyright (C) 2019.  Daniel Jimenez
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.login.view

import android.app.Activity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide
import com.dinan.controlhorario.R
import com.dinan.controlhorario.extension.showSnackbar
import com.dinan.controlhorario.features.login.presenter.changepass.ChangePassPresenter
import kotlinx.android.synthetic.main.activity_change_password.*
import org.koin.android.ext.android.inject

class ChangePasswordActivity : AppCompatActivity(), ChangePassPresenter.View, View.OnClickListener {

    override val presenter: ChangePassPresenter.Presenter by inject()

    //***************************************************
    // LIFECYCLE
    //***************************************************
    override fun onStart() {
        super.onStart()
        lifecycle.addObserver(presenter)
    }
    override fun onStop() {
        super.onStop()
        lifecycle.removeObserver(presenter)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        setKoin()
        bindViews()

    }

    private fun bindViews() {
        Glide.with(this).load(presenter.loadUrl()).into(change_pass_header_img)
        change_pass_btn.setOnClickListener(this)
        change_pass_new_pass_img.setOnClickListener {visiblePass(change_pass_new_pass_img, change_pass_new_pass) }
        change_pass_repeat_pass_img.setOnClickListener { visiblePass(change_pass_repeat_pass_img, change_pass_repeat_pass)  }
    }

    private fun visiblePass(view: AppCompatImageView?, editText: AppCompatEditText) {
        if(view?.drawable?.constantState == resources.getDrawable(R.drawable.ic_visibility_off).constantState){
            view?.setImageResource(R.drawable.ic_visibility)
            editText.transformationMethod = HideReturnsTransformationMethod.getInstance()
            editText.setSelection(editText.length())
        }else{
            view?.setImageResource(R.drawable.ic_visibility_off)
            editText.transformationMethod = PasswordTransformationMethod.getInstance()
            editText.setSelection(editText.length())
        }
    }

    private fun setKoin() {
        presenter.view = this
    }


    //***************************************************
    // EVENTS
    //***************************************************

    override fun onClick(v: View?) {
        presenter.changePassword(change_pass_new_pass.text.toString(),change_pass_repeat_pass.text.toString())
    }

    override fun changePassInputError() {
        change_pass_btn.showSnackbar(getString(R.string.error_change_pass))
    }

    override fun chagePassError() {
        change_pass_btn.showSnackbar(getString(R.string.login_error_change_pass))
    }

    override fun changePassSucces() {
        setResult(Activity.RESULT_OK)
        finish()
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }
}
