/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */
package com.dinan.controlhorario.features.login.view

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.airbnb.lottie.LottieAnimationView
import com.dinan.controlhorario.R
import com.dinan.controlhorario.extension.launchActivity
import com.dinan.controlhorario.features.factoryDialogs.FactoryDialogs
import com.dinan.controlhorario.features.factoryDialogs.dialogs.GenericDialog
import com.dinan.controlhorario.features.home.view.MainActivity
import com.dinan.controlhorario.features.login.presenter.login.LoginPresenter
import kotlinx.android.synthetic.main.activity_login.*
import org.koin.android.ext.android.inject


class LoginActivity : AppCompatActivity(), LoginPresenter.View {


    private val ONLYCHANGEPASS: Int = 100
    private val ONLYLOPD: Int = 200
    private val LOPDANDCHANGEPASS: Int = 300

    //lazy injection with lifecycleObserve
    override val presenter: LoginPresenter.Presenter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        injectKoin()
        bindViews()
    }

    //***************************************************
    // LIFECYCLE
    //***************************************************
    override fun onStart() {
        super.onStart()
        lifecycle.addObserver(presenter)
    }

    override fun onStop() {
        super.onStop()
        lifecycle.removeObserver(presenter)
    }

    private fun bindViews() {
        login_btn_entrar.setOnClickListener {
            disableInputs()
            presenter.logIn(
                getCif()
                , login_et_user_email.text.toString()
                , login_et_password.text.toString()
            )

        }
    }


    private fun getCif(): String {
        return if (!presenter.getUserSession()) {
            login_et_cif_empresa.text.toString()
        } else {
            Log.e("manel", "obtenemos el valor del cif ${presenter.getUserSessionCif()}")
            presenter.getUserSessionCif()
        }
    }

    private fun injectKoin() {
        presenter.view = this
    }


    override fun lopdAndChangePass() {
        startActivityForResult(Intent(this, LegalActivity::class.java), LOPDANDCHANGEPASS)
    }

    override fun onlyLopd() {
        startActivityForResult(Intent(this, LegalActivity::class.java), ONLYLOPD)
    }

    override fun onlyChangePass() {
        startActivityForResult(Intent(this, ChangePasswordActivity::class.java), ONLYCHANGEPASS)
    }

    override fun goHome() {
        launchActivity<MainActivity>(this)
        finishAfterTransition()
    }


    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            ONLYLOPD -> if (resultCode == Activity.RESULT_OK) goHome() else dialog(ONLYLOPD)
            ONLYCHANGEPASS -> if (resultCode == Activity.RESULT_OK) goHome() else dialog(ONLYCHANGEPASS)
            LOPDANDCHANGEPASS -> if (resultCode == Activity.RESULT_OK) onlyChangePass() else dialog(
                LOPDANDCHANGEPASS
            )
        }
    }

    private fun dialog(retry: Int) {
        val dialogs = FactoryDialogs.createDialog(this, FactoryDialogs.TypeDialogs.GENERICDIALOG)
        (dialogs as GenericDialog).setCustomTitleMessage(
            getString(R.string.login_error_title),
            getString(R.string.login_error_msg_cancel),
            GenericDialog.EnumDialogGenericButton.TWOBUTTON
        )
        dialogs.btnNegativeTwoButton.setOnClickListener { dialogs.dismiss();finishAffinity() }
        dialogs.btnPositiveTwoButton.setOnClickListener {
            dialogs.dismiss()
            when (retry) {
                ONLYLOPD -> onlyLopd()
                ONLYCHANGEPASS -> onlyChangePass()
                LOPDANDCHANGEPASS -> lopdAndChangePass()
            }
        }
        dialogs.showDialog()
    }

    override fun loginError(s: String) {
        Log.e(LoginActivity::class.java.name, "login_error ${s}")
        val dialog = FactoryDialogs.createDialog(this, FactoryDialogs.TypeDialogs.GENERICDIALOG)
        dialog.getPositiveBoton().setOnClickListener { dialog.dismiss();enableInputs() }
        (dialog as GenericDialog).setCustomTitleMessage(
            getString(R.string.login_error_title),
            getString(R.string.login_error_access, s)
        )
        dialog.showDialog()

    }

    override fun showProgress() {
        (progress_login as LottieAnimationView).playAnimation()
        progress_login.visibility = View.VISIBLE

    }

    override fun hideProgress() {
        if ((progress_login as LottieAnimationView).isAnimating) {
            (progress_login as LottieAnimationView).pauseAnimation()
        }
        progress_login.visibility = View.GONE
    }

    override fun enableInputs() {
        login_et_cif_empresa.isEnabled = true
        login_et_user_email.isEnabled = true
        login_et_password.isEnabled = true

    }

    private fun disableInputs() {
        login_et_cif_empresa.isEnabled = false
        login_et_user_email.isEnabled = false
        login_et_password.isEnabled = false
    }

    override fun errorInputCif() {
        enableInputs()
        login_input_layout_cif.error = getString(R.string.error_login_txt_cif)
        login_et_cif_empresa.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //empty method
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (count > 0) {
                    login_input_layout_cif.isErrorEnabled = false
                }
            }

            override fun afterTextChanged(s: Editable) {
                //empty method
            }
        })
    }

    override fun errorInputUser() {
        enableInputs()
        login_input_layout_user_email.error = getString(R.string.error_login_txt_userEmail)
        login_et_user_email.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //empty method
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (start > 4) {
                    login_input_layout_user_email.isErrorEnabled = false
                }
            }

            override fun afterTextChanged(s: Editable) {
                //empty method
            }
        })
    }

    override fun errorInputPassword() {
        enableInputs()
        login_input_layout_password.error = getString(R.string.error_login_txt_password)
        login_et_password.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                //empty method
            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (start > 3) {
                    login_input_layout_password.isErrorEnabled = false
                }
            }

            override fun afterTextChanged(s: Editable) {
                //empty method
            }
        })

    }

}
