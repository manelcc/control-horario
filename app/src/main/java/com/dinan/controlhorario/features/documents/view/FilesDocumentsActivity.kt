/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.documents.view

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.recyclerview.widget.DividerItemDecoration
import com.airbnb.lottie.LottieAnimationView
import com.dinan.controlhorario.R
import com.dinan.controlhorario.core.base.globalConstants
import com.dinan.controlhorario.domain.model.documents.FileDocument
import com.dinan.controlhorario.domain.model.documents.FolderDocument
import com.dinan.controlhorario.extension.empty
import com.dinan.controlhorario.extension.showOrGone
import com.dinan.controlhorario.extension.showSnackbar
import com.dinan.controlhorario.features.documents.REQUEST_CAMERA_CAPTURE
import com.dinan.controlhorario.features.documents.REQUEST_IMAGE_CAPTURE
import com.dinan.controlhorario.features.documents.presenter.DocumentsPresenter
import com.dinan.controlhorario.features.documents.view.adapter.FilesAdapter
import com.dinan.controlhorario.features.factoryDialogs.FactoryDialogs
import com.dinan.controlhorario.features.factoryDialogs.dialogs.NameDocUpdate
import kotlinx.android.synthetic.main.activity_files_documents.*
import org.koin.android.ext.android.inject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.util.*


class FilesDocumentsActivity : AppCompatActivity(), DocumentsPresenter.View {

    private var file: File? = null
    private lateinit var currentPhotoPath: String
    private lateinit var adapter: FilesAdapter
    private lateinit var nameFolder: String
    private lateinit var directorio: String
    private lateinit var id: String
    override val presenter: DocumentsPresenter.Presenter by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_files_documents)
        getExtrasData()
        bindViews()
        settingViews()
        presenter.view = this
        presenter.loadFiles(id, directorio)
    }

    private fun settingViews() {
        fab_accion_image.setOnClickListener { fab.collapse();dispatchGalleryPictureIntent() }
        fab_accion_camera.setOnClickListener { fab.collapse();dispatchTakePictureIntent() }
    }

    private fun bindViews() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(true)
        files_document_folder_name.text = nameFolder
        adapter = FilesAdapter()
        fab.showOrGone(id == "0")
    }

    private fun getExtrasData() {
        intent.extras?.let {
            id = it.getString(globalConstants.KEY_ID_DOCUMENTS, String.empty())
            directorio = it.getString(globalConstants.KEY_DIRECTORIO, String.empty())
            nameFolder = it.getString(globalConstants.KEY_NAME_FOLDER, String.empty())
        }

    }

    override fun folderError(message: String) {
        //empty method.
    }

    override fun folderSuccess(resultSuccess: ArrayList<FolderDocument>) {
        //empty method.
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finishAndRemoveTask()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun successFilesDocuments(resultSuccess: ArrayList<FileDocument>) {
        files_document_recycler.adapter = adapter
        files_document_recycler.addItemDecoration(
            DividerItemDecoration(
                this,
                DividerItemDecoration.VERTICAL
            )
        )
        adapter.updateFilesItems(resultSuccess)
    }

    override fun errorFilesDocuments(message: String) {
        files_document_recycler.showSnackbar(message)
    }

    override fun onErrorUpdateDocument(message: String) {
        files_document_recycler.showSnackbar(message)
    }

    override fun successUpdateDocument() {
        files_document_recycler.showSnackbar("Hemos subido el fichero correctamente")
        file?.delete()
        presenter.loadFiles(id, directorio)
    }

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                // Create the File where the photo should go
                file = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    files_document_recycler.showSnackbar("Se ha producido un error al crear el fichero")
                    null
                }
                file?.also {
                    try {
                        val photoURI: Uri = FileProvider.getUriForFile(
                            this,
                            getString(R.string.providerAuthorities),
                            it
                        )
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                        startActivityForResult(takePictureIntent, REQUEST_CAMERA_CAPTURE)
                    } catch (ex: Exception) {
                        errorFilesDocuments(ex.message!!)
                    }
                }
            }
        }
    }

    private fun dispatchGalleryPictureIntent() {
        Intent(Intent.ACTION_OPEN_DOCUMENT, MediaStore.Images.Media.EXTERNAL_CONTENT_URI).also { galleryPictureIntent ->
            galleryPictureIntent.type = "image/*"
            galleryPictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            val mimeTypes = arrayOf("image/*", "application/pdf")
            galleryPictureIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
            galleryPictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(galleryPictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CAMERA_CAPTURE && resultCode == Activity.RESULT_OK) {
            getDialogNameDocument(null, REQUEST_CAMERA_CAPTURE)
        }
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK && data != null) {
            getDialogNameDocument(data, REQUEST_IMAGE_CAPTURE)
        }
    }

    private fun sendGalleryUpload(data: Intent, nameDocument: String) {
        val contentURI = data.data
        try {
            val bytes = ByteArrayOutputStream()
            val bitmap: Bitmap? = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
            saveFile(bitmap, bytes, contentURI ?: Uri.EMPTY)
            presenter.updateDocumentGallery(nameDocument, file!!, contentURI)
        } catch (e: IOException) {
            e.printStackTrace()
            files_document_recycler.showSnackbar("Se ha producido un error")
        }
    }

    private fun saveFile(
        bitmap: Bitmap?,
        bytes: ByteArrayOutputStream,
        contentURI: Uri
    ) {
        if (bitmap != null) {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 60, bytes)
            file = File.createTempFile("temp_", ".jpg")
            file?.writeBytes(bytes.toByteArray())
        } else {
            val parsePath = Uri.parse(contentURI.path)
            file = File(parsePath.path)
        }
    }

    private fun getDialogNameDocument(data: Intent?, requestCode: Int) {
        val dialog = FactoryDialogs.createDialog(this, FactoryDialogs.TypeDialogs.NAMEDOC)
        dialog.getPositiveBoton().setOnClickListener {
            when (requestCode) {
                REQUEST_CAMERA_CAPTURE -> presenter.updateDocument(
                    (dialog as NameDocUpdate).getNameDocument(),
                    currentPhotoPath
                )
                REQUEST_IMAGE_CAPTURE -> sendGalleryUpload(data!!, (dialog as NameDocUpdate).getNameDocument())
            }
            dialog.dismiss()
        }
        dialog.getNegativeButton().setOnClickListener { dialog.dismiss() }
        dialog.showDialog()
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${"nombre"}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }

    override fun showProgress() {
        progressFile.let {
            progressFile.visibility = View.VISIBLE
            if (!(progressFile as LottieAnimationView).isAnimating) {
                (progressFile as LottieAnimationView).playAnimation()
            }
        }

    }

    override fun hideProgress() {
        progressFile.let {
            if ((progressFile as LottieAnimationView).isAnimating) {
                (progressFile as LottieAnimationView).pauseAnimation()
            }
            progressFile.visibility = View.GONE
        }
    }
}
