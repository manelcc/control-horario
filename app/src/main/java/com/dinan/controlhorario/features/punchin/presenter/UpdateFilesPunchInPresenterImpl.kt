/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.punchin.presenter

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.dinan.controlhorario.domain.usecase.GetPunchInUploadCause
import com.dinan.controlhorario.features.punchin.presenter.callbacks.CallBackPunchInUpload
import java.io.File

class UpdateFilesPunchInPresenterImpl(private val useCase: GetPunchInUploadCause) : UpdateFilesPunchInPresenter.Presenter,
    LifecycleObserver {

    override lateinit var view: UpdateFilesPunchInPresenter.View

    override fun updateJustification(cause: Int, textIntro: String, file: File?) {
        view.showProgress()
        useCase.execute(CallBackPunchInUpload(view),GetPunchInUploadCause.Params(cause,textIntro,file))
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    override fun onResumeView() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    override fun onDestroyView() {
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun pausedFragment() {
        view.hideProgress()
    }
}