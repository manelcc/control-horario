/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.punchin.view


import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.RadioGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.content.FileProvider
import com.airbnb.lottie.LottieAnimationView
import com.dinan.controlhorario.R
import com.dinan.controlhorario.core.base.BaseFragment
import com.dinan.controlhorario.extension.showSnackbar
import com.dinan.controlhorario.features.documents.REQUEST_CAMERA_CAPTURE
import com.dinan.controlhorario.features.documents.REQUEST_IMAGE_CAPTURE
import com.dinan.controlhorario.features.punchin.presenter.UpdateFilesPunchInPresenter
import com.google.android.material.button.MaterialButton
import kotlinx.android.synthetic.main.fragment_update_files.*
import kotlinx.android.synthetic.main.fragment_update_files.view.*
import org.koin.android.ext.android.inject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import kotlin.math.roundToInt

class UpdateFilesFragment : BaseFragment<UpdateFilesPunchInPresenter.Presenter, UpdateFilesPunchInPresenter.View>(),
    UpdateFilesPunchInPresenter.View {


    private lateinit var textIntro: AppCompatEditText
    private lateinit var radioGroup: RadioGroup
    private lateinit var btnCancel: MaterialButton
    private lateinit var btnSend: MaterialButton
    private lateinit var fabFiles: AppCompatImageView
    private lateinit var fabCamera: AppCompatImageView
    private lateinit var layout: View
    private var file: File? = null
    private lateinit var currentPhotoPath: String


    override val presenter: UpdateFilesPunchInPresenter.Presenter by inject()
    override val view: UpdateFilesPunchInPresenter.View
        get() = this

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        layout = inflater.inflate(R.layout.fragment_update_files, container, false)
        bindViews()
        settingListeners()
        animateFabMenu()
        return layout
    }

    private fun settingListeners() {
        layout.fragment_updatefiles_close.setOnClickListener { activity?.onBackPressed() }
        fabCamera.setOnClickListener { dispatchCameraIntent() }
        fabFiles.setOnClickListener { dispatchFileIntent() }
        btnCancel.setOnClickListener { activity?.onBackPressed() }
        btnSend.isEnabled = getCause() != -1
        btnSend.setOnClickListener {
            val causa = getCause()
            Log.e("manel", "el radiogroup pulsado es ${causa}")
            Log.e("manel", "el radiogroup texto es ${textIntro.text.toString()}")
            Log.e("manel", "el radiogroup archivo es ${file?.length()}")
            presenter.updateJustification(causa, textIntro.text.toString(), file)
        }

        radioGroup.setOnCheckedChangeListener { _, _ ->
            btnSend.isEnabled = getCause() != -1
        }
    }

    private fun getCause(): Int {
        return when (radioGroup.checkedRadioButtonId) {
            R.id.fragment_updatefiles_rb_retraso -> return 1
            R.id.fragment_updatefiles_rb_baja -> return 3
            R.id.fragment_updatefiles_rb_ausencia -> return 2
            else -> -1
        }
    }

    private fun bindViews() {
        textIntro = layout.fragment_updatefiles_et
        radioGroup = layout.fragment_updatefiles_radiogroup
        btnSend = layout.fragment_updatefiles_btn_request
        btnCancel = layout.fragment_updatefiles_btn_cancel
        fabCamera = layout.fragment_updatefiles_fabCamera
        fabFiles = layout.fragment_updatefiles_fabFiles


    }

    private fun animateFabMenu() {
        layout.fragment_updatefiles_fabmenu.setOnClickListener {
            if (fabCamera.visibility == VISIBLE) {
                fabCamera.visibility = GONE
                fabFiles.visibility = GONE
            } else {
                fabCamera.visibility = VISIBLE
                fabFiles.visibility = VISIBLE
            }
        }
    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_CAMERA_CAPTURE && resultCode == Activity.RESULT_OK) {
            //camera
            var fileCamera = File(currentPhotoPath)
            file = resizeBitmap(fileCamera, currentPhotoPath)
            fabCamera.setBackgroundResource(R.drawable.shape_fab_updatefiles_menu_selected)
            fabFiles.setBackgroundResource(R.drawable.shape_fab_updatefiles_menu)
        }
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == AppCompatActivity.RESULT_OK && data != null) {
            sendGalleryUpload(data)
            fabCamera.setBackgroundResource(R.drawable.shape_fab_updatefiles_menu)
            fabFiles.setBackgroundResource(R.drawable.shape_fab_updatefiles_menu_selected)
        }
    }

    private fun dispatchFileIntent() {
        Intent(Intent.ACTION_OPEN_DOCUMENT,MediaStore.Images.Media.EXTERNAL_CONTENT_URI).also { galleryPictureIntent ->
            galleryPictureIntent.type = "image/*"
            galleryPictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            val mimeTypes = arrayOf("image/*","application/pdf")
            galleryPictureIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
            galleryPictureIntent.resolveActivity(activity?.packageManager!!)?.also {
                startActivityForResult(galleryPictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }

    @Suppress("DEPRECATION")
    private fun sendGalleryUpload(data: Intent) {
        val contentURI = data.data
        try {
            val bytes = ByteArrayOutputStream()
            val bitmap = MediaStore.Images.Media.getBitmap(activity?.contentResolver, contentURI)
            saveFile(bitmap, bytes,contentURI?: Uri.EMPTY)
        } catch (e: IOException) {
            e.printStackTrace()
            fabFiles.showSnackbar("Se ha producido un error")
        }
    }

    private fun saveFile(
        bitmap: Bitmap?,
        bytes: ByteArrayOutputStream,
        contentURI: Uri
    ) {
        if (bitmap != null) {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 60, bytes)
            file = File.createTempFile("temp_", ".jpg")
            file?.writeBytes(bytes.toByteArray())
        } else {
            val parsePath = Uri.parse(contentURI.path)
            file = File(parsePath.path)
        }
    }


    private fun dispatchCameraIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(activity?.packageManager!!)?.also {
                // Create the File where the photo should go
                file = try {
                    createImageFile()
                } catch (ex: IOException) {
                    // Error occurred while creating the File
                    fabCamera.showSnackbar("Se ha producido un error al crear el fichero")
                    null
                }
                file?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        activity?.baseContext!!,
                        getString(R.string.providerAuthorities),
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    startActivityForResult(takePictureIntent, REQUEST_CAMERA_CAPTURE)
                }

            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val storageDir: File? = activity?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${"nombre"}_", /* prefix */
            ".jpg", /* suffix */
            storageDir /* directory */
        ).apply {
            // Save a file: path for use with ACTION_VIEW intents
            currentPhotoPath = absolutePath
        }
    }

    private fun resizeBitmap(fileCamera: File, pathFile: String): File {
        val bytes = ByteArrayOutputStream()
        if (fileCamera.exists()) {
            val bitmap = BitmapFactory.decodeFile(pathFile)
            try {
                bitmap?.let {
                    val newWith: Int = (it.width * 0.50).roundToInt()
                    val newHeight: Int = (it.height * 0.50).roundToInt()
                    Bitmap.createScaledBitmap(it, newWith, newHeight, true)
                    it.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
                    fileCamera.writeBytes(bytes.toByteArray())
                }
            } catch (exception: Exception) {
                Log.e(javaClass.simpleName, "exception write camera ${exception.message}")
            }

        }
        return fileCamera
    }

    override fun successUploadPunchIn(resultSuccess: Boolean) {
        fabCamera.showSnackbar("Perfecto, hemos enviado su petición").also {
            Thread(Runnable {
                Thread.sleep(1000)
                activity?.runOnUiThread { activity?.onBackPressed() }
            }).start()
        }

    }

    override fun errorUploadPunchIn(message: String?) {
        fabCamera.showSnackbar("Ups, algo a fallado pruebe de nuevo ${message ?: ""}").also {
            Thread(Runnable {
                Thread.sleep(1000)
                activity?.runOnUiThread { activity?.onBackPressed() }
            }).start()
        }
    }

    override fun showProgress() {
        (progressUpload as LottieAnimationView).playAnimation()
        progressUpload.visibility = VISIBLE

    }

    override fun hideProgress() {
        if ((progressUpload as LottieAnimationView).isAnimating) {
            (progressUpload as LottieAnimationView).pauseAnimation()
        }
        progressUpload.visibility = GONE
    }


    companion object {
        @JvmStatic
        fun newInstance() = UpdateFilesFragment()
    }

    override fun setUpToolbarFromFragment() {
        //do nothing here
    }
}
