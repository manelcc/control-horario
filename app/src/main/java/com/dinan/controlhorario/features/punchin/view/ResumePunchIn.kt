/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.punchin.view


import android.annotation.SuppressLint
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.dinan.controlhorario.R
import com.dinan.controlhorario.domain.extension.asynCompose
import com.dinan.controlhorario.domain.model.punchin.EventPunchIn
import com.dinan.controlhorario.extension.Properties.DATEFORMAT_STRING_MONTH
import com.dinan.controlhorario.extension.customDate
import com.dinan.controlhorario.features.punchin.view.adapter.PunchInAdapterEvent
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_resume_punch_in.view.*
import org.koin.android.ext.android.inject
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class ResumePunchIn : Fragment() {

    private var fecha: String? = ""
    private var value: Disposable? = null
    private var timerDispose: Disposable? = null
    private var layout: View? = null
    private lateinit var adapter: PunchInAdapterEvent
    private var listEvents: ArrayList<EventPunchIn>? = null
    private val preference: SharedPreferences by inject()

    companion object {
        @JvmStatic
        fun newInstance(
            listPunchIn: ArrayList<EventPunchIn>,
            fecha: String
        ) =
            ResumePunchIn().apply {
                arguments = Bundle().apply {
                    putParcelableArrayList(ARG_PARAM1, listPunchIn)
                    putString(ARG_PARAM2, fecha)
                }
            }
    }

    override fun onStart() {
        super.onStart()
        timerDispose = Observable.interval(0, 1, TimeUnit.SECONDS)
            .timeInterval()
            .asynCompose()
            .subscribe(
                { successTimer() },
                { Log.e("manel", "tiempo ${it.message}") })
    }

    @SuppressLint("SimpleDateFormat")
    private fun successTimer() {
        val dateFormat = SimpleDateFormat("HH:mm:ss")
        val valueHour: String = dateFormat.format(Calendar.getInstance().time)
        this.layout?.let { it.resumeHour.text = valueHour }
    }


    override fun onStop() {
        super.onStop()
        timerDispose?.dispose()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        value?.dispose()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            listEvents = it.getParcelableArrayList(ARG_PARAM1)
            fecha = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        layout = inflater.inflate(R.layout.fragment_resume_punch_in, container, false)
        adapter = PunchInAdapterEvent(false)
        listeners()
        recycler()

        return layout
    }

    private fun recycler() {
        layout?.apply {
            resumeDate.text = String.customDate(DATEFORMAT_STRING_MONTH, fecha?:"")
            listEvents?.first { eventPunchIn -> resumeTxtTotalHours.text = eventPunchIn.totalWork; true }
            resumeRecycler.setHasFixedSize(true)
            resumeRecycler.adapter = adapter
        }
        listEvents?.let { adapter.updateValues(it) }
    }

    private fun listeners() {
        layout?.resumeBtnAceptar?.setOnClickListener {
            fragmentManager?.let { fragmentManager ->
                getPunchInFragment(fragmentManager)
                val fragment = fragmentManager.findFragmentByTag("resumeFragment")
                fragment?.let {
                    fragmentManager.beginTransaction()
                        .setCustomAnimations(
                            R.anim.slide_in_up,
                            R.anim.slide_in_down,
                            R.anim.slide_out_down,
                            R.anim.slide_out_up
                        )
                        .remove(fragment).commitAllowingStateLoss()
                }
            }
        }
    }

    private fun getPunchInFragment(fragmentManager: FragmentManager) {
        for (item in fragmentManager.fragments) {
            if (item is PunchInFragment) {
                item.updateToggleButton(true)
            }
        }
    }




}


