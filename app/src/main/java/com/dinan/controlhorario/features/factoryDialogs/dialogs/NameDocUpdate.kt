/*
 * Copyright (C) 2019.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.factoryDialogs.dialogs

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.AppCompatEditText
import com.dinan.controlhorario.R
import com.dinan.controlhorario.features.factoryDialogs.CustomDialog
import kotlinx.android.synthetic.main.dialog_custom_name_document.view.*

class NameDocUpdate(context: Activity?) : BaseAlertDialogDinan(context), CustomDialog {

    val dialogView: View by lazy {
        val viewGroup = context?.findViewById<ViewGroup>(android.R.id.content)
        LayoutInflater.from(context).inflate(R.layout.dialog_custom_name_document, viewGroup, false)
    }
    val alertDialog: AlertDialog by lazy { dialogMaterial(dialogView) }
    val btnNegative: View by lazy { dialogView.dialog_custom_two_negative }
    val btnPositive: View by lazy { dialogView.dialog_custom_two_positiveButon }
    val nameDoc: AppCompatEditText by lazy { dialogView.dialog_et_message }


    override fun showDialog() {
        alertDialog.show()
    }

    override fun getNeutralButton(): View {
        return btnNegative
    }

    override fun getPositiveBoton(): View {
        return btnPositive
    }

    override fun getNegativeButton(): View {
        return btnNegative
    }

    override fun dismiss() {
        alertDialog.dismiss()
    }
    fun getNameDocument():String{
        return nameDoc.text.toString()
    }
}




