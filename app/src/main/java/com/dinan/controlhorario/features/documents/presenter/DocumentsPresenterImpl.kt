/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.documents.presenter

import android.net.Uri
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.dinan.controlhorario.domain.usecase.GetFileDocument
import com.dinan.controlhorario.domain.usecase.GetFolderDocument
import com.dinan.controlhorario.domain.usecase.GetUploadDocument
import com.dinan.controlhorario.features.documents.presenter.callbacks.FilesCallBack
import com.dinan.controlhorario.features.documents.presenter.callbacks.FolderCallBack
import com.dinan.controlhorario.features.documents.presenter.callbacks.UploadCallBack
import java.io.File

class DocumentsPresenterImpl(
    private val getFolderDocument: GetFolderDocument,
    private val getFileDocument: GetFileDocument,
    private val getUploadDocument: GetUploadDocument
) : DocumentsPresenter.Presenter, LifecycleObserver {

    override lateinit var view: DocumentsPresenter.View

    override fun updateDocumentGallery(nameFile: String, fileImage: File, contentURI: Uri?) {
        if (nameFile.isEmpty()) {
            view.onErrorUpdateDocument("Indicar nombre del archivo a subir")
        } else {
            view.showProgress()
            getUploadDocument.execute(
                UploadCallBack(view), GetUploadDocument.Params(
                    nameFile.plus(".").plus(fileImage.extension), "",
                    fileImage, contentURI ?: Uri.EMPTY
                )
            )
        }
    }


    override fun updateDocument(nameFile: String, pathImage: String) {
        if (nameFile.isEmpty()) {
            view.onErrorUpdateDocument("Indicar nombre del archivo a subir")
        } else {
            view.showProgress()
            getUploadDocument.execute(
                UploadCallBack(view), GetUploadDocument.Params(
                    nameFile.plus(".jpg"),
                    pathImage
                )
            )
        }

    }

    override fun loadFiles(id: String, directorio: String) {
        getFileDocument.execute(FilesCallBack(view), GetFileDocument.Params(id, directorio))
    }


    override fun loadFolders() {
        getFolderDocument.execute(FolderCallBack(view), Unit)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    fun pausedFragment() {
        view.hideProgress()
    }


    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    override fun onResumeView() {
        //do nothing
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    override fun onDestroyView() {
        getFolderDocument.dispose()
    }
}