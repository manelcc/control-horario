/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.extension

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object Properties {
    const val DATEFORMAT_STRING_MONTH = "dd  MMMM yyyy"
    const val DATEFORMAT_STRING_NORMALCALENDAR = "dd-MM-yyyy"
    private var CIF_URL: String = String.empty()
}


fun String.Companion.empty() = ""

/**
 * Extension method to check if String is Phone Number.
 */
fun String.isPhone(): Boolean {
    val p = "^1([34578])\\d{9}\$".toRegex()
    return matches(p)
}

fun String.isValidPass(): Boolean {
    return this.length > 5
}

/**
 * Extension method to check if String is Email.
 */
fun String.isEmail(): Boolean {
    val p = "^(\\w)+(\\.\\w+)*@(\\w)+((\\.\\w+)+)\$".toRegex()
    return matches(p)
}

/**
 * Extension method to check if String is Number.
 */
fun String.isNumeric(): Boolean {
    val p = "^[0-9]+$".toRegex()
    return matches(p)
}

fun String.Companion.customDateNow(formatDate: String?): String {
    formatDate?.let {
        val sdf = SimpleDateFormat(formatDate,Locale("es", "ES"))
        val calendar = Calendar.getInstance()
        return sdf.format(calendar.time)
    }
    return String.empty()
}

fun String.Companion.customDate(formatDate: String?, dataTime: String): String {
    formatDate?.let {
        val sdf = SimpleDateFormat(formatDate, Locale("es", "ES"))
        val calendar = dateInFormat("yyyy-MM-dd", dataTime)
        return sdf.format(calendar)
    }
    return String.empty()
}

/**
 * Extension method to get Date for String with specified format.
 */
fun dateInFormat(format: String, dataTime: String): Date {
    val dateFormat = SimpleDateFormat(format,Locale("es", "ES"))
    val convertedDate = Date()
    return try {
        dateFormat.parse(dataTime)!!
    } catch (e: ParseException) {
        e.printStackTrace()
        convertedDate
    }
}