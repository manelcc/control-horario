/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.extension

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
val formatDefaultPattern = SimpleDateFormat("yyyy-MM-dd")
@SuppressLint("SimpleDateFormat")
val formatFullMonth = SimpleDateFormat("MMMM")
@SuppressLint("SimpleDateFormat")
val formatOutputHourCalendar = SimpleDateFormat("H'h' m'min'")
@SuppressLint("SimpleDateFormat")
val formatInputHourCalendar = SimpleDateFormat("HH:mm:ss",Locale.getDefault())


fun Calendar.stringServerFormat(): String {
    return formatDefaultPattern.format(this.time)
}

fun Calendar.lessMonth(): Calendar {
    add(Calendar.MONTH, -1)
    return this

}

fun Calendar.addMonth(): Calendar {
    add(Calendar.MONTH, +1)
    return this
}

