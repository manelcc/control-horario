/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.extension

import android.app.Activity
import android.content.Context
import android.view.inputmethod.InputMethodManager
import androidx.annotation.IdRes
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.dinan.controlhorario.R


/**
 * The `fragment` is replace to the container view with id `frameId`. The operation is
 * performed by the `fragmentManager`.
 */
fun AppCompatActivity.replaceFragment(fragment: Fragment, @IdRes frameId: Int, tagFragment: String) {
    if (findFragmentByTag(tagFragment) == null) {
        supportFragmentManager.beginTransaction()
            .replace(frameId, fragment, tagFragment)
            .commitAllowingStateLoss()
    }
}

/**
 * The `fragment` is replace ANIM LEFT-RIGHT to the container view with id `frameId`. The operation is
 * performed by the `fragmentManager`.
 */
fun FragmentActivity.replaceFragmentAnim(fragment: Fragment, @IdRes frameId: Int, tagFragment: String) {
    supportFragmentManager.beginTransaction()
        .setCustomAnimations(R.anim.slide_in_up, R.anim.slide_in_down, R.anim.slide_out_down, R.anim.slide_out_up)
        .replace(frameId, fragment, tagFragment)
        .addToBackStack(tagFragment)
        .commitAllowingStateLoss()
}

/**
 * The `fragment` is added to the container view with id `frameId`. The operation is
 * performed by the `fragmentManager`.
 */
fun AppCompatActivity.removeFragmentByTag(tagFragment: String) {
    val fragment = findFragmentByTag(tagFragment)
    fragment?.let { supportFragmentManager.beginTransaction().remove(it) }
}

/**
 * The `fragment` is added to the container view with id `frameId`. The operation is
 * performed by the `fragmentManager`.
 */
fun AppCompatActivity.findFragmentByTag(tagFragment: String): Fragment? {
    return supportFragmentManager.findFragmentByTag(tagFragment)
}


/**
 * Extension method to provide hide keyboard for [Activity].
 */
fun FragmentActivity.hideSoftKeyboard() {
    if (currentFocus != null) {
        val inputMethodManager = getSystemService(
            Context
                .INPUT_METHOD_SERVICE
        ) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    }
}


/**
 * Extension method to provide hide keyboard for [Fragment].
 */
fun Fragment.hideSoftKeyboard() {
    activity?.hideSoftKeyboard()
}

