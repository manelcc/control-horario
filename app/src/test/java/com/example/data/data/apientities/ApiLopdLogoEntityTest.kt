/*
 * Copyright (C) 2019.  Daniel Jimenez
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.example.data.data.apientities

import com.dinan.controlhorario.data.apientities.login.ApiLopdLogoEntity
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertSame
import org.junit.Before
import org.junit.Test

class ApiLopdLogoEntityTest {

    private lateinit var apiLopdEntity: ApiLopdLogoEntity

    @Before
    fun setUp() {
        apiLopdEntity = ApiLopdLogoEntity(
            "cif",
            "logo",
            "lopd",
            "utoken",
            "version"
        )
    }

    @Test
    fun valuesConfirm() {
        assertNotNull(apiLopdEntity)
        assertSame(apiLopdEntity.cif, "cif")
        assertSame(apiLopdEntity.lopd,"lopd")
        assertSame(apiLopdEntity.logo,"logo")
        assertSame(apiLopdEntity.utoken, "utoken")
        assertSame(apiLopdEntity.version, "version")
    }
}