/*
 * Copyright (C) 2019.  Daniel Jimenez
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.example.data.data.apientities

import com.dinan.controlhorario.data.apientities.login.ApiConfirmLopd
import org.junit.Assert.assertNotNull
import org.junit.Assert.assertSame
import org.junit.Before
import org.junit.Test

class ApiConfirmLopdTest {

    private lateinit var confirmLopd: ApiConfirmLopd

    @Before
    fun setUp() {
        confirmLopd = ApiConfirmLopd("cif", true, "utoken", "version")
    }

    @Test
    fun valuesConfirm() {
        assertNotNull(confirmLopd)
        assertSame(confirmLopd.cif, "cif")
        assertSame(confirmLopd.lopdOk, true)
        assertSame(confirmLopd.utoken, "utoken")
        assertSame(confirmLopd.version, "version")
    }
}