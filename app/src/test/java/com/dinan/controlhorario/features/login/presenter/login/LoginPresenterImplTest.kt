/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.features.login.presenter.login

import android.app.Application
import android.content.SharedPreferences
import com.dinan.controlhorario.domain.model.login.User
import com.dinan.controlhorario.domain.usecase.login.GetLoginUseCase
import org.amshove.kluent.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import kotlin.test.assertNotNull


class LoginPresenterImplTest {

    @Mock
    private lateinit var usecase: GetLoginUseCase

    @Mock
    private lateinit var preferences: SharedPreferences

    @Mock
    private lateinit var view: LoginPresenter.View

    @Mock
    private lateinit var params: GetLoginUseCase.ParamsLogin

    @Mock
    private lateinit var context: Application



    private lateinit var presenter: LoginPresenter.Presenter

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = LoginPresenterImpl(usecase,preferences,context)
        presenter.view = view
    }


    @Test
    fun getUserSessionCif() {
        //Given
        val actual: String = ""
        //when
        val expected = presenter.getUserSessionCif()
        //then
        assert(actual.equals(expected))
    }


    @Test
    fun getUserSession() {
    }

    @Test
    fun logIn() {
        //Given
        val email = "email"
        val cif = "cif"
        val pass = "pass"
        //when
        presenter.logIn(cif, email, pass)
        //then
        Verify on view that view.showProgress() was called
        //pendiente verificar usecase.execute con kluent
        //Verify on usecase that usecase.execute(observer = obs , params = parametro)
    }



    @Test
    fun getPreferences() {
        val user = getUser()
        presenter.getUserSession()

        assertNotNull(user)
    }


    fun getUserWhithOutCif(): User? {
        return User("urlSession", "", "")
    }

    fun getUser(): User? {
        return User("urlSession", "123456a", "afsfasfsfa")
    }

}