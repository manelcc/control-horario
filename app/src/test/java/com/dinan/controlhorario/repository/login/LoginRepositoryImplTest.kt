/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.repository.login

import android.content.SharedPreferences
import com.dinan.controlhorario.data.apientities.login.ApiGetHost
import com.dinan.controlhorario.data.datasource.login.login.DtsLogin
import com.dinan.controlhorario.data.repository.LoginRepositoryImpl

import com.dinan.controlhorario.domain.repository.LoginRepository
import com.dinan.controlhorario.domain.usecase.login.GetLoginUseCase

import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import java.util.concurrent.atomic.AtomicInteger


class LoginRepositoryImplTest {

    @Mock
    private lateinit var dtsLogin: DtsLogin
    @Mock
    private lateinit var preferences: SharedPreferences
    //testClass
    private lateinit var repository: LoginRepository


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        repository = LoginRepositoryImpl(dtsLogin, preferences)

    }

    @Test
    fun `check emissions count`() {
        val emissionsCount = AtomicInteger()//(1)
        Observable.range(1, 10)
                .subscribeOn(Schedulers.computation())
                .blockingSubscribe {//(2)
                    _ ->
                    emissionsCount.incrementAndGet()
                }

        assertEquals(10, emissionsCount.get())//(3)
    }

    /*@Test
    fun getApiHostValues(): ApiGetHost {
        val apiData: ApiGetHost = getApiHost()
        val params = GetLoginUseCase.ParamsLogin("a", "b", "c")

        Mockito.`when`(dtsLogin.getHost(params)).thenReturn(Observable.just(getApiHost()))

        val apiGetHostTest = dtsLogin.getHost(params)
        val testObserver = TestObserver<ApiGetHost>()
        apiGetHostTest.subscribe(testObserver)//(1) suscribiendo el observable

        var values = testObserver.values().get(0)

        assertNotNull(values)
        return values
    }*/


    @Test
    fun getLogIn() {
        val params = GetLoginUseCase.ParamsLogin("a", "b", "c")

        /*Mockito.`when`(com.dinan.controlhorario.data.repository.getLogIn(params)).thenReturn(Observable.just(User()))
        com.dinan.controlhorario.data.repository.getLogIn(params)


        val observable: Observable<User> = com.dinan.controlhorario.data.repository.getLogIn(params)
        val testObserver = TestObserver<User>()

        observable.subscribe(testObserver)//(1) suscribiendo el observable
        testObserver.assertSubscribed()//(2) preguntamos si la suscribcion es exitosa
        testObserver.awaitTerminalEvent()//(3) estamos bloqueando el hilo hasta que Observable / Flowable complete su ejecución con el
        testObserver.assertNoErrors()//(4)
        testObserver.assertComplete()//(5)
        testObserver.assertValueCount(1)//(6)*/

        /* dtsLogin.getHost(params)
             .concatMap { apiData: ApiGetHost ->
                 dtsLogin.getLogin(params, apiData).map { data: ApiUser ->
                     data.error?.let { mapError(it) }
                     apiData.acceso?.let { User().mapFromApi(apiData, data, it) }
                 }
                     .doAfterNext { user ->
                         val serializedUser: String = Gson().toJson(user)
                         preferences.edit().putString(globalConstants.KEY_USER_PREF, serializedUser).commit()
                     }
             }

 */
    }


    private fun getApiHost(): ApiGetHost {
        return ApiGetHost("acceso", "version", "url")
    }

}