/*
 * Copyright (C) 2019.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.domain.core.base

import com.nhaarman.mockito_kotlin.mock
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler
import org.amshove.kluent.*
import org.junit.Before
import org.junit.Test

class BaseUseCaseTest{

    private var useCase: UseCaseTestClass? = null

    private lateinit var testObserver: TestDisposableObserver<Any>


    val disponsable: Disposable = mock()


    @Before
    fun setUp() {
        RxJavaPlugins.setIoSchedulerHandler { _ -> TestScheduler() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { _ -> Schedulers.trampoline() }
        this.useCase = UseCaseTestClass()
        this.testObserver = TestDisposableObserver()

    }
    @Test
    fun testBuildUseCaseObservableReturnCorrectResult() {
        //Given
        val expectedValue = 0
        //when
        useCase?.execute(testObserver, Params.EMPTY)
        //then
        expectedValue.shouldEqualTo(testObserver.valuesCount)

    }

    @Test
    fun testSubscriptionWhenExecutingUseCase() {
        //given
        //when
        useCase?.execute(testObserver, Params.EMPTY)
        useCase?.dispose()
        val expectedValue:Boolean = testObserver.isDisposed
        //then
        expectedValue.`should equal to`(true)
    }

    @Test
    fun testCallDispose(){
        //given
        val disposables: CompositeDisposable = CompositeDisposable()
        disposables.add(disponsable)
        //when
        disposables.isDisposed
        //then
        Verify that disposables.dispose() was called
    }

    private class UseCaseTestClass : BaseUseCase<Any, Params>() {

        public override fun buildUseCaseObservable(params: Params): Observable<Any> {
            return Observable.empty()
        }

    }

    private class TestDisposableObserver<T> : DefaultObserver<T>() {
        var valuesCount = 0

        override fun onNext(value: T) {
            valuesCount++
        }

        override fun onError(e: Throwable) {
            // no-op by default.
        }

        override fun onComplete() {
            // no-op by default.
        }
    }

    private class Params {
        companion object{val EMPTY = Params()}
    }
}