/*
 * Copyright (C) 2019.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.domain.usecase

import com.dinan.controlhorario.domain.repository.CalendarRepository
import org.amshove.kluent.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class GetCalendarioUseCaseTest{
    private lateinit var useCase: GetCalendarioUseCase
    @Mock
    private lateinit var mockRepo: CalendarRepository
    @Mock
    private lateinit var params: GetCalendarioUseCase.ParamsCalendar

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        useCase = GetCalendarioUseCase(mockRepo)
    }

    @Test
    fun testGetCalendarOnSuccess() {
        var fecha = GetCalendarioUseCase.ParamsCalendar("fecha")
        useCase.buildUseCaseObservable(fecha)

        Verify on mockRepo that mockRepo.calendar(fecha = "fecha") was called

        /*verify<Any>(mockUserRepository).user(USER_ID)
        verifyNoMoreInteractions(mockUserRepository)
        verifyZeroInteractions(mockPostExecutionThread)
        verifyZeroInteractions(mockThreadExecutor)*/
    }
}