/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.domain.model.punchin

import android.os.Parcelable
import com.dinan.controlhorario.domain.extension.empty
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EventPunchIn(
    val action: String = String.empty(),
    val dataTime: String = "2050-01-01",
    val hourTime: String = "00:00",
    val pendingMessageReader: String = String.empty(),
    val totalWork: String = "0:00",
    val id: String = String.empty(),
    var hourSelected: String = String.empty()
) : Parcelable