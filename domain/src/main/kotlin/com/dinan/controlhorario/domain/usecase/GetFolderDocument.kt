/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.domain.usecase

import com.dinan.controlhorario.domain.core.base.BaseUseCase
import com.dinan.controlhorario.domain.model.documents.FolderDocument
import com.dinan.controlhorario.domain.repository.DocumentsRepository
import io.reactivex.Observable

class GetFolderDocument(private val repository: DocumentsRepository):
    BaseUseCase<ArrayList<FolderDocument>, Unit>() {

    override fun buildUseCaseObservable(params: Unit): Observable<ArrayList<FolderDocument>> {
        return try {
            repository.folderDocuments()
        } catch (exception: Exception) {
            Observable.error(exception)
        }

    }


}