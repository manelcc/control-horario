/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.domain.repository

import com.dinan.controlhorario.domain.model.calendario.Calendario
import com.dinan.controlhorario.domain.model.punchin.EventPunchIn
import io.reactivex.Observable

interface CalendarRepository {

    fun calendar(fecha: String): Observable<Calendario>

    fun getHours(fecha: String, horas: String): Observable<List<EventPunchIn>>
    fun changeHours(params: ArrayList<EventPunchIn>, observation: String): Observable<Boolean>
}