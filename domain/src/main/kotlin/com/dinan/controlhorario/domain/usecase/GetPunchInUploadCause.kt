/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.domain.usecase

import com.dinan.controlhorario.domain.core.base.BaseUseCase
import com.dinan.controlhorario.domain.extension.empty
import com.dinan.controlhorario.domain.repository.PunchInRepository
import io.reactivex.Observable
import java.io.File

class GetPunchInUploadCause(private val repository: PunchInRepository) :
    BaseUseCase<Boolean, GetPunchInUploadCause.Params>() {

    override fun buildUseCaseObservable(params: Params): Observable<Boolean> {
        return try {
            repository.uploadCausePunchIn(params.cause,params.text,params.file,params.filename)
        } catch (exception: Exception) {
            Observable.error(exception)
        }
    }

    data class Params(val cause:Int=-1,val text:String = String.empty(),val file: File?,val filename: String="provisionalFile")

}