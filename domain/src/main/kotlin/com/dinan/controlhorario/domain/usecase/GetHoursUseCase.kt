/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.domain.usecase

import com.dinan.controlhorario.domain.core.base.BaseUseCase
import com.dinan.controlhorario.domain.model.punchin.EventPunchIn
import com.dinan.controlhorario.domain.repository.CalendarRepository
import io.reactivex.Observable

class GetHoursUseCase(private val repository: CalendarRepository) :
    BaseUseCase<List<EventPunchIn>, GetHoursUseCase.Params>() {

    override fun buildUseCaseObservable(params: Params): Observable<List<EventPunchIn>> {
        return try {
            repository.getHours(params.fecha,params.horas)
        } catch (exception: Exception) {
            Observable.error(exception)
        }
    }

    data class Params(val fecha: String, val horas: String)


}