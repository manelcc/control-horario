/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.domain.core.di

import com.dinan.controlhorario.domain.usecase.*
import com.dinan.controlhorario.domain.usecase.login.GetAcceptLopdImpl
import com.dinan.controlhorario.domain.usecase.login.GetChangePassUseCaseImpl
import com.dinan.controlhorario.domain.usecase.login.GetLegalUseCaseImpl
import com.dinan.controlhorario.domain.usecase.login.GetLoginUseCase
import org.koin.dsl.module

val useCaseModule = module {


    factory { GetLoginUseCase(get()) }
    factory { GetLegalUseCaseImpl(get()) }
    factory { GetAcceptLopdImpl(get()) }
    factory { GetChangePassUseCaseImpl(get()) }
    factory { GetPunchInUseCase(get()) }
    factory { GetCalendarioUseCase(get()) }
    factory { GetHoursUseCase(get()) }
    factory { GetFolderDocument(get()) }
    factory { GetFileDocument(get()) }
    factory { GetUploadDocument(get()) }
    factory { GetMessagesList(get()) }
    factory { SendMessageUseCase(get()) }
    factory { GetRequestChangeHour(get()) }
    factory { GetPunchInUploadCause(get()) }
}