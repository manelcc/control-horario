/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.domain.usecase

import android.location.Location
import com.dinan.controlhorario.domain.core.base.BaseUseCase
import com.dinan.controlhorario.domain.model.punchin.PunchInModel
import com.dinan.controlhorario.domain.repository.PunchInRepository
import io.reactivex.Observable


class GetPunchInUseCase(private val repository: PunchInRepository) :
    BaseUseCase<PunchInModel, GetPunchInUseCase.ParamsPunchIn>() {
    override fun buildUseCaseObservable(params: ParamsPunchIn): Observable<PunchInModel> {
        return when (params.event) {
            EventPunchIn.LOADEVENTS -> repository.loadEvents()
            EventPunchIn.UPDATEEVENTS -> repository.updatePunchIn(params.statePunchIn, params.location)
        }

    }

    enum class EventPunchIn {
        LOADEVENTS, UPDATEEVENTS
    }


    data class ParamsPunchIn(
        val event: EventPunchIn,
        val location: Location,
        val statePunchIn: String = ""

    )

}