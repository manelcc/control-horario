/*
 * Copyright (C) 2020.  Manel Cabezas
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

package com.dinan.controlhorario.domain.usecase

import android.net.Uri
import com.dinan.controlhorario.domain.core.base.BaseUseCase
import com.dinan.controlhorario.domain.extension.empty
import com.dinan.controlhorario.domain.model.documents.UploadDocument
import com.dinan.controlhorario.domain.repository.DocumentsRepository
import io.reactivex.Observable
import java.io.File

class GetUploadDocument (private val repository: DocumentsRepository):
    BaseUseCase<UploadDocument, GetUploadDocument.Params>() {
    override fun buildUseCaseObservable(params: Params): Observable<UploadDocument> {
        return try {
            repository.uploadDocument(params.fileName,params.pathFile,params.fileImage,params.contentURI)
        } catch (exception: Exception) {
            Observable.error(exception)
        }
    }


    data class Params(
        val fileName: String,
        val pathFile: String = String.empty(),
        val fileImage: File? = null,
        val contentURI: Uri = Uri.EMPTY
    )
}